<?php

require_once 'init.php';

if (!authenticated()) {
    header("Location: login.php");
    exit;
}

?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

        <!-- START @GLOBAL MANDATORY STYLES -->
        <link href="assets/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!--/ END GLOBAL MANDATORY STYLES -->

        <!-- START @PAGE LEVEL STYLES -->
        <link href="assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/global/plugins/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="assets/global/plugins/bower_components/bootstrap-calendar/css/calendar.min.css" rel="stylesheet">
        <link href="assets/global/plugins/bower_components/jquery.gritter/css/jquery.gritter.css" rel="stylesheet">
        <link href="assets/global/plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
        <link href="assets/global/plugins/bower_components/horizontal-chart/build/css/horizBarChart.css" rel="stylesheet">
        <!--/ END PAGE LEVEL STYLES -->

        <!-- START @THEME STYLES -->
        <link href="assets/admin/css/reset.css" rel="stylesheet">
        <link href="assets/admin/css/layout.css" rel="stylesheet">
        <link href="assets/admin/css/components.css" rel="stylesheet">
        <link href="assets/admin/css/plugins.css" rel="stylesheet">
        <!-- <link href="assets/admin/css/themes/default.theme.css" rel="stylesheet" id="theme"> -->
        <link href="assets/admin/css/pages/dashboard-projects.css" rel="stylesheet">
        <link href="assets/admin/css/custom.css" rel="stylesheet">

    <title>Askrindo Timesheet</title>

    <!-- Bootstrap core CSS -->
    <!-- Custom styles for this template -->
    <!-- <link href="css/default.css" rel="stylesheet"> -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="css/cgx.css">
    <link rel="stylesheet" href="css/redmond/jquery-ui-1.10.3.custom.css" />
    <link rel="stylesheet" href="css/jquery-ui-timepicker-addon.css" />
    
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-ui-1.10.3.custom.min.js"></script> 
    <script src="js/jquery-ui-timepicker-addon.js"></script> 
    <script src="js/bootstrap.file-input.js"></script>

  </head>

  <body>

    <!-- Static navbar -->
    <section id="wrapper">

            <!-- START @HEADER -->
            <header id="header">

                <!-- Start header left -->
                <div class="header-left">
                    <!-- Start offcanvas left: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
                    <div class="navbar-minimize-mobile left">
                        <i class="fa fa-bars"></i>
                    </div>
                    <!--/ End offcanvas left -->

                    <!-- Start navbar header -->
                    <div class="navbar-header">

                        <!-- Start brand -->
                        <a class="navbar-brand" href="./">
                            <img class="logo" src="images/header.png" alt="brand logo">
                        </a><!-- /.navbar-brand -->
                        <!--/ End brand -->

                    </div><!-- /.navbar-header -->
                    <!--/ End navbar header -->

                    <!-- Start offcanvas right: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
                    <div class="navbar-minimize-mobile right">
                        <i class="fa fa-cog"></i>
                    </div>
                    <!--/ End offcanvas right -->

                    <div class="clearfix"></div>
                </div><!-- /.header-left -->
                <!--/ End header left -->

                <!-- Start header right -->
                <div class="header-right">
                    <!-- Start navbar toolbar -->
                    <div class="navbar navbar-toolbar">

                        <!-- Start left navigation -->
                        <ul class="nav navbar-nav navbar-left">

                            <!-- Start sidebar shrink -->
                            <li class="navbar-minimize">
                                <a href="javascript:void(0);" title="Minimize sidebar">
                                    <i class="fa fa-bars"></i>
                                </a>
                            </li>
                            <!--/ End sidebar shrink -->

                            <!-- Start form search -->
                            <li class="navbar-search">
                                <!-- Just view on mobile screen-->
                                <a href="#" class="trigger-search"><i class="fa fa-search"></i></a>
                                <form class="navbar-form">
                                    <div class="form-group has-feedback">
                                        <input type="text" class="form-control typeahead rounded" placeholder="Search for people, places and things">
                                        <button type="submit" class="btn btn-theme fa fa-search form-control-feedback rounded"></button>
                                    </div>
                                </form>
                            </li>
                            <!--/ End form search -->

                        </ul><!-- /.nav navbar-nav navbar-left -->
                        <!--/ End left navigation -->

                        <!-- Start right navigation -->
                        <ul class="nav navbar-nav pull-right"><!-- /.nav navbar-nav navbar-right -->

                        <!-- Start profile -->
                        <li class="dropdown navbar-profile">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="meta">
                                    <span class="avatar"><img src="images/no-picture.jpg" width="25px" height="20px" class="img-circle" alt="admin"></span>
                                    <span class="text hidden-xs hidden-sm text-muted">[ <?php echo user('emp_id') . ' - ' . user('emp_name'); ?> ]</span>
                                    <span class="caret"></span>
                                </span>
                            </a>
                            <!-- Start dropdown menu -->
                            <ul class="dropdown-menu animated flipInX">
                                <li class="dropdown-header">Account</li>
                                <li><a href="./index.php?m=profile"><i class="fa fa-user"></i>View profile</a></li>
                                <li class="divider"></li>
                                <li><a href="action/logout.php"><i class="fa fa-sign-out"></i>Logout</a></li>
                            </ul>
                            <!--/ End dropdown menu -->
                        </li><!-- /.dropdown navbar-profile -->
                        <!--/ End profile -->

                        </ul>
                        <!--/ End right navigation -->

                    </div><!-- /.navbar-toolbar -->
                    <!--/ End navbar toolbar -->
                </div><!-- /.header-right -->
                <!--/ End header left -->

            </header> <!-- /#header -->
        
            <aside id="sidebar-left" class="sidebar-circle">

                <!-- Start left navigation - profile shortcut -->
                <div class="sidebar-content">
                    <div class="media">
                        <a class="pull-left has-notif avatar" href="./index.php?m=profile">
                            <img src="images/no-picture.jpg" alt="admin">
                            <i class="online"></i>
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading">Hello, <span><?php echo user('emp_name'); ?></span></h4>
                            <small>Web Designer</small>
                        </div>
                    </div>
                </div><!-- /.sidebar-content -->
                <!--/ End left navigation -  profile shortcut -->

                <!-- Start left navigation - menu -->
                <ul class="sidebar-menu">

                    <li>
                        <a href="./">
                            <span class="icon"><i class="fa fa-home"></i></span>
                            <span class="text">Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="index.php?m=task">
                            <span class="icon"><i class="fa fa-tasks"></i></span>
                            <span class="text">Task</span>
                        </a>
                    </li>
                    <li class="submenu">
                        <a href="javascript:void(0);">
                            <span class="icon"><i class="fa fa-sitemap"></i></span>
                            <span class="text">Project</span>
                            <span class="arrow"></span>
                        </a>
                        <ul>
                            <?php
                  
                            $rsx = mysql_query("SELECT * FROM project WHERE active = 'Y' ORDER BY project_name", $APP_CONNECTION);
                            while ($dtx = mysql_fetch_array($rsx, MYSQL_ASSOC)) {
                                echo "<li><a href='index.php?m=project-ts&id={$dtx['project_id']}'>{$dtx['project_name']}</a></li>\n";
                            }
                            mysql_free_result($rsx);
                            
                            ?>
                        </ul>
                    </li>
                    <li>
                        <a href="index.php?m=task">
                            <span class="icon"><i class="fa fa-clone"></i></span>
                            <span class="text">My Timesheet</span>
                        </a>
                    </li>
                    <li class="submenu">
                        <a href="javascript:void(0);">
                            <span class="icon"><i class="fa fa-book"></i></span>
                            <span class="text">Report</span>
                            <span class="arrow"></span>
                        </a>
                        <ul>
                            <?php if (has_privilege('project_manager')) { ?>
                              <li><a href="index.php?m=att">Attendance Report</a></li>
                              <li><a href="index.php?m=project-summary">Project Summary</a></li>
                            <?php } ?>
                            <li><a href="index.php?m=mts">Monthly Timesheet</a></li>
                            <li><a href="index.php?m=ts">Timesheet Report</a></li>
                            <li><a href="index.php?m=activity-board">Activity Board</a></li>
                        </ul>
                    </li>
                    <?php if (is_admin()) { ?>
                    <li class="submenu">
                        <a href="javascript:void(0);">
                            <span class="icon"><i class="fa fa-wrench"></i></span>
                            <span class="text">References</span>
                            <span class="arrow"></span>
                        </a>
                        <ul>
                            <li><a href="index.php?m=emp">Employee</a></li>
                            <li><a href="index.php?m=project">Project</a></li>
                            <li><a href="index.php?m=activity">Activity</a></li>
                        </ul>
                    </li>
                    <li class="submenu">
                        <a href="javascript:void(0);">
                            <span class="icon"><i class="fa fa-archive"></i></span>
                            <span class="text">Administration</span>
                            <span class="arrow"></span>
                        </a>
                        <ul>
                            <li><a href="index.php?m=user">User Management</a></li>
                            <li><a href="index.php?m=pelamar">Job Applicants</a></li>
                        </ul>
                    </li>
                    <?php } ?>

                    

                </ul><!-- /.sidebar-menu -->
                <!--/ End left navigation - menu -->

                <!-- Start left navigation - footer -->
                <div class="sidebar-footer hidden-xs hidden-sm hidden-md">
                    <a id="setting" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Setting"><i class="fa fa-cog"></i></a>
                    <a id="fullscreen" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Fullscreen"><i class="fa fa-desktop"></i></a>
                    <a id="lock-screen" data-url="page-signin.html" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Lock Screen"><i class="fa fa-lock"></i></a>
                    <a id="logout" data-url="action/logout.php" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Logout"><i class="fa fa-power-off"></i></a>
                </div><!-- /.sidebar-footer -->
                <!--/ End left navigation - footer -->

            </aside><!-- /#sidebar-left -->

    <div id="page-content">
    
      <div class="body-content animated fadeIn">

        <?php
        $_REQUEST['m'] = empty($_REQUEST['m']) ? 'dashboard' : $_REQUEST['m'];
        
        if (file_exists("include/{$_REQUEST['m']}.php")) {
            include("include/{$_REQUEST['m']}.php");
        } else {
            echo "File not found!";
        }
        ?>

      </div>

    </div> 
        <!-- /container -->
          </section><!-- /#wrapper -->
        <!--/ END WRAPPER -->

        <!-- START @BACK TOP -->
        <div id="back-top" class="animated pulse circle">
            <i class="fa fa-angle-up"></i>
        </div><!-- /#back-top -->
        <!--/ END BACK TOP -->

    <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- START @CORE PLUGINS -->
        <script src="assets/global/plugins/bower_components/jquery-cookie/jquery.cookie.js"></script>
        <script src="assets/global/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="assets/global/plugins/bower_components/typehead.js/dist/handlebars.js"></script>
        <script src="assets/global/plugins/bower_components/typehead.js/dist/typeahead.bundle.min.js"></script>
        <script src="assets/global/plugins/bower_components/jquery-nicescroll/jquery.nicescroll.min.js"></script>
        <script src="assets/global/plugins/bower_components/jquery.sparkline.min/index.js"></script>
        <script src="assets/global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js"></script>
        <script src="assets/global/plugins/bower_components/ionsound/js/ion.sound.min.js"></script>
        <script src="assets/global/plugins/bower_components/bootbox/bootbox.js"></script>
        <!--/ END CORE PLUGINS -->

        <!-- START @PAGE LEVEL PLUGINS -->
        <script src="assets/global/plugins/bower_components/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js"></script>
        <script src="assets/global/plugins/bower_components/jquery.gritter/js/jquery.gritter.min.js"></script>
        <script src="assets/global/plugins/bower_components/underscore/underscore-min.js"></script>
        <script src="assets/global/plugins/bower_components/jsTimezoneDetect/jstz.min.js"></script>
        <script src="assets/global/plugins/bower_components/bootstrap-calendar/js/calendar.min.js"></script>
        <script src="assets/global/plugins/bower_components/raphael/raphael-min.js"></script>
        <script src="assets/global/plugins/bower_components/horizontal-chart/build/js/jquery.horizBarChart.min.js"></script>
        <script src="assets/global/plugins/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
        <script src="assets/global/plugins/bower_components/counter-up/jquery.counterup.min.js"></script>
        <!--/ END PAGE LEVEL PLUGINS -->

        <!-- START @PAGE LEVEL SCRIPTS -->
        <script src="assets/admin/js/apps.js"></script>
        <script src="assets/admin/js/demo.js"></script>
        <!--/ END PAGE LEVEL SCRIPTS -->
        <!--/ END JAVASCRIPT SECTION -->

        <!-- START GOOGLE ANALYTICS -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-55892530-1', 'auto');
            ga('send', 'pageview');

        </script>
        <!--/ END GOOGLE ANALYTICS -->
  </body>
</html>

<?php

/*
 * file
 * Azwari Nugraha <nugraha@pt-gai.com>
 * Sep 14, 2014 2:02:22 PM
 */

require_once 'init.php';

if (!authenticated()) die;

$file = explode('/', $_SERVER['QUERY_STRING']);
$frec = npl_fetch_table("SELECT * FROM t_file WHERE id_t_file = '{$file[1]}' AND file_key = '{$file[2]}'");
$path = $APP_ATTACHMENT . '/' . $frec['id_m_file'] . '/' . $frec['id_ref'] . '/' . $file[1];

header("Content-Type: {$frec['file_mime']}");
header("Content-Length: {$frec['file_size']}");
header("Content-Disposition: attachment; filename=\"{$frec['file_name']}\"");

echo file_get_contents($path);
exit;

?>
<?php

/*
 * Default Library
 * Azwari Nugraha <nugraha@duabelas.org>
 * Oct 1, 2012 4:47:02 PM
 */

function has_privilege($priv_id) {
    return user('is_' . $priv_id) == 'Y';
}

function login($id, $password) {
    global $APP_CONNECTION, $APP_ID;
    $rsx = mysql_query(
            "SELECT * " .
            "FROM emp " .
            "WHERE email = '" . mysql_real_escape_string($id) . "' " .
            "AND password = '" . md5($password) . "'",
            $APP_CONNECTION);
    if ($dtx = mysql_fetch_array($rsx)) {
        
        if ($dtx['active'] == 'N') {
            unset($_SESSION[$APP_ID]);
            $_SESSION[$APP_ID]['auth-message'] = "Your account is not active, please contact system administrator";
            return FALSE;
        } else {
            // update
            mysql_query(
                    "UPDATE emp SET " .
                    "last_login = NOW(), " .
                    "last_ip = '{$_SERVER['REMOTE_ADDR']}' " .
                    "WHERE emp_id = '{$dtx['emp_id']}'",
                    $APP_CONNECTION);

            $user = npl_fetch_table("SELECT * FROM emp WHERE emp_id = '{$dtx['emp_id']}'");
            unset($_SESSION[$APP_ID]);
            $_SESSION[$APP_ID]['authenticated'] = 1;
            $_SESSION[$APP_ID]['user'] = $user;
            return TRUE;
        }
        
    } else {
        unset($_SESSION[$APP_ID]);
        $_SESSION[$APP_ID]['auth-message'] = "Invalid user/password";
        return FALSE;
    }
}

function authenticated() {
    global $APP_ID;
    return $_SESSION[$APP_ID]['authenticated'] == 1;
}

function user($field = 'emp_id') {
    global $APP_ID;
    return $_SESSION[$APP_ID]['user'][$field];
}

function user_image($user_id = NULL) {
    global $APP_BASE_DIR, $APP_BASE_URL;
    $user_id = is_null($user_id) ? user() : $user_id;
    $user_id = str_pad($user_id, 3, '0', STR_PAD_LEFT);
    if (file_exists($APP_BASE_DIR . '/images/profile/' . $user_id . '.jpg')) {
        return $APP_BASE_URL . '/images/profile/' . $user_id . '.jpg?' . substr(session_id(), 0, 8);
    } else {
        return $APP_BASE_URL . '/images/no-picture.jpg';
    }
}

function is_admin() {
    return user('is_admin') == 'Y';
}

function npl_emptydate($date) {
    return empty($date) || $date == '0000-00-00' || $date == '0000-00-00 00:00:00';
}

function npl_dmy2ymd($dmy) {
    $arr = explode("-", $dmy);
    $out = $arr[2] . '-' . $arr[1] . '-' . $arr[0];
    $out = npl_emptydate($out) || $out == '--' ? '0000-00-00' : $out;
    return $out;
}

function npl_fetch_table($sql) {
    $r1 = mysql_query($sql, $GLOBALS['APP_CONNECTION']);
    if (mysql_num_rows($r1) == 0) {
        $ret = null;
    } else {
        if(($d1 = mysql_fetch_array($r1)) !== FALSE) {
            $ret = $d1;
        } else {
            $ret = NULL;
        }
    }
    mysql_free_result($r1);
    return $ret;
}

function npl_format_date($date) {
    if (npl_emptydate($date)) return NULL;
    $format = strlen($GLOBALS['APP_DATE_FORMAT']) > 0 ? $GLOBALS['APP_DATE_FORMAT'] : 'd-M-Y';
    return date($format, strtotime($date));
}

function npl_format_period($date) {
    if (npl_emptydate($date)) return NULL;
    return date('m-Y', strtotime($date));
}

function npl_period2mysqldate($period) {
    $a = explode('-', $period);
    return $a[1] . '-' . $a[0] . '-01';
}

function valid_url($url)
{
    return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url);
}

function valid_email_address($email) {
    // First, we check that there's one @ symbol,
    // and that the lengths are right.
    if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
        // Email invalid because wrong number of characters
        // in one section or wrong number of @ symbols.
        return false;
    }
    // Split it into sections to make life easier
    $email_array = explode("@", $email);
    $local_array = explode(".", $email_array[0]);
    for ($i = 0; $i < sizeof($local_array); $i++) {
        if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
            return false;
        }
    }
    // Check if domain is IP. If not,
    // it should be valid domain name
    if (!ereg("^\\[?[0-9\\.]+\\]?$", $email_array[1])) {
        $domain_array = explode(".", $email_array[1]);
        if (sizeof($domain_array) < 2) {
            return false; // Not enough parts to domain
        }
        for ($i = 0; $i < sizeof($domain_array); $i++) {
            if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$",$domain_array[$i])) {
                return false;
            }
        }
    }
    return true;
}

function replace_template($template, $vars) {
    foreach ($template as $tk => $tv) {
        $result[$tk] = $tv;
        foreach ($vars as $vk => $vv) {
            $result[$tk] = str_replace("<\${$vk}\$>", $vv, $result[$tk]);
        }
    }
    return $result;
}

function is_task_owner($task_id) {
    $res = npl_fetch_table("SELECT emp_id FROM task WHERE task_id = '{$task_id}'");
    return $res['emp_id'] == user();
}

function is_task_member($task_id) {
    $res = npl_fetch_table("SELECT task_assign_id FROM task_assign "
            . "WHERE task_id = '{$task_id}' "
            . "AND emp_id = '" . user() . "'");
    return $res['task_assign_id'] > 0;
}

function format_emp($arr) {
    $res = $arr['emp_name'] . ' <span class="text-muted">&lt;' . $arr['email'] . '&gt</span>';
    return $res;
}

?>
<?php

/**
 * Azwari Nugraha
 * Fri Sep 23 15:28:51 CST 2011
 */

function image_crop_max($src, $dst, $width, $height, $quality = 90) {
    if (!file_exists($src)) return FALSE;

    $info = getimagesize($src);
    $srcW = $info[0];
    $srcH = $info[1];
    $srcWHratio = $srcW / $srcH;

    $dstWHratio = $width / $height;
    $dstHWratio = $height / $width;

    if ($srcWHratio >= $dstWHratio) {
        $startX = ($srcW - ($srcH * $dstWHratio)) / 2;
        $startY = 0;
        $cropW = $srcH * $dstWHratio;
        $cropH = $srcH;
    } else {
        $startX = 0;
        $startY = ($srcH - ($srcW * $dstHWratio)) / 2;
        $cropW = $srcW;
        $cropH = $srcW * $dstHWratio;
    }

    $type = getimagesize($src);
    $type = $type[2];
    switch($type){
        case 1: $img_src=imagecreatefromgif($src);break;
        case 2: $img_src=imagecreatefromjpeg($src);break;
        case 3: $img_src=imagecreatefrompng($src);break;
        case 6: $img_src=imagecreatefromwbmp($src);break;
        default: return false;
    }

    $img_dst = ImageCreateTrueColor($width, $height);
    imagefill($img_dst,0,0,imagecolorallocate($img_dst,255,255,255)); // fill the background
    imagecopyresampled($img_dst, $img_src, 0, 0, $startX, $startY, $width, $height, $cropW, $cropH);
    return imagejpeg($img_dst, $dst, $quality);
}

function image_reduce_size($file, $maxWidth, $maxHeight) {
    $src = imagecreatefromjpeg($file);
    list($width,$height) = getimagesize($file);
    if (($width/$maxWidth) > ($height/$maxHeight)) {
        $newwidth = $maxWidth;
        $newheight = ($height/$width) * $maxWidth;
    }else {
        $newheight = $maxHeight;
        $newwidth = ($width/$height) * $maxHeight;
    }
    $tmp = imagecreatetruecolor($newwidth, $newheight);
    imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
    if (imagejpeg($tmp, $file, 90) && imagedestroy($src) && imagedestroy($tmp)) {
        return true;
    }else {
        return false;
    }
}

?>
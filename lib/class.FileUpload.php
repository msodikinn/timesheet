<?php

/*
 * class
 * Azwari Nugraha <nugraha@pt-gai.com>
 * Sep 12, 2014 4:32:39 PM
 */

class FileUpload {
    
    private $base_path;
    private $connection;

    public function __construct($connection = NULL, $base_path = NULL) {
        if ($GLOBALS['APP_ATTACHMENT']) $this->base_path = $GLOBALS['APP_ATTACHMENT'];
        if ($GLOBALS['APP_CONNECTION']) $this->connection = $GLOBALS['APP_CONNECTION'];
        if ($connection) $this->connection = $connection;
        if ($base_path) $this->base_path = $base_path;
        if (empty($this->base_path)) $this->base_path = './FileUpload';
        if (!file_exists($this->base_path)) mkdir ($this->base_path, 0777, TRUE);
    }
    
    public function get_base_path() {
        return $this->base_path;
    }

    public function set_base_path($base_path) {
        $this->base_path = $base_path;
    }
    
    public function upload($id_m_file, $id_ref, $field) {
        $rsx = mysql_query(
                "SELECT * FROM m_file WHERE id_m_file = '{$id_m_file}'",
                $this->connection);
        $m_file = mysql_fetch_array($rsx, MYSQL_ASSOC);
        mysql_free_result($rsx);
        if (!$m_file) return FALSE;
        
        $path = "{$this->base_path}/{$id_m_file}/{$id_ref}";
        mkdir($path, 0777, TRUE);
        
        $is_multi = is_array($_FILES[$field]['name']);
        if ($is_multi) {
            foreach ($_FILES[$field]['name'] as $index => $name) {
                $this->_upload($id_m_file, $id_ref,
                            array(
                                'name'      => $name,
                                'type'      => $_FILES[$field]['type'][$index],
                                'error'     => $_FILES[$field]['error'][$index],
                                'size'      => $_FILES[$field]['size'][$index],
                                'tmp_name'  => $_FILES[$field]['tmp_name'][$index]
                            )
                        );
            }
        } else {
            $this->_upload($id_m_file, $id_ref, $_FILES[$field]);
        }
    }
    
    private function _upload($id_m_file, $id_ref, $field) {
        if ($field['error'] > 0) return FALSE;
        $key = substr(md5(mktime()), 0, 8);
        if (mysql_query("INSERT INTO t_file"
                . "(id_m_file, id_ref, file_name, file_mime, file_size, file_key) VALUES"
                . "('{$id_m_file}', '{$id_ref}', '{$field['name']}', '{$field['type']}', '{$field['size']}', '{$key}')",
                $this->connection)) {
            $id_t_file = mysql_insert_id($this->connection);
        } else {
            return FALSE;
        }
        
        $path = "{$this->base_path}/{$id_m_file}/{$id_ref}";
        if (move_uploaded_file($field['tmp_name'], "{$path}/{$id_t_file}")) {
            return TRUE;
        } else {
            mysql_query("DELETE FROM t_file WHERE id_t_file = '{$id_t_file}'",
                    $this->connection);
            return FALSE;
        }
        
    }
    
    public function file_delete($id_t_file) {
        $rsx = mysql_query(
                "SELECT * FROM t_file WHERE id_t_file = '{$id_t_file}'",
                $this->connection);
        $t_file = mysql_fetch_array($rsx, MYSQL_ASSOC);
        mysql_free_result($rsx);
        if (!$t_file) return FALSE;
        $filename = "{$this->base_path}/{$t_file['id_m_file']}/{$t_file['id_ref']}/{$id_t_file}";
        if (unlink($filename)) {
            return mysql_query("DELETE FROM t_file WHERE id_t_file = '{$id_t_file}'", $this->connection);
        } else {
            return FALSE;
        }
    }
    
    public function file_get_array($id_m_file, $id_ref) {
        $rsx = mysql_query(
                "SELECT * FROM t_file "
                . "WHERE id_m_file = '{$id_m_file}' AND id_ref = '{$id_ref}'",
                $this->connection);
        $ret = array();
        while ($dtx = mysql_fetch_array($rsx, MYSQL_ASSOC)) {
            $ret[] = array(
                'id' => $dtx['id_t_file'],
                'name' => $dtx['file_name'],
                'mime' => $dtx['file_mime'],
                'size' => $dtx['file_size'],
                'key' => $dtx['file_key'],
                'href' => 'fu.get.php?/' .
                    $dtx['id_t_file'] . '/' . 
                    $dtx['file_key'] . '/' . 
                    urlencode($dtx['file_name']),
                'del' => 'action/fu.del.php?/' .
                    $dtx['id_t_file'] . '/' . 
                    $dtx['file_key'] . '/' . 
                    urlencode($dtx['file_name'])
            );
        }
        mysql_free_result($rsx);
        return $ret;
    }
    
    public function html_file_list($id_m_file, $id_ref, $allow_edit = FALSE, $inline = FALSE) {
        $arr = $this->file_get_array($id_m_file, $id_ref);
        if (count($arr) == 0) return '';
        foreach ($arr as $file) {
            $del = "action/fu.del.php?id={$file['id']}&key={$file['key']}&back=" . urlencode($_SERVER['REQUEST_URI']);
            if ($inline) {
                $ret .= "<nobr style='padding-right: 20px;'>";
                $ret .= "<a href='{$file['href']}'>";
                $ret .= "<span class='glyphicon glyphicon-file'></span> ";
                $ret .= $file['name'];
                $ret .= "</a>";
                if ($allow_edit) {
                    $ret .= " <span onclick=\"if(confirm('Mengapus file \\'{$file['name']}\\'?')){window.location = '{$del}';}\" style='cursor: pointer;' class='glyphicon glyphicon-remove-circle text-danger'></span>";
                }
                $ret .= "</nobr> ";
            } else {
                $ret .= "<div>";
                $ret .= "<a href='{$file['href']}'>";
                $ret .= "<span class='glyphicon glyphicon-file'></span> ";
                $ret .= $file['name'];
                $ret .= "</a>";
                if ($allow_edit) {
                    $ret .= " <span onclick=\"if(confirm('Mengapus file \\'{$file['name']}\\'?')){window.location = '{$del}';}\" style='cursor: pointer;' class='glyphicon glyphicon-remove-circle text-danger'></span>";
                }
                $ret .= "</div>";
            }
        }
        return $ret;
    }

    public function html_form($field_name, $id_m_file, $id_ref = NULL) {
        $rsx = mysql_query(
                "SELECT * FROM m_file "
                . "WHERE id_m_file = '{$id_m_file}'", $this->connection);
        $m_file = mysql_fetch_array($rsx, MYSQL_ASSOC);
        mysql_free_result($rsx);
        
        $ret  = "<div class='panel panel-default'>";
        if ($m_file['title']) {
            $ret .= "<div class='panel-heading'><h3 class='panel-title'>{$m_file['title']}</h3></div>";
        }
        $ret .= "<div class='panel-body'>";
        if ($id_ref) {
            $rsx = mysql_query(
                    "SELECT * FROM t_file "
                    . "WHERE id_m_file = '{$id_m_file}' AND id_ref = '{$id_ref}'",
                    $this->connection);
            while ($dtx = mysql_fetch_array($rsx, MYSQL_ASSOC)) {
                $ret .= "<div><a href='file.php'>";
                $ret .= "<span class='glyphicon glyphicon-file'></span> ";
                $ret .= $dtx['file_name'];
                $ret .= "</a>";
                $ret .= "<button onclick=\"if(confirm('Menghapus file {$dtx['file_name']}?')){xajax_fuDelete('{$dtx['id_t_file']}', '{$dtx['file_key']}'); return false;}\" class='btn btn-link'><span class='glyphicon glyphicon-remove text-danger'></span></button>";
                $ret .= "</div>";
            }
            mysql_free_result($rsx);
        }
        $ret .= "<br>";
        $ret .= "<input name='{$field_name}[]' type='file' title='Pilih File...' />";
        $ret .= "</div>";
        $ret .= "</div>";

        return $ret;
    }
    
}

?>
<?php

/*
 * init
 * Azwari Nugraha <nugraha@duabelas.org>
 * Jan 28, 2013 12:47:05 PM
 */
error_reporting(E_ALL);
$root_path = dirname(__FILE__);
require_once $root_path . '/config.php';
session_start();

date_default_timezone_set($APP_TIMEZONE);
ini_set('track_errors', true);
set_time_limit(0);
set_include_path("." .
    PATH_SEPARATOR . $APP_BASE_DIR .
    PATH_SEPARATOR . $APP_BASE_DIR . '/lib' .
    PATH_SEPARATOR . $APP_BASE_DIR . '/lib/pear');

require_once $APP_BASE_DIR . '/lib/default.php';
require_once $APP_BASE_DIR . '/lib/cgx.php';
require_once $APP_BASE_DIR . '/lib/class.FileUpload.php';

// database connection
$APP_DSN = "mysql://{$APP_DB_USER}:{$APP_DB_PASSWORD}@{$APP_DB_HOST}:{$APP_DB_PORT}/{$APP_DB_NAME}";
@$APP_CONNECTION = mysql_pconnect("{$APP_DB_HOST}:{$APP_DB_PORT}", $APP_DB_USER, $APP_DB_PASSWORD);
if ($APP_CONNECTION) {
    if (! mysql_select_db($APP_DB_NAME, $APP_CONNECTION)) {
        die(mysql_error($APP_CONNECTION));
    }
} else {
    die($php_errormsg);
}
@mysql_query("SET NAMES 'utf8'");
@mysql_query("SET time_zone = 'Asia/Jakarta'");

// cgx related config
$cgx_connection = $APP_CONNECTION;
$cgx_dsn = $APP_DSN;
$cgx_max_rows = $APP_DATAGRID_MAXROWS;
$cgx_TableAttribs = array('class'=>'table table-striped table-hover table-condensed');

//$cgx_TableAttribs = array('width' => '100%', 'cellspacing' => '1', 'cellpadding' => '2');
//$cgx_HeaderAttribs = array('class' => 'datagrid_header');
//$cgx_EvenRowAttribs = array('bgcolor' => '#FFFFFF', 'style' => 'font-size: 11px;');
//$cgx_OddRowAttribs = array('bgcolor' => '#EEEEEE', 'style' => 'font-size: 11px;');
$cgx_RendererOptions = array('sortIconASC' => ' <span style="color: darkorange; font-size: 1.2em;" class="glyphicon glyphicon glyphicon-sort-by-alphabet"></span>', 'sortIconDESC' => ' <span style="color: darkorange; font-size: 1.2em;" class="glyphicon glyphicon glyphicon-sort-by-alphabet-alt"></span>');

$TS_TYPE = array(
    'W' => 'Work',
    'S' => 'Sick',
    'P' => 'Permission',
    'L' => 'Leave'
);

$TASK_PRIORITY = array(
    1   =>  'Critical (1)',
    2   =>  'High (2)',
    3   =>  'Normal (3)',
    4   =>  'Low (4)',
    5   =>  'Trivial (5)'
);

$TASK_STATUS = array(
    'C' =>  'Created',
    'O' =>  'In Progress',
    'F' =>  'Finished',
    'X' =>  'Cancelled',
    'L' =>  'Closed'
);

$PROJECT_TYPE = array(
    'C' =>  'Custom Development',
    'E' =>  'ERP Implementation',
    'R' =>  'Resources / Outsources',
    'G' =>  'General'
);

// xajax
/*
if (isset($_SERVER['HTTP_USER_AGENT'])) {
    require_once $APP_BASE_DIR . '/lib/xajax_core/xajax.inc.php';
    $xajax = new xajax();
    if ($_REQUEST['dbg'] == 1) $xajax->setFlag('debug', TRUE);
    $xajax->configure('javascript URI', $APP_BASE_URL . '/js/');
    require_once $APP_BASE_DIR . '/xajax/default.php';
    if (file_exists($APP_BASE_DIR . '/xajax/' . $_REQUEST['m'] . '.php')) {
        require_once $APP_BASE_DIR . '/xajax/' . $_REQUEST['m'] . '.php';
    }
    $xajax->processRequest();
}
*/

?>

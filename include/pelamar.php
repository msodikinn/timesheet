<?php

/**
 * (c) Azwari Nugraha <nugraha@duabelas.org>
 * 03/03/2014 12:20:47
 */

if (!has_privilege('admin')) return;

echo "<div class='page-header'><h3>Job Applicants Data</h3></div>";

function cgx_format_sex($data) {
    $arr = array('M' => 'Male', 'F' => 'Female');
    return $arr[$data['record'][$data['fieldName']]];
}

function validate_email($data){
    if (preg_match("`^[a-z0-9!#$%&'*+\/=?^_\`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_\`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$`i", $data)){
        return $data;
    }
    else{
        //alert("Email invalid");
    }
}

function cgx_edit($data) {
    $href = "index.php?&m={$_REQUEST['m']}&pkey[no_id]={$data['record']['no_id']}";
    $out = "<a href='{$href}'><img title='Edit this row' src='images/icon_edit.png' border='0'></a>";
    return $out;
}

function cgx_delete($data) {
    $href  = "javascript:if(confirm('Delete this row?')){window.location='action/pelamar.php";
    $href .= "?backvar=index.php%253F%2526m%253Dpelamar&mode=delete&pkey[no_id]={$data['record']['no_id']}';}";
    $out = "<a href=\"{$href}\"><img title='Delete this row' src='images/icon_delete.png' border='0'></a>";
    return $out;
}

if (strlen($_REQUEST['pkey']['no_id']) > 0) {
    $cgx_id = $_REQUEST['id'];
    $cgx_data = cgx_fetch_table("SELECT * FROM pelamar WHERE pelamar.no_id = '" . mysql_escape_string($_REQUEST['pkey']['no_id']) . "'");

    $cgx_data['birth_date'] = cgx_emptydate($cgx_data['birth_date']) ? '' : cgx_ddmmyyyy($cgx_data['birth_date']);

    echo "<div class='panel panel-default'>\n";
    echo "<div class='panel-body'>\n";
    echo "<form class='form-horizontal' role='form' action='action/pelamar.php' method='post'>\n";
    echo "<input type='hidden' name='backvar' value='" . urlencode("index.php?&m={$_REQUEST['m']}") . "'>\n";
    echo "<input type='hidden' name='mode' value='" . ($_REQUEST['pkey']['no_id'] == '0' ? 'new' : 'update') . "'>\n";
    echo "<input type='hidden' name='pkey[no_id]' value=\"{$_REQUEST['pkey']['no_id']}\">\n";
    echo "<input type='hidden' name='table' value='pelamar'>\n";

    if ($_SESSION[$GLOBALS['APP_ID']]['pelamar']['error']) {
        echo "<div class='alert alert-danger'>{$_SESSION[$GLOBALS['APP_ID']]['pelamar']['error']}</div>";
        unset($_SESSION[$GLOBALS['APP_ID']]['pelamar']['error']);
    }

    if ($_SESSION[$GLOBALS['APP_ID']]['pelamar']['info']) {
        echo "<div class='alert alert-success'>{$_SESSION[$GLOBALS['APP_ID']]['pelamar']['info']}</div>";
        unset($_SESSION[$GLOBALS['APP_ID']]['pelamar']['info']);
    }

    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_no_id'>No ID</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo "        <input class='form-control' id='data_no_id' name='data[no_id]' type='text' value=\"{$cgx_data['no_id']}\" maxlength='20' style='text-align: left;width: 20em;' " . ($_REQUEST['pkey']['no_id'] == '0' ? '' : ' disabled') . " />\n";
    echo "        </div>\n";
    echo "        </div>\n";
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_name'>Name</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo "        <input class='form-control' id='data_name' name='data[name]' type='text' value=\"{$cgx_data['name']}\" maxlength='100' style='text-align: left;width: 51em;'  />\n";
    echo "        </div>\n";
    echo "        </div>\n";
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_birth_place'>Birth Place</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo "        <input class='form-control' id='data_birth_place' name='data[birth_place]' type='text' value=\"{$cgx_data['birth_place']}\" maxlength='30' style='text-align: left;width: 31em;'  />\n";
    echo "        </div>\n";
    echo "        </div>\n";
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_birth_date'>Birth Date</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo "        <input class='form-control' id='data_birth_date' name='data[birth_date]' type='text' value=\"{$cgx_data['birth_date']}\" maxlength='10' style='text-align: left;width: 10em;'  />\n";
    echo "        </div>\n";
    echo "        </div>\n";
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_sex'>Sex</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo cgx_form_select('data[sex]', array('M' => 'Male', 'F' => 'Female'), $cgx_data['sex'], FALSE, "id='data_sex'");
    echo "        </div>\n";
    echo "        </div>\n";
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_address'>Address</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo "        <input class='form-control' id='data_address' name='data[address]' type='text' value=\"{$cgx_data['address']}\" maxlength='100' style='text-align: left;width: 51em;'  />\n";
    echo "        </div>\n";
    echo "        </div>\n";
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_phone'>Phone</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo "        <input class='form-control' id='data_phone' name='data[phone]' type='text' value=\"{$cgx_data['phone']}\" maxlength='15' style='text-align: left;width: 21em;'  />\n";
    echo "        </div>\n";
    echo "        </div>\n";
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_email'>Email</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo "        <input class='form-control' id='data_email' name='data[email]' type='text' value=\"{$cgx_data['email']}\" maxlength='50' style='text-align: left;width: 51em;'  />\n";
    echo "        </div>\n";
    echo "        </div>\n";
    echo "        <div class='form-group'>\n";
    echo "        <div class='col-sm-offset-2 col-sm-10'>\n";
    echo "        <input class='btn btn-primary' type='submit' value='Submit'>\n";
    echo "        <input class='btn btn-warning' type='button' value='Back' onclick=\"window.location = 'index.php?&m=pelamar';\">\n";
    echo "        </div>\n";
    echo "        </div>\n";
    echo "</form>\n";
    echo "</div>\n";
    echo "</div>\n";

?>
<script type='text/javascript'>
<!--
$(function() {
    $("#data_birth_date").datepicker({dateFormat: 'dd-mm-yy'});
});
//-->
</script>
<?php
} else {
    require_once 'Structures/DataGrid.php';
    require_once 'HTML/Table.php';

    echo "<div class='panel panel-default'>";
    if (is_array($_SESSION[$GLOBALS['APP_ID']]['pelamar']['columns'])) {
        $cgx_def_columns = $_SESSION[$GLOBALS['APP_ID']]['pelamar']['columns'];
    } else {
        $cgx_def_columns = array(
            'no_id' => 1,
            'name' => 1,
            'birth_place' => 1,
            'birth_date' => 1,
            'sex' => 1,
            'address' => 1,
            'phone' => 1,
            'email' => 1,
        );
        $_SESSION[$GLOBALS['APP_ID']]['pelamar']['columns'] = $cgx_def_columns;
    }

    $cgx_sql = "SELECT * FROM pelamar";
    $cgx_datagrid = new Structures_DataGrid($cgx_max_rows);
    $cgx_options = array('dsn' => $cgx_dsn);

    $cgx_search = $_REQUEST['q'];

    echo "<div class='panel-heading'>";
    echo "<form name='frmFILTER' action='{$_SERVER['SCRIPT_NAME']}'>\n";
    echo "<input type='hidden' name='m' value='{$_REQUEST['m']}'>\n";
    echo "<table id='bar' class='datagrid_bar' width='100%'><tr>\n";
    echo "<td align='right'><input type='text' size='20' name='q' value=\"{$cgx_search}\"></td>\n";
    echo "<td width='1'><input title='Search' type='image' src='images/icon_search.png' border='0' style='padding-right: 20px;'></td>\n";
    echo "<td></td>\n";
    echo "<td width='20'></td>\n";
    if (has_privilege('admin')) {
        echo "<td width='1' class='datagrid_bar_icon'><a title='New record' href='index.php?&m={$_REQUEST['m']}&pkey[no_id]=0'><img border='0' src='images/icon_add.png'></a></td>\n";
    } else {
        echo "<td width='1' class='datagrid_bar_icon'><img border='0' src='images/icon_add_dis.png'></td>\n";
    }
    echo "<td width='1' class='datagrid_bar_icon'><a title='Export all (CSV)' href='action/pelamar.php?mode=export-all'><img border='0' src='images/icon_csv.png'></a></td>\n";
    echo "<td width='1' class='datagrid_bar_icon'><a title='Customize columns' href='javascript:customizeColumn(true);'><img border='0' src='images/icon_columns.png'></a></td>\n";
    echo "</tr></table>\n";
    echo "</form>\n";
    echo "</div>";

    echo "<div id='columns' class='panel-body' style='display: none;'>\n";
    echo "<form name='frmCUSTOMIZE' action='action/cgx_customize.php' method='post'>\n";
    echo "<input type='hidden' name='back' value='" . urlencode($_SERVER['REQUEST_URI']) . "'>\n";
    echo "<input type='hidden' name='dg_name' value='pelamar'>\n";
    echo "<input type='hidden' name='col[no_id]' value='on'>\n";
    echo "<input type='hidden' name='col[name]' value='on'>\n";
    echo "<div class='form-group'>";
    echo "<label class='checkbox-inline'><input disabled checked type='checkbox' name='col[no_id]'> No ID</label>";
    echo "<label class='checkbox-inline'><input disabled checked type='checkbox' name='col[name]'> Name</label>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['birth_place'] == 1 ? ' checked' : '') . " type='checkbox' name='col[birth_place]'> Birth Place</label>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['birth_date'] == 1 ? ' checked' : '') . " type='checkbox' name='col[birth_date]'> Birth Date</label>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['sex'] == 1 ? ' checked' : '') . " type='checkbox' name='col[sex]'> Sex</label>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['address'] == 1 ? ' checked' : '') . " type='checkbox' name='col[address]'> Address</label>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['phone'] == 1 ? ' checked' : '') . " type='checkbox' name='col[phone]'> Phone</label>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['email'] == 1 ? ' checked' : '') . " type='checkbox' name='col[email]'> Email</label>";
    echo "</div>";
    echo "<input class='btn btn-primary' type='submit' value='Update'>\n";
    echo "<input class='btn btn-warning' type='button' value='Cancel' onclick='customizeColumn(false);'>\n";
    echo "</form>\n";
    echo "</div>\n";
?>
<script type="text/javascript">
<!--
function customizeColumn(s) {
    var divCols = document.getElementById('columns');
    var divBar = document.getElementById('bar');
    if (s) {
        divCols.style.display = 'block';
        divBar.style.display = 'none';
    } else {
        window.location = window.location;
    }
}
//-->
</script>
<?php
    $cgx_sql .= " WHERE ( no_id LIKE '%{$cgx_search}%' OR name LIKE '%{$cgx_search}%' OR email LIKE '%{$cgx_search}%' OR phone LIKE '%{$cgx_search}%')";
    if ($_SESSION[$GLOBALS['APP_ID']]['pelamar']['error']) {
        echo "<div class='alert alert-danger'>{$_SESSION[$GLOBALS['APP_ID']]['pelamar']['error']}</div>";
        unset($_SESSION[$GLOBALS['APP_ID']]['pelamar']['error']);
    }

    if ($_SESSION[$GLOBALS['APP_ID']]['pelamar']['info']) {
        echo "<div class='alert alert-success'>{$_SESSION[$GLOBALS['APP_ID']]['pelamar']['info']}</div>";
        unset($_SESSION[$GLOBALS['APP_ID']]['pelamar']['info']);
    }


    $cgx_test = $cgx_datagrid->bind($cgx_sql, $cgx_options);
    if (PEAR::isError($cgx_test)) {
        echo $cgx_test->getMessage();
    }

    if ($cgx_def_columns['no_id'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('No ID', 'no_id', 'no_id', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['name'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Name', 'name', 'name', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['birth_place'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Birth Place', 'birth_place', 'birth_place', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['birth_date'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Birth Date', 'birth_date', 'birth_date', array('align' => 'left'), NULL, "cgx_format_date()"));
    if ($cgx_def_columns['sex'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Sex', 'sex', 'sex', array('align' => 'left'), NULL, "cgx_format_sex()"));
    if ($cgx_def_columns['address'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Address', 'address', 'address', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['phone'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Phone', 'phone', 'phone', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['email'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Email', 'email', 'email', array('align' => 'left'), NULL, NULL));
    if (has_privilege('admin')) $cgx_datagrid->addColumn(new Structures_DataGrid_Column(NULL, NULL, NULL, array('align' => 'center', 'width' => '1'), NULL, 'cgx_edit()'));
    if (has_privilege('admin')) $cgx_datagrid->addColumn(new Structures_DataGrid_Column(NULL, NULL, NULL, array('align' => 'center', 'width' => '1'), NULL, 'cgx_delete()'));

    $cgx_table = new HTML_Table($cgx_TableAttribs);
    $cgx_tableHeader = & $cgx_table->getHeader();
    $cgx_tableBody = & $cgx_table->getBody();

    $cgx_test = $cgx_datagrid->fill($cgx_table, $cgx_RendererOptions);
    if (PEAR::isError($cgx_test)) {
        echo $cgx_test->getMessage();
    }

    $cgx_tableHeader->setRowAttributes(0, $cgx_HeaderAttribs);
    $cgx_tableBody->altRowAttributes(0, $cgx_EvenRowAttribs, $cgx_OddRowAttribs, TRUE);

    echo $cgx_table->toHtml();

    echo "<table width='100%'><tr>\n";
    echo "<td class='datagrid_pager'>Found " . number_format($cgx_datagrid->getRecordCount()) . " record(s)</td>\n";
    echo "<td align='right' class='datagrid_pager'>\n";
    $cgx_test = $cgx_datagrid->render(DATAGRID_RENDER_PAGER);
    if (PEAR::isError($cgx_test)) {
        echo $cgx_test->getMessage();
    }
    echo "</td></tr></table>\n";
    echo "</div>\n";
}

?>
<?php

/*
 * dashboard
 * Azwari Nugraha <nugraha@duabelas.org>
 * Mar 3, 2014 1:17:37 PM
 */
echo "<h3>Project Activities</h3>";

$rsx = mysql_query("SELECT * FROM ts "
        . "JOIN emp USING (emp_id) "
        . "JOIN project USING (project_id) "
        . "WHERE ts_date = '" . date('Y-m-d') . "' "
        . "ORDER BY ts.last_updated DESC", $APP_CONNECTION);

if (mysql_num_rows($rsx) > 0) {
    echo "<div class='panel panel-success'>\n";
    echo "<div class='panel-heading'>Today</div>\n";
    echo "<div class='panel-body'>\n";
    echo "<table class='table table-condensed table-striped'>";
    while ($dtx = mysql_fetch_array($rsx, MYSQL_ASSOC)) {
        if ($dtx['ts_start'] != '00:00:00') {
            $time = substr($dtx['ts_start'], 0, 5);
            if ($dtx['ts_end'] != '00:00:00') {
                $time .= "-" . substr($dtx['ts_end'], 0, 5);
            }
        } else {
            $time = '';
        }
        echo "<tr>";
        echo "<td width='1%'><img class='img-circle' src='" . user_image($dtx['emp_id']) . "'></td>";
        echo "<td width='20%' align='right'><strong>{$dtx['emp_name']}</strong><br>" .
                "<div class='text-primary'>" . $dtx['project_name'] . "</div>" .
                "<em>" . $dtx['location'] . " {$time}</em></td>";
        echo "<td>" . nl2br($dtx['tasks']) . "</td>";
        echo "</tr>";
    }
    echo "</table>";
    echo "</div>\n";
    echo "</div>\n";
}
mysql_free_result($rsx);

$rsx = mysql_query("SELECT * FROM project WHERE active = 'Y' ORDER BY update_date DESC", $APP_CONNECTION);
while ($dtx = mysql_fetch_array($rsx, MYSQL_ASSOC)) {
    $rsz = mysql_query(
            "SELECT ts.*, emp_id, emp_name "
            . "FROM ts "
            . "JOIN emp USING (emp_id) "
            . "WHERE project_id = '{$dtx['project_id']}' "
            . "ORDER BY ts_date DESC "
            . "LIMIT 5", $APP_CONNECTION);
    if (mysql_num_rows($rsz) == 0) continue;
    echo "<div class='panel panel-success'>\n";
    echo "<div class='panel-heading'>{$dtx['project_name']}</div>\n";
    echo "<div class='panel-body'>\n";
    echo "<table class='table table-condensed table-striped'>";
    while ($dtz = mysql_fetch_array($rsz, MYSQL_ASSOC)) {
        if ($dtz['ts_date'] != date('Y-m-d')) {
            $tsdate = date($APP_DATE_FORMAT_LONG, strtotime($dtz['ts_date']));
        } else {
            $tsdate = "<span style='color: darkorange;'>Today</span>";
        }
        echo "<tr>";
        echo "<td width='1%'><img class='img-circle' src='" . user_image($dtz['emp_id']) . "'></td>";
        echo "<td width='20%' align='right'><strong>{$dtz['emp_name']}</strong><br>" .
                $tsdate . "<br>" .
                "<em>" . $dtz['location'] . "</em></td>";
        echo "<td>" . nl2br($dtz['tasks']) . "</td>";
        echo "</tr>";
    }
    echo "</table>";
    echo "</div>\n";
    echo "</div>\n";
}
mysql_free_result($rsx);

?>
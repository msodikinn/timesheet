<?php

/**
 * (c) Azwari Nugraha <nugraha@duabelas.org>
 * 02/03/2014 22:50:26
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE);
if (!has_privilege('admin')) return;

echo "<div class='page-header'><h3>User Management</h3></div>";

function cgx_edit($data) {
    $href = "index.php?&m={$_REQUEST['m']}&pkey[emp_id]={$data['record']['emp_id']}";
    $out = "<a href='{$href}'><img title='Security options' src='images/icon_security.png' border='0'></a>";
    return $out;
}

if (strlen($_REQUEST['pkey']['emp_id']) > 0) {
    $cgx_id = $_REQUEST['id'];
    $cgx_data = cgx_fetch_table("SELECT * FROM emp WHERE emp.emp_id = '" . mysql_escape_string($_REQUEST['pkey']['emp_id']) . "'");


    echo "<div class='panel panel-default'>\n";
    echo "<div class='panel-body'>\n";
    echo "<form class='form-horizontal' role='form' action='action/user.php' method='post'>\n";
    echo "<input type='hidden' name='backvar' value='" . urlencode("index.php?&m={$_REQUEST['m']}") . "'>\n";
    echo "<input type='hidden' name='mode' value='" . ($_REQUEST['pkey']['emp_id'] == '0' ? 'new' : 'update') . "'>\n";
    echo "<input type='hidden' name='pkey[emp_id]' value=\"{$_REQUEST['pkey']['emp_id']}\">\n";
    echo "<input type='hidden' name='table' value='emp'>\n";

    if ($_SESSION[$GLOBALS['APP_ID']]['user']['error']) {
        echo "<div class='alert alert-danger'>{$_SESSION[$GLOBALS['APP_ID']]['user']['error']}</div>";
        unset($_SESSION[$GLOBALS['APP_ID']]['user']['error']);
    }

    if ($_SESSION[$GLOBALS['APP_ID']]['user']['info']) {
        echo "<div class='alert alert-success'>{$_SESSION[$GLOBALS['APP_ID']]['user']['info']}</div>";
        unset($_SESSION[$GLOBALS['APP_ID']]['user']['info']);
    }

    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_emp_id'>Employee ID</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo "        <input id='data_emp_id' style='width: 5em;' readonly class='form-control' value='{$cgx_data['emp_id']}'>";
    echo "        </div>\n";
    echo "        </div>\n";
    
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_emp_name'>Name</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo "        <input id='data_emp_name' xstyle='width: 5em;' readonly class='form-control' value=\"{$cgx_data['emp_name']} &lt;{$cgx_data['email']}&gt;\">";
    echo "        </div>\n";
    echo "        </div>\n";
    
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_active'>Active</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo cgx_form_select('data[active]', array('Y' => 'Yes', 'N' => 'No'), $cgx_data['active'], FALSE, "id='data_active'");
    echo "        </div>\n";
    echo "        </div>\n";
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_is_admin'>Administrator</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo cgx_form_select('data[is_admin]', array('Y' => 'Yes', 'N' => 'No'), $cgx_data['is_admin'], FALSE, "id='data_is_admin'");
    echo "        </div>\n";
    echo "        </div>\n";
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_is_project_manager'>Project Manager</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo cgx_form_select('data[is_project_manager]', array('Y' => 'Yes', 'N' => 'No'), $cgx_data['is_project_manager'], FALSE, "id='data_is_project_manager'");
    echo "        </div>\n";
    echo "        </div>\n";
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_password'>Password</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo "        <input class='form-control' id='data_password' name='data[password]' type='text' maxlength='16' style='text-align: left;width: 16em;' autocomplete='off' /><small>Leave it empty if you don't want to change the password</small>\n";
    echo "        </div>\n";
    echo "        </div>\n";
    echo "        <div class='form-group'>\n";
    echo "        <div class='col-sm-offset-2 col-sm-10'>\n";
    echo "        <input class='btn btn-primary' type='submit' value='Submit'>\n";
    echo "        <input class='btn btn-warning' type='button' value='Back' onclick=\"window.location = 'index.php?&m=user';\">\n";
    echo "        </div>\n";
    echo "        </div>\n";
    echo "</form>\n";
    echo "</div>\n";
    echo "</div>\n";

} else {
    require_once 'Structures/DataGrid.php';
    require_once 'HTML/Table.php';

    echo "<div class='panel panel-default'>";
    if (is_array($_SESSION[$GLOBALS['APP_ID']]['user']['columns'])) {
        $cgx_def_columns = $_SESSION[$GLOBALS['APP_ID']]['user']['columns'];
    } else {
        $cgx_def_columns = array(
            'emp_id' => 1,
            'emp_name' => 1,
            'email' => 1,
            'active' => 1,
            'is_admin' => 1,
            'last_login' => 1,
            'last_ip' => 1,
        );
        $_SESSION[$GLOBALS['APP_ID']]['user']['columns'] = $cgx_def_columns;
    }

    $cgx_sql = "SELECT * FROM emp WHERE 1 = 1";
    $cgx_datagrid = new Structures_DataGrid($cgx_max_rows);
    $cgx_options = array('dsn' => $cgx_dsn);


    echo "<div class='panel-heading'>";
    echo "<form name='frmFILTER' action='{$_SERVER['SCRIPT_NAME']}'>\n";
    echo "<input type='hidden' name='m' value='{$_REQUEST['m']}'>\n";
    echo "<table id='bar' class='datagrid_bar' width='100%'><tr>\n";
    echo "<td></td>\n";
    echo "<td width='20'></td>\n";
    echo "<td width='1' class='datagrid_bar_icon'><a title='Customize columns' href='javascript:customizeColumn(true);'><img border='0' src='images/icon_columns.png'></a></td>\n";
    echo "</tr></table>\n";
    echo "</form>\n";
    echo "</div>";

    echo "<div id='columns' class='panel-body' style='display: none;'>\n";
    echo "<form name='frmCUSTOMIZE' action='action/cgx_customize.php' method='post'>\n";
    echo "<input type='hidden' name='back' value='" . urlencode($_SERVER['REQUEST_URI']) . "'>\n";
    echo "<input type='hidden' name='dg_name' value='user'>\n";
    echo "<div class='checkbox'><label><input" . ($cgx_def_columns['emp_id'] == 1 ? ' checked' : '') . " type='checkbox' name='col[emp_id]'> Emp No</label></div>";
    echo "<div class='checkbox'><label><input" . ($cgx_def_columns['emp_name'] == 1 ? ' checked' : '') . " type='checkbox' name='col[emp_name]'> Employee Name</label></div>";
    echo "<div class='checkbox'><label><input" . ($cgx_def_columns['email'] == 1 ? ' checked' : '') . " type='checkbox' name='col[email]'> Email</label></div>";
    echo "<div class='checkbox'><label><input" . ($cgx_def_columns['active'] == 1 ? ' checked' : '') . " type='checkbox' name='col[active]'> Active</label></div>";
    echo "<div class='checkbox'><label><input" . ($cgx_def_columns['is_admin'] == 1 ? ' checked' : '') . " type='checkbox' name='col[is_admin]'> Administrator</label></div>";
    echo "<div class='checkbox'><label><input" . ($cgx_def_columns['last_login'] == 1 ? ' checked' : '') . " type='checkbox' name='col[last_login]'> Last Login</label></div>";
    echo "<div class='checkbox'><label><input" . ($cgx_def_columns['last_ip'] == 1 ? ' checked' : '') . " type='checkbox' name='col[last_ip]'> Last IP Address</label></div>";
    echo "<input class='btn btn-primary' type='submit' value='Update'>\n";
    echo "<input class='btn btn-warning' type='button' value='Cancel' onclick='customizeColumn(false);'>\n";
    echo "</form>\n";
    echo "</div>\n";
?>
<script type="text/javascript">
<!--
function customizeColumn(s) {
    var divCols = document.getElementById('columns');
    var divBar = document.getElementById('bar');
    if (s) {
        divCols.style.display = 'block';
        divBar.style.display = 'none';
    } else {
        window.location = window.location;
    }
}
//-->
</script>
<?php
    if ($_SESSION[$GLOBALS['APP_ID']]['user']['error']) {
        echo "<div class='alert alert-danger'>{$_SESSION[$GLOBALS['APP_ID']]['user']['error']}</div>";
        unset($_SESSION[$GLOBALS['APP_ID']]['user']['error']);
    }

    if ($_SESSION[$GLOBALS['APP_ID']]['user']['info']) {
        echo "<div class='alert alert-success'>{$_SESSION[$GLOBALS['APP_ID']]['user']['info']}</div>";
        unset($_SESSION[$GLOBALS['APP_ID']]['user']['info']);
    }


    $cgx_test = $cgx_datagrid->bind($cgx_sql, $cgx_options);
    if (PEAR::isError($cgx_test)) {
        echo $cgx_test->getMessage();
    }

    if ($cgx_def_columns['emp_id'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Emp No', 'emp_id', 'emp_id', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['emp_name'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Employee Name', 'emp_name', 'emp_name', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['email'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Email', 'email', 'email', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['active'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Active', 'active', 'active', array('align' => 'left'), NULL, "cgx_format_yesno()"));
    if ($cgx_def_columns['is_admin'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Administrator', 'is_admin', 'is_admin', array('align' => 'left'), NULL, "cgx_format_yesno()"));
    if ($cgx_def_columns['last_login'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Last Login', 'last_login', 'last_login', array('align' => 'left'), NULL, "cgx_format_timestamp()"));
    if ($cgx_def_columns['last_ip'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Last IP Address', 'last_ip', 'last_ip', array('align' => 'left'), NULL, NULL));
    if (has_privilege('admin')) $cgx_datagrid->addColumn(new Structures_DataGrid_Column(NULL, NULL, NULL, array('align' => 'center', 'width' => '1'), NULL, 'cgx_edit()'));

    $cgx_table = new HTML_Table($cgx_TableAttribs);
    $cgx_tableHeader = & $cgx_table->getHeader();
    $cgx_tableBody = & $cgx_table->getBody();

    $cgx_test = $cgx_datagrid->fill($cgx_table, $cgx_RendererOptions);
    if (PEAR::isError($cgx_test)) {
        echo $cgx_test->getMessage();
    }

    $cgx_tableHeader->setRowAttributes(0, $cgx_HeaderAttribs);
    $cgx_tableBody->altRowAttributes(0, $cgx_EvenRowAttribs, $cgx_OddRowAttribs, TRUE);

    echo $cgx_table->toHtml();

    echo "<table width='100%'><tr>\n";
    echo "<td class='datagrid_pager'>Found " . number_format($cgx_datagrid->getRecordCount()) . " record(s)</td>\n";
    echo "<td align='right' class='datagrid_pager'>\n";
    $cgx_test = $cgx_datagrid->render(DATAGRID_RENDER_PAGER);
    if (PEAR::isError($cgx_test)) {
        echo $cgx_test->getMessage();
    }
    echo "</td></tr></table>\n";
    echo "</div>\n";
}

?>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <br>
                    <?php
                    if ((strlen($_REQUEST['f1']) > 0)&&(strlen($_REQUEST['q']) > 0)) {
                        $default_date = $_REQUEST['f1'];
                        $cgx_search_name = $_REQUEST['q'];
                    }
                    else if ((strlen($_REQUEST['f1']) > 0)&&(strlen($_REQUEST['q']) == 0)) {
                        $default_date = $_REQUEST['f1'];
                        $cgx_search_name = "";
                    }else {
                        $default_date = date("Y-m-d");
                        $cgx_search_name = "";
                    }
                    $rsw=mysql_query("select * from ts join emp using (emp_id) "
                    . "WHERE ts.ts_date LIKE '%{$default_date}%' AND emp.emp_name LIKE '%{$cgx_search_name}%' group by emp_name
                    ",$APP_CONNECTION);
                    $rsx=mysql_query("select * from ts join emp using (emp_id) "
                    . "WHERE ts.ts_date LIKE '%{$default_date}%' AND emp.emp_name LIKE '%{$cgx_search_name}%' group by emp_name
                    ",$APP_CONNECTION);
                    $rsy=mysql_query("select * from ts join emp using (emp_id) "
                    . "WHERE ts.ts_date LIKE '%{$default_date}%' AND emp.emp_name LIKE '%{$cgx_search_name}%' group by emp_name
                    ",$APP_CONNECTION);
                    $rsz=mysql_query("select * from ts join emp using (emp_id) "
                    . "WHERE ts.ts_date LIKE '%{$default_date}%' AND emp.emp_name LIKE '%{$cgx_search_name}%' group by emp_name
                    ",$APP_CONNECTION);
                echo "<div class='panel-heading'>";
            echo "<form role='form' class='form-inline' name='frmBoard' action='{$_SERVER['SCRIPT_NAME']}'>\n";
            echo "<input type='hidden' name='m' value='{$_REQUEST['m']}'>\n";
            echo "<table id='bar' class='datagrid_bar' width='100%'><tr>\n";
            echo "<td>\n";
            echo "<table align='right' cellspacing='0' cellpadding='0' border='0'><tr>\n";
            echo "<td>Search By Name<div class='input-group col-md-12'><input class='form-control input-sm' type='text' style='text-align: left;width: 20em;' name='q' value=\"{$cgx_search_name}\"><span class='input-group-addon'><span class='glyphicon glyphicon-search' onClick='frmBoard.submit();' style='cursor:pointer;'></span></span></div></td>\n";
            echo "<td width='20'></td>\n";
            echo "</tr></table>\n";

            echo "<table align='right' cellspacing='0' cellpadding='0' border='0'><tr>\n";
            echo "<td>Date<div class='input-group col-md-12'><input class='form-control input-sm' id='date' name='f1' type='date' value=\"{$default_date}\" maxlength='5' style='text-align: left;width: 20em;' onChange='frmBoard.submit()'/></div></td>\n";
            echo "<td width='20'></td>\n";
            echo "</tr></table>\n";

            echo "</td>\n";
            echo "<td></td>\n";
            echo "</tr></table>\n";
            echo "</form>\n";
            echo "</div>";
            ?>
            <div id="ab" style="min-width: 600px; height: 600px; margin: 0 auto"></div>
        </div>
    </div>
    
    <script src="http://code.highcharts.com/highcharts.js"></script>
    <script src="http://code.highcharts.com/highcharts-more.js"></script>
    <script src="http://code.highcharts.com/modules/exporting.js"></script>


    <script type="text/javascript">
    $(function () {
    $("#date").datepicker({dateFormat: 'yy-mm-dd'});
    $('#ab').highcharts({
        credits: {
                enabled: false
            },
        chart: {
            type: 'columnrange',
            inverted: true,
            marginLeft: 100,
        },

        title: {
            text: <?php echo "'Activity Board ".$default_date."'" ?>
        },

        subtitle: {
            text: ''
        },
        

        xAxis: {
            categories: [
                <?php
                while ($dtx = mysql_fetch_array($rsx, MYSQL_ASSOC)){
                    echo "'".$dtx['emp_name']."',";
                }
                mysql_free_result($rsx);
                ?>
                ],
            title: {
                text: 'Employee Name'
            }
        },

        yAxis: {
            title: {
                text: 'Time (Hour)'
            }
        },

        tooltip: {
            formatter: function(){
                return 'Name : <b>'+this.x+'</b><br>Start Time : <b>'+this.point.start+' WIB</b><br>End Time : <b>'+this.point.end+' WIB</b><br>Location : <b>'+this.point.location+'</b>';
            },
            crosshairs : [true,true]
        },

        plotOptions: {
            columnrange: {
                pointPadding: 0,
                grouping: false,
                dataLabels: {
                    inside: true,
                    enabled: true,
                    useHTML: true,
                    align: 'center',
                    formatter: function () {
                        return 'at '+this.point.location;
                    },
                    y:0
                }
            }
        },

        legend: {
            enabled: false
        },

        series: [{
            name: 'Time (Hour)',
            color: 'rgb(4,121,175)',
            data: [
                <?php
                while ($dty = mysql_fetch_array($rsy, MYSQL_ASSOC)){
                        $start = str_replace(':','.',substr($dty['ts_start'], 0, 5));
                        $end = str_replace(':','.',substr($dty['ts_end'], 0, 5));
                        echo "{";
                        echo "low : ".(float)$start.",";
                        echo "high : ".(float)$end.",";
                        echo "start : '".$dty['ts_start']."',";
                        echo "end : '".$dty['ts_end']."',";
                        echo "location : '".addslashes($dty['location'])."',";
                        echo "},";
                } 
                mysql_free_result($rsy);
                ?>]
        },{
            name: 'Time (Hour)',
            data: [
                <?php
                while ($dtw = mysql_fetch_array($rsw, MYSQL_ASSOC)){
                    $emp_name1 = $dtw['emp_name'];
                    
                    $count_data2 = npl_fetch_table("select count(emp_name) from ts join emp using (emp_id) "
                    . "WHERE ts.ts_date = '{$default_date}' AND emp_name = '{$emp_name1}' order by ts_id desc limit 1
                    ");
                    
                    $count1 = $count_data2['count(emp_name)'];
                    
                    if($count1 == 2){
                        $count_data3 = npl_fetch_table("select location, ts_start, ts_end from ts join emp using (emp_id) "
                        . "WHERE ts.ts_date = '{$default_date}' AND emp_name = '{$emp_name1}' order by ts_id desc limit 1
                        ");
                        $ts_start1 = $count_data3['ts_start'];
                        $ts_end1 = $count_data3['ts_end'];
                        $location1 = $count_data3['location'];
                        echo "{";
                        echo "low : ".(float)$ts_start1.",";
                        echo "high : ".(float)$ts_end1.",";
                        echo "start : '".$ts_start1."',";
                        echo "end : '".$ts_end1."',";
                        echo "location : '".addslashes($count_data3['location'])."',";
                        echo "},";
                    }
                    else if($count1 == 3){
                        $count_data3 = npl_fetch_table("select location, ts_start, ts_end from ts join emp using (emp_id) "
                        . "WHERE ts.ts_date LIKE '{$default_date}' AND emp_name LIKE '{$emp_name1}' AND (ts_id > (select min(ts_id) from ts join emp using (emp_id) "
                        . "WHERE ts.ts_date = '{$default_date}' AND emp_name = '{$emp_name1}')) AND (ts_id<(select max(ts_id) from ts join emp using (emp_id) "
                        . "WHERE ts.ts_date = '{$default_date}' AND emp_name = '{$emp_name1}')) order by ts_id desc limit 1
                        ");
                        $ts_start1 = $count_data3['ts_start'];
                        $ts_end1 = $count_data3['ts_end'];
                        $location1 = $count_data3['location'];
                        echo "{";
                        echo "low : ".(float)$ts_start1.",";
                        echo "high : ".(float)$ts_end1.",";
                        echo "start : '".$ts_start1."',";
                        echo "end : '".$ts_end1."',";
                        echo "location : '".addslashes($count_data3['location'])."',";
                        echo "},";
                    }
                    else{
                        echo "[,],";
                    }
                } 
                mysql_free_result($rsw);
                ?>]
        },{
            name: 'Time (Hour)',
            color: 'rgb(235,59,125)',
            data: [
                <?php
                while ($dtz = mysql_fetch_array($rsz, MYSQL_ASSOC)){
                    $emp_name = $dtz['emp_name'];
                    $count_data = npl_fetch_table("select count(emp_name) from ts join emp using (emp_id) "
                    . "WHERE ts.ts_date = '{$default_date}' AND emp_name = '{$emp_name}' order by ts_id desc limit 1
                    ");
                    $count_data1 = npl_fetch_table("select location, ts_start, ts_end from ts join emp using (emp_id) "
                    . "WHERE ts.ts_date = '{$default_date}' AND emp_name = '{$emp_name}' order by ts_id desc limit 1
                    ");
                    $count = $count_data['count(emp_name)'];
                    $ts_start = $count_data1['ts_start'];
                    $ts_end = $count_data1['ts_end'];
                    $location = $count_data1['location'];
                    if($count != 3){
                        echo "[,],";
                    }
                    else if($count == 3){
                        echo "{";
                        echo "low : ".(float)$ts_start.",";
                        echo "high : ".(float)$ts_end.",";
                        echo "start : '".$ts_start."',";
                        echo "end : '".$ts_end."',";
                        echo "location : '".addslashes($count_data1['location'])."',";
                        echo "},";
                    }
                } 
                mysql_free_result($rsz);
                ?>]
        }]
        

    });
});
</script>

<?php

/**
 * (c) Azwari Nugraha <nugraha@duabelas.org>
 * 02/03/2014 22:50:26
 */


if (!has_privilege('project_manager')) return;

$ts_date = empty($_REQUEST['date']) ? date('d-m-Y') : $_REQUEST['date'];
$mysql_date = npl_dmy2ymd($ts_date);

$today = date('d-m-Y');
$yesterday = date('d-m-Y', mktime(0, 0, 0, date('n'), date('j') - 1, date('Y')));

echo "<div class='page-header'><h3>Attendance Report - " . date('l, j F Y', strtotime($mysql_date)) . "</h3></div>";

echo "<div class='panel panel-default'>\n";
echo "<div class='panel-body'>\n";
echo "<form class='form-inline' role='form' action='' method='post'>\n";
echo "        <div class='form-group'>\n";
echo "        <label class='control-label' for='ts_date'>Date</label>\n";
echo "        <input class='form-control' id='ts_date' name='ts_date' type='text' value=\"{$ts_date}\" maxlength='10' style='text-align: center;width: 10em;'  />\n";
echo "        <input type='button' value='Today' class='btn btn-primary' onclick=\"window.location = 'index.php?m=att&date={$today}';\" />\n";
echo "        <input type='button' value='Yesterday' class='btn btn-primary' onclick=\"window.location = 'index.php?m=att&date={$yesterday}';\" />\n";
echo "        </div>\n";
echo "</form>\n";
echo "</div>\n";
echo "</div>\n";

require_once 'Structures/DataGrid.php';
require_once 'HTML/Table.php';

echo "<div class='panel panel-default'>";

$cgx_sql = "SELECT emp_id, emp_name, TIME_FORMAT(ts_start, '%H:%i') check_in, TIME_FORMAT(ts_end, '%H:%i') check_out, project_name, location, contact_person FROM emp "
        . "LEFT JOIN (SELECT * FROM ts JOIN project USING (project_id) WHERE ts_date = '{$mysql_date}') tsx USING (emp_id) "
        . "WHERE is_employee = 'Y' AND emp.active = 'Y'";

$cgx_datagrid = new Structures_DataGrid($cgx_max_rows);
$cgx_options = array('dsn' => $cgx_dsn);
$cgx_datagrid->setDefaultSort(array('emp_id' => 'ASC'));

$cgx_test = $cgx_datagrid->bind($cgx_sql, $cgx_options);
if (PEAR::isError($cgx_test)) {
    echo $cgx_test->getMessage();
}

$cgx_datagrid->addColumn(new Structures_DataGrid_Column('Emp No', 'emp_id', 'emp_id', array('align' => 'left'), NULL, NULL));
$cgx_datagrid->addColumn(new Structures_DataGrid_Column('Employee Name', 'emp_name', 'emp_name', array('align' => 'left'), NULL, NULL));
$cgx_datagrid->addColumn(new Structures_DataGrid_Column('Check In', 'check_in', 'check_in', array('align' => 'left'), NULL, NULL));
$cgx_datagrid->addColumn(new Structures_DataGrid_Column('Check Out', 'check_out', 'check_out', array('align' => 'left'), NULL, NULL));
$cgx_datagrid->addColumn(new Structures_DataGrid_Column('Project', 'project_name', 'project_name', array('align' => 'left'), NULL, NULL));
$cgx_datagrid->addColumn(new Structures_DataGrid_Column('Location', 'location', 'location', array('align' => 'left'), NULL, NULL));

$cgx_table = new HTML_Table($cgx_TableAttribs);
$cgx_tableHeader = & $cgx_table->getHeader();
$cgx_tableBody = & $cgx_table->getBody();

$cgx_test = $cgx_datagrid->fill($cgx_table, $cgx_RendererOptions);
if (PEAR::isError($cgx_test)) {
    echo $cgx_test->getMessage();
}

$cgx_tableHeader->setRowAttributes(0, $cgx_HeaderAttribs);
$cgx_tableBody->altRowAttributes(0, $cgx_EvenRowAttribs, $cgx_OddRowAttribs, TRUE);

echo $cgx_table->toHtml();

echo "<table width='100%'><tr>\n";
echo "<td class='datagrid_pager'>Found " . number_format($cgx_datagrid->getRecordCount()) . " record(s)</td>\n";
echo "<td align='right' class='datagrid_pager'>\n";
$cgx_test = $cgx_datagrid->render(DATAGRID_RENDER_PAGER);
if (PEAR::isError($cgx_test)) {
    echo $cgx_test->getMessage();
}
echo "</td></tr></table>\n";
echo "</div>\n";


?>
<script type='text/javascript'>
<!--
$(function() {
    $("#ts_date").datepicker({
        dateFormat: 'dd-mm-yy',
        onSelect: function(date) {
            window.location = "index.php?m=att&date=" + date;
        }
    });
});
//-->
</script>

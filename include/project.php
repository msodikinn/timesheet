<?php

/**
 * (c) Azwari Nugraha <nugraha@duabelas.org>
 * 03/03/2014 12:19:10
 */

if (!has_privilege('admin')) return;

echo "<div class='page-header'><h3>Project</h3></div>";

function cgx_edit($data) {
    $href = "index.php?&m={$_REQUEST['m']}&pkey[project_id]={$data['record']['project_id']}";
    $out = "<a href='{$href}'><img title='Edit this row' src='images/icon_edit.png' border='0'></a>";
    return $out;
}

function cgx_delete($data) {
    $href  = "javascript:if(confirm('Delete this row?')){window.location='action/project.php";
    $href .= "?backvar=index.php%253F%2526m%253Dproject&mode=delete&pkey[project_id]={$data['record']['project_id']}';}";
    $out = "<a href=\"{$href}\"><img title='Delete this row' src='images/icon_delete.png' border='0'></a>";
    return $out;
}

if (strlen($_REQUEST['pkey']['project_id']) > 0) {
    $cgx_id = $_REQUEST['id'];
    $cgx_data = cgx_fetch_table("SELECT * FROM project WHERE project.project_id = '" . mysql_escape_string($_REQUEST['pkey']['project_id']) . "'");

    $cgx_data['date_start'] = cgx_emptydate($cgx_data['date_start']) ? '' : cgx_ddmmyyyy($cgx_data['date_start']);
    $cgx_data['date_end'] = cgx_emptydate($cgx_data['date_end']) ? '' : cgx_ddmmyyyy($cgx_data['date_end']);

    echo "<div class='panel panel-default'>\n";
    echo "<div class='panel-body'>\n";
    echo "<form class='form-horizontal' role='form' action='action/project.php' method='post'>\n";
    echo "<input type='hidden' name='backvar' value='" . urlencode("index.php?&m={$_REQUEST['m']}") . "'>\n";
    echo "<input type='hidden' name='mode' value='" . ($_REQUEST['pkey']['project_id'] == '0' ? 'new' : 'update') . "'>\n";
    echo "<input type='hidden' name='pkey[project_id]' value=\"{$_REQUEST['pkey']['project_id']}\">\n";
    echo "<input type='hidden' name='table' value='project'>\n";

    if ($_SESSION[$GLOBALS['APP_ID']]['project']['error']) {
        echo "<div class='alert alert-danger'>{$_SESSION[$GLOBALS['APP_ID']]['project']['error']}</div>";
        unset($_SESSION[$GLOBALS['APP_ID']]['project']['error']);
    }

    if ($_SESSION[$GLOBALS['APP_ID']]['project']['info']) {
        echo "<div class='alert alert-success'>{$_SESSION[$GLOBALS['APP_ID']]['project']['info']}</div>";
        unset($_SESSION[$GLOBALS['APP_ID']]['project']['info']);
    }

    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_project_id'>ID</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo "        <input class='form-control' id='data_project_id' name='data[project_id]' type='text' value=\"{$cgx_data['project_id']}\" maxlength='8' style='text-align: right;width: 9em;'  disabled />\n";
    echo "        </div>\n";
    echo "        </div>\n";
    
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_project_name'>Project Name</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo "        <input class='form-control' id='data_project_name' name='data[project_name]' type='text' value=\"{$cgx_data['project_name']}\" maxlength='50' style='text-align: left;width: 51em;'  />\n";
    echo "        </div>\n";
    echo "        </div>\n";
    
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_project_code'>3 Digit Code</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo "        <input class='form-control' id='data_project_code' name='data[project_code]' type='text' value=\"{$cgx_data['project_code']}\" maxlength='3' style='text-align: center;width: 5em;'  />\n";
    echo "        </div>\n";
    echo "        </div>\n";
    
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_description'>Description</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo "        <textarea class='form-control' id='data_description' name='data[description]' rows='4' cols='50' style='text-align: left;' >{$cgx_data['description']}</textarea>\n";
    echo "        </div>\n";
    echo "        </div>\n";
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_company_name'>Company Name</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo "        <input class='form-control' id='data_company_name' name='data[company_name]' type='text' value=\"{$cgx_data['company_name']}\" maxlength='50' style='text-align: left;width: 51em;'  />\n";
    echo "        </div>\n";
    echo "        </div>\n";
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_date_start'>Date Start</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo "        <input class='form-control' id='data_date_start' name='data[date_start]' type='text' value=\"{$cgx_data['date_start']}\" maxlength='10' style='text-align: left;width: 10em;'  />\n";
    echo "        </div>\n";
    echo "        </div>\n";
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_date_end'>Date End</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo "        <input class='form-control' id='data_date_end' name='data[date_end]' type='text' value=\"{$cgx_data['date_end']}\" maxlength='10' style='text-align: left;width: 10em;'  />\n";
    echo "        </div>\n";
    echo "        </div>\n";
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_active'>Active</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo cgx_form_select('data[active]', array('Y' => 'Yes', 'N' => 'No'), $cgx_data['active'], FALSE, "id='data_active'");
    echo "        </div>\n";
    echo "        </div>\n";
    echo "        <div class='form-group'>\n";
    echo "        <div class='col-sm-offset-2 col-sm-10'>\n";
    echo "        <input class='btn btn-primary' type='submit' value='Submit'>\n";
    echo "        <input class='btn btn-warning' type='button' value='Back' onclick=\"window.location = 'index.php?&m=project';\">\n";
    echo "        </div>\n";
    echo "        </div>\n";
    echo "</form>\n";
    echo "</div>\n";
    echo "</div>\n";

?>
<script type='text/javascript'>
<!--
$(function() {
    $("#data_date_start").datepicker({dateFormat: 'dd-mm-yy'});
    $("#data_date_end").datepicker({dateFormat: 'dd-mm-yy'});
});
//-->
</script>
<?php
} else {
    require_once 'Structures/DataGrid.php';
    require_once 'HTML/Table.php';

    echo "<div class='panel panel-default'>";
    if (is_array($_SESSION[$GLOBALS['APP_ID']]['project']['columns'])) {
        $cgx_def_columns = $_SESSION[$GLOBALS['APP_ID']]['project']['columns'];
    } else {
        $cgx_def_columns = array(
            'project_id' => 1,
            'project_name' => 1,
            'project_code' => 1,
            'company_name' => 1,
            'date_start' => 1,
            'date_end' => 1,
            'active' => 1,
        );
        $_SESSION[$GLOBALS['APP_ID']]['project']['columns'] = $cgx_def_columns;
    }

    $cgx_sql = "SELECT * FROM project WHERE 1 = 1";
    $cgx_datagrid = new Structures_DataGrid($cgx_max_rows);
    $cgx_options = array('dsn' => $cgx_dsn);

    $cgx_filter1 = urldecode($_REQUEST['f1']);
    $cgx_search = $_REQUEST['q'];

    echo "<div class='panel-heading'>";
    echo "<form name='frmFILTER' action='{$_SERVER['SCRIPT_NAME']}'>\n";
    echo "<input type='hidden' name='m' value='{$_REQUEST['m']}'>\n";
    echo "<table id='bar' class='datagrid_bar' width='100%'><tr>\n";
    echo "<td>\n";
    echo "<table align='left' cellspacing='0' cellpadding='0' border='0'><tr>\n";
    echo "<td><nobr><label for='f1'>Active</label></nobr></td>\n";
    echo "<td>&nbsp;</td>\n";
    echo "<td>" . cgx_filter('f1', array('Y' => 'Yes', 'N' => 'No'), $cgx_filter1, TRUE) . "</td>\n";
    echo "<td width='20'></td>\n";
    echo "</tr></table>\n";
    echo "</td>\n";
    echo "<td align='right'><input type='text' size='20' name='q' value=\"{$cgx_search}\"></td>\n";
    echo "<td width='1'><input title='Search' type='image' src='images/icon_search.png' border='0' style='padding-right: 20px;'></td>\n";
    echo "<td></td>\n";
    echo "<td width='20'></td>\n";
    if (has_privilege('admin')) {
        echo "<td width='1' class='datagrid_bar_icon'><a title='New record' href='index.php?&m={$_REQUEST['m']}&pkey[project_id]=0'><img border='0' src='images/icon_add.png'></a></td>\n";
    } else {
        echo "<td width='1' class='datagrid_bar_icon'><img border='0' src='images/icon_add_dis.png'></td>\n";
    }
    echo "<td width='1' class='datagrid_bar_icon'><a title='Export all (CSV)' href='action/project.php?mode=export-all'><img border='0' src='images/icon_csv.png'></a></td>\n";
    echo "<td width='1' class='datagrid_bar_icon'><a title='Customize columns' href='javascript:customizeColumn(true);'><img border='0' src='images/icon_columns.png'></a></td>\n";
    echo "</tr></table>\n";
    echo "</form>\n";
    echo "</div>";

    echo "<div id='columns' class='panel-body' style='display: none;'>\n";
    echo "<form name='frmCUSTOMIZE' action='action/cgx_customize.php' method='post'>\n";
    echo "<input type='hidden' name='back' value='" . urlencode($_SERVER['REQUEST_URI']) . "'>\n";
    echo "<input type='hidden' name='dg_name' value='project'>\n";
    echo "<input type='hidden' name='col[project_id]' value='on'>\n";
    echo "<input type='hidden' name='col[project_name]' value='on'>\n";
    echo "<div class='form-group'>";
    echo "<label class='checkbox-inline'><input disabled checked type='checkbox' name='col[project_id]'> ID</label>";
    echo "<label class='checkbox-inline'><input disabled checked type='checkbox' name='col[project_name]'> Project Name</label>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['project_code'] == 1 ? ' checked' : '') . " type='checkbox' name='col[project_code]'> Project Code</label>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['description'] == 1 ? ' checked' : '') . " type='checkbox' name='col[description]'> Description</label>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['company_name'] == 1 ? ' checked' : '') . " type='checkbox' name='col[company_name]'> Company Name</label>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['date_start'] == 1 ? ' checked' : '') . " type='checkbox' name='col[date_start]'> Date Start</label>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['date_end'] == 1 ? ' checked' : '') . " type='checkbox' name='col[date_end]'> Date End</label>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['active'] == 1 ? ' checked' : '') . " type='checkbox' name='col[active]'> Active</label>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['update_date'] == 1 ? ' checked' : '') . " type='checkbox' name='col[update_date]'> Last Updated</label>";
    echo "</div>";
    echo "<input class='btn btn-primary' type='submit' value='Update'>\n";
    echo "<input class='btn btn-warning' type='button' value='Cancel' onclick='customizeColumn(false);'>\n";
    echo "</form>\n";
    echo "</div>\n";
?>
<script type="text/javascript">
<!--
function customizeColumn(s) {
    var divCols = document.getElementById('columns');
    var divBar = document.getElementById('bar');
    if (s) {
        divCols.style.display = 'block';
        divBar.style.display = 'none';
    } else {
        window.location = window.location;
    }
}
//-->
</script>
<?php

    if (strlen($cgx_filter1) > 0) $cgx_sql .= " AND project.active = '" . mysql_escape_string($cgx_filter1) . "'";
    $cgx_sql .= " and ( project.project_id LIKE '%{$cgx_search}%' OR project.project_name LIKE '%{$cgx_search}%' OR project.description LIKE '%{$cgx_search}%' OR project.company_name LIKE '%{$cgx_search}%')";
    if ($_SESSION[$GLOBALS['APP_ID']]['project']['error']) {
        echo "<div class='alert alert-danger'>{$_SESSION[$GLOBALS['APP_ID']]['project']['error']}</div>";
        unset($_SESSION[$GLOBALS['APP_ID']]['project']['error']);
    }

    if ($_SESSION[$GLOBALS['APP_ID']]['project']['info']) {
        echo "<div class='alert alert-success'>{$_SESSION[$GLOBALS['APP_ID']]['project']['info']}</div>";
        unset($_SESSION[$GLOBALS['APP_ID']]['project']['info']);
    }


    $cgx_test = $cgx_datagrid->bind($cgx_sql, $cgx_options);
    if (PEAR::isError($cgx_test)) {
        echo $cgx_test->getMessage();
    }

    if ($cgx_def_columns['project_id'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('ID', 'project_id', 'project_id', array('align' => 'right'), NULL, NULL));
    if ($cgx_def_columns['project_name'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Project Name', 'project_name', 'project_name', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['project_code'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Code', 'project_code', 'project_code', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['description'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Description', 'description', 'description', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['company_name'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Company Name', 'company_name', 'company_name', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['date_start'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Date Start', 'date_start', 'date_start', array('align' => 'left'), NULL, "cgx_format_date()"));
    if ($cgx_def_columns['date_end'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Date End', 'date_end', 'date_end', array('align' => 'left'), NULL, "cgx_format_date()"));
    if ($cgx_def_columns['active'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Active', 'active', 'active', array('align' => 'left'), NULL, "cgx_format_yesno()"));
    if ($cgx_def_columns['update_date'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Last Updated', 'update_date', 'update_date', array('align' => 'left'), NULL, "cgx_format_timestamp()"));
    if (has_privilege('admin')) $cgx_datagrid->addColumn(new Structures_DataGrid_Column(NULL, NULL, NULL, array('align' => 'center', 'width' => '1'), NULL, 'cgx_edit()'));
    if (has_privilege('admin')) $cgx_datagrid->addColumn(new Structures_DataGrid_Column(NULL, NULL, NULL, array('align' => 'center', 'width' => '1'), NULL, 'cgx_delete()'));

    $cgx_table = new HTML_Table($cgx_TableAttribs);
    $cgx_tableHeader = & $cgx_table->getHeader();
    $cgx_tableBody = & $cgx_table->getBody();

    $cgx_test = $cgx_datagrid->fill($cgx_table, $cgx_RendererOptions);
    if (PEAR::isError($cgx_test)) {
        echo $cgx_test->getMessage();
    }

    $cgx_tableHeader->setRowAttributes(0, $cgx_HeaderAttribs);
    $cgx_tableBody->altRowAttributes(0, $cgx_EvenRowAttribs, $cgx_OddRowAttribs, TRUE);

    echo $cgx_table->toHtml();

    echo "<table width='100%'><tr>\n";
    echo "<td class='datagrid_pager'>Found " . number_format($cgx_datagrid->getRecordCount()) . " record(s)</td>\n";
    echo "<td align='right' class='datagrid_pager'>\n";
    $cgx_test = $cgx_datagrid->render(DATAGRID_RENDER_PAGER);
    if (PEAR::isError($cgx_test)) {
        echo $cgx_test->getMessage();
    }
    echo "</td></tr></table>\n";
    echo "</div>\n";
}

?>
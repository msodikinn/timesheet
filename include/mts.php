<?php

/*
 * mts
 * Azwari Nugraha <nugraha@pt-gai.com>
 * Oct 15, 2014 8:17:58 PM
 */
error_reporting(E_ERROR | E_WARNING | E_PARSE);

if (!authenticated()) return;

$def_month = empty($_REQUEST['period']) ? date('Y-m') : $_REQUEST['period'];
$def_month_name = date("F Y", strtotime($def_month . "-01"));

echo "<style>";
echo ".smalltd {font-size: 9px; vertical-align:middle;}";
echo "</style>";

echo "<div class='row'>";
echo "<div class='col-sm-9'><h3>Timesheet of {$def_month_name}</h3></div>";
echo "<div class='col-sm-3 text-right'>";
echo "<select onchange=\"window.location = 'index.php?m=mts&period=' + this.value;\" style='margin-top: 10px;' class='form-control'>";
$rsx = mysql_query("SELECT DISTINCT DATE_FORMAT(ts_date, '%Y-%m') period_id, DATE_FORMAT(ts_date, '%M %Y') period_name FROM ts ORDER BY ts_date", $APP_CONNECTION);
while ($dtx = mysql_fetch_array($rsx, MYSQL_ASSOC)) {
    if ($dtx['period_id'] == $def_month) {
        echo "<option selected value='{$dtx['period_id']}'>{$dtx['period_name']}</option>";
    } else {
        echo "<option value='{$dtx['period_id']}'>{$dtx['period_name']}</option>";
    }
}
mysql_free_result($rsx);
echo "</select>";
echo "</div>";
echo "</div>";


$ds = strtotime($def_month . '-01');
$de = mktime(0, 0, 0, date('m', $ds) + 1, date('d', $ds) - 1, date('Y', $ds));

echo "<div xstyle='overflow-x: scroll;'>";
echo "<table class='table table-condensed table-stripped table-bordered'>";
echo "<tr>";
echo "<th class='text-right'>No</th>";
echo "<th>Employee</th>";
for ($d = $ds; $d <= $de; $d += 86400) {
    if (date('N', $d) > 5) {
        echo "<th style='color: red;' class='danger text-center'>" . date('d', $d) . "</th>";
    } else {
        echo "<th class='text-center'>" . date('d', $d) . "</th>";
    }
}
echo "</tr>";

$no = 0;
$rsx = mysql_query("SELECT * FROM emp WHERE active = 'Y' AND is_employee = 'Y' ORDER BY emp_name", $APP_CONNECTION);
while ($dtx = mysql_fetch_array($rsx, MYSQL_ASSOC)) {
    
    $no++;
    
    $rsy = mysql_query(
            "SELECT ts_date, project_code, project_name FROM ts "
            . "JOIN project USING (project_id) "
            . "WHERE emp_id = '{$dtx['emp_id']}' "
            . "AND ts_date BETWEEN '" . date('Y-m-d', $ds) . "' AND '" . date('Y-m-d', $de) . "'",
            $APP_CONNECTION);
    unset($emp_ts);
    unset($emp_ts_desc);
    while ($dty = mysql_fetch_array($rsy, MYSQL_ASSOC)) {
        if ($emp_ts[$dty['ts_date']]) {
            $emp_ts[$dty['ts_date']][] = $dty['project_code'];
            $emp_ts_desc[$dty['ts_date']][] = $dty['project_name'];
        } else {
            $emp_ts[$dty['ts_date']][0] = $dty['project_code'];
            $emp_ts_desc[$dty['ts_date']][0] = $dty['project_name'];
        }
    }
    mysql_free_result($rsy);
    
    echo "<tr>";
    echo "<td class='text-right'>{$no}</td>";
    echo "<td style='padding: 1px;'><img width='32' class='img-circle' src='" . user_image($dtx['emp_id']) . "'> {$dtx['emp_id']} - {$dtx['emp_name']}</td>";
    for ($d = $ds; $d <= $de; $d += 86400) {
        if (date('N', $d) > 5) {
            echo "<td class='danger smalltd'>";
        } else {
            echo "<td class='smalltd'>";
        }
        $first = TRUE;
        foreach ($emp_ts[date('Y-m-d', $d)] as $i => $code) {
            if ($first) {
                $first = FALSE;
            } else {
                echo "<br>";
            }
            echo "<abbr title=\"" . $emp_ts_desc[date('Y-m-d', $d)][$i] . "\">{$code}</abbr>";
        }
        echo "</td>";
    }
    echo "</tr>";
}
mysql_free_result($rsx);
echo "</table>";
echo "</div>";



?>
<?php

/*
 * profile
 * Azwari Nugraha <nugraha@duabelas.org>
 * Mar 3, 2014 11:39:32 AM
 */
error_reporting(E_ERROR | E_WARNING | E_PARSE);
if (!authenticated()) return;

$cgx_data = cgx_fetch_table("SELECT * FROM emp WHERE emp.emp_id = '" . user('emp_id') . "'");

echo "<div class='page-header'><h3>" . user('emp_name') . "</h3></div>";

echo "<form class='form-horizontal' role='form'>\n";
echo "<div class='panel panel-success'>\n";
echo "<div class='panel-heading'>Employee Information</div>\n";
echo "<div class='panel-body'>\n";
echo "        <div class='form-group'>\n";
echo "        <label class='col-sm-2 control-label' for='data_emp_id'>Emp No</label>\n";
echo "        <div class='col-sm-10'>\n";
echo "        <input readonly='readonly' class='form-control' id='data_emp_id' name='data[emp_id]' type='text' value=\"{$cgx_data['emp_id']}\" maxlength='3' style='text-align: left;width: 4em;' />\n";
echo "        </div>\n";
echo "        </div>\n";
echo "        <div class='form-group'>\n";
echo "        <label class='col-sm-2 control-label' for='data_emp_name'>Employee Name</label>\n";
echo "        <div class='col-sm-10'>\n";
echo "        <input readonly='readonly' class='form-control' id='data_emp_name' name='data[emp_name]' type='text' value=\"{$cgx_data['emp_name']}\" maxlength='50' style='text-align: left;width: 28em;'  />\n";
echo "        </div>\n";
echo "        </div>\n";
echo "        <div class='form-group'>\n";
echo "        <label class='col-sm-2 control-label' for='data_email'>Email</label>\n";
echo "        <div class='col-sm-10'>\n";
echo "        <input readonly='readonly' class='form-control' id='data_email' name='data[email]' type='text' value=\"{$cgx_data['email']}\" maxlength='50' style='text-align: left;width: 28em;'  />\n";
echo "        </div>\n";
echo "        </div>\n";
echo "        <div class='form-group'>\n";
echo "        <label class='col-sm-2 control-label' for='data_email'>Last Login</label>\n";
echo "        <div class='col-sm-10'>\n";
echo "        <input readonly='readonly' class='form-control' id='data_email' name='data[email]' type='text' value=\"" . date($APP_DATETIME_FORMAT, strtotime($cgx_data['last_login'])) . ' from ' . $cgx_data['last_ip'] . "\" maxlength='50' style='text-align: left;width: 28em;'  />\n";
echo "        </div>\n";
echo "        </div>\n";
echo "</div>\n";
echo "</div>\n";
echo "</form>\n";


echo "<form class='form-horizontal' role='form' action='action/profile.php' method='post' enctype='multipart/form-data'>\n";
echo "<div class='panel panel-success'>\n";
echo "<div class='panel-heading'>Update Avatar</div>\n";
echo "<div class='panel-body'>\n";

echo "<div class='row'>";

echo "<div class='col-sm-2 text-right'>";
echo "<img class='img-circle' src='" . user_image() . "'>";
echo "</div>";
echo "<div class='col-sm-10'>";
echo "Image file (.jpg) <input name='avatar-file' type='file'>";

echo "<div class='checkbox'>";
echo "<label>";
echo "<input name='avatar-del' type='checkbox'> Delete avatar";
echo "</label>";
echo "</div>";
          
echo "</div>";

echo "</div>";

echo "</div>\n";
echo "</div>\n";


echo "<div class='panel panel-success'>\n";
echo "<div class='panel-heading'>Change Password</div>\n";
echo "<div class='panel-body'>\n";

if ($_SESSION[$GLOBALS['APP_ID']]['profile']['error']) {
    echo "<div class='alert alert-danger'>{$_SESSION[$GLOBALS['APP_ID']]['profile']['error']}</div>";
    unset($_SESSION[$GLOBALS['APP_ID']]['profile']['error']);
}

if ($_SESSION[$GLOBALS['APP_ID']]['profile']['info']) {
    echo "<div class='alert alert-success'>{$_SESSION[$GLOBALS['APP_ID']]['profile']['info']}</div>";
    unset($_SESSION[$GLOBALS['APP_ID']]['profile']['info']);
}

echo "        <div class='form-group'>\n";
echo "        <label class='col-sm-2 control-label' for='passwd1'>Current Password</label>\n";
echo "        <div class='col-sm-10'>\n";
echo "        <input class='form-control' id='passwd1' name='passwd1' type='password' maxlength='16' style='text-align: left;width: 12em;' />\n";
echo "        </div>\n";
echo "        </div>\n";
echo "        <div class='form-group'>\n";
echo "        <label class='col-sm-2 control-label' for='passwd2'>New Password</label>\n";
echo "        <div class='col-sm-10'>\n";
echo "        <input class='form-control' id='passwd2' name='passwd2' type='password' maxlength='16' style='text-align: left;width: 12em;' />\n";
echo "        </div>\n";
echo "        </div>\n";
echo "        <div class='form-group'>\n";
echo "        <label class='col-sm-2 control-label' for='passwd3'>Confirm New Password</label>\n";
echo "        <div class='col-sm-10'>\n";
echo "        <input class='form-control' id='passwd3' name='passwd3' type='password' maxlength='16' style='text-align: left;width: 12em;' />\n";
echo "        </div>\n";
echo "        </div>\n";

echo "</div>\n";
echo "</div>\n";

echo "        <div class='form-group'>\n";
echo "        <div class='col-sm-offset-2 col-sm-10'>\n";
echo "        <input class='btn btn-primary' type='submit' value='Update'>\n";
echo "        </div>\n";
echo "        </div>\n";


echo "</form>\n";


?>
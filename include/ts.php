<?php

/**
 * (c) Azwari Nugraha <nugraha@pt-gai.com>
 * 02/04/2014 17:04:50
 */


error_reporting(E_ERROR | E_WARNING | E_PARSE);
echo "<h3>Timesheet</h3>";

function cgx_edit($data) {
    $href = "index.php?&p={$_REQUEST['p']}&pkey[user_id]={$data['record']['user_id']}";
    $out = "<a href='{$href}'><span style='margin: 0px 4px 0px 4px;' class='glyphicon glyphicon-edit' title='Edit this row'></span></a>";
    return $out;
}

function cgx_delete($data) {
    $href  = "javascript:if(confirm('Delete this row?')){window.location='action/app_user.php";
    $href .= "?backvar=index.php%253F%2526p%253Dapp_user&mode=delete&pkey[user_id]={$data['record']['user_id']}';}";
    $out = "<a href=\"{$href}\"><span style='margin: 0px 4px 0px 4px;' class='glyphicon glyphicon-remove-circle' title='Delete this row'></span></a>";
    return $out;
}
if (strlen($_REQUEST['pkey']['user_id']) == 0){
    require_once 'Structures/DataGrid.php';
    require_once 'HTML/Table.php';

    echo "<div class='panel panel-default'>";
    unset($_SESSION[$GLOBALS['APP_ID']]['app_user']['columns']);
    if (is_array($_SESSION[$GLOBALS['APP_ID']]['app_user']['columns'])) {
        $cgx_def_columns = $_SESSION[$GLOBALS['APP_ID']]['app_user']['columns'];
    } else {
        $cgx_def_columns = array(
            'user_id' => 1,
            'user_email' => 1,
            'user_fullname' => 1,
            'user_active' => 1,
            'user_last_ip' => 1,
            'user_last_login' => 1,
            'user_approval_date' => 1,
            'user_registration_ip' => 1,
            'user_verified' => 1,
            'user_phone' => 1,
            'user_phone_mobile' => 1,
            'role_name' => 1,
            'instansi' => 1,
            'kantor' => 1,
            'approved_by' => 1,
        );
        $_SESSION[$GLOBALS['APP_ID']]['app_user']['columns'] = $cgx_def_columns;
    }

    $cgx_sql = "select  app_user.user_id, app_user.user_email,app_user.user_fullname,app_user.user_active, app_user.user_last_ip, app_user.user_last_login, app_user.user_approval_date, app_user.user_registration_ip, 
    app_user.user_verified, app_user.user_phone, app_user.user_phone_mobile, role_name, m_business_partner.nama instansi, kantor, app_user2.user_fullname approved_by FROM app_user
LEFT JOIN app_role ON (app_user.role_id = app_role.role_id)
LEFT JOIN m_business_partner ON (app_user.m_business_partner_id = m_business_partner.m_business_partner_id)
LEFT JOIN app_user app_user2 ON (app_user.user_approved_by = app_user2.user_id)
LEFT JOIN core_kantor ON (app_user.id_r_kantor = core_kantor.id_r_kantor) WHERE 1 = 1";
    $cgx_datagrid = new Structures_DataGrid($cgx_max_rows);
    $cgx_options = array('dsn' => $cgx_dsn);
    
    echo "<div class='panel-heading'>";
    echo "<form role='form' class='form-inline' name='frmFILTER' action='{$_SERVER['SCRIPT_NAME']}'>\n";
    echo "<input type='hidden' name='p' value='{$_REQUEST['p']}'>\n";
    echo "<table id='bar' class='datagrid_bar' width='100%'><tr>\n";
    echo "<td>\n";
    echo "<table align='right' cellspacing='0' cellpadding='0' border='0'><tr>\n";
    echo "<td>Search<div class='input-group col-md-12'><input class='form-control input-sm' type='text' style='text-align: left;width: 20em;'name='q' value=\"{$cgx_search}\"><span class='input-group-addon'><span class='glyphicon glyphicon-search' onClick='frmFILTER.submit();' style='cursor:pointer;'></span></span></div></td>\n";
    echo "<td width='20'></td>\n";
    echo "</tr></table>\n";
    echo "</td>\n";
    echo "<td width='1' class='datagrid_bar_icon'><a title='Export all (CSV)' href='action/app_user.php?mode=export-all&cgx_search={$cgx_search}><span style='margin: 0px 4px 0px 4px;' class='glyphicon glyphicon-export'></span></a></td>\n";
    echo "</tr></table>\n";
    echo "</form>\n";
    echo "</div>";

    //if (strlen($cgx_filter1) > 0)
    $cgx_sql .= " and ( app_user.user_id LIKE '%{$cgx_search}%' OR app_user.user_fullname LIKE '%{$cgx_search}%' OR app_user.user_email LIKE '%{$cgx_search}%' OR app_user.user_phone LIKE '%{$cgx_search}%')";
    if ($_SESSION[$GLOBALS['APP_ID']]['app_user']['error']) {
        echo "<div class='alert alert-danger'>{$_SESSION[$GLOBALS['APP_ID']]['app_user']['error']}</div>";
        unset($_SESSION[$GLOBALS['APP_ID']]['ts']['error']);
    }

    if ($_SESSION[$GLOBALS['APP_ID']]['app_user']['info']) {
        echo "<div class='alert alert-success'>{$_SESSION[$GLOBALS['APP_ID']]['app_user']['info']}</div>";
        unset($_SESSION[$GLOBALS['APP_ID']]['ts']['info']);
    }

	
    $cgx_test = $cgx_datagrid->bind($cgx_sql, $cgx_options);
    if (PEAR::isError($cgx_test)) {
        echo $cgx_test->getMessage();
    }

    if ($cgx_def_columns['user_id'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('ID', 'user_id', 'user_id', array('align' => 'right'), NULL, NULL));
    if ($cgx_def_columns['user_email'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Email', 'user_email', 'user_email', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['user_fullname'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Nama', 'user_fullname', 'user_fullname', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['user_active'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Aktif', 'user_fullname', 'user_active', array('align' => 'center'), NULL, "cgx_format_yesno()"));
    if ($cgx_def_columns['user_last_ip'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('IP Terakhir', 'user_last_ip', 'user_last_ip', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['user_last_login'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Login Terakhir', 'user_last_login', 'user_last_login', array('align' => 'center'), NULL, "cgx_format_date()"));
    if ($cgx_def_columns['user_approval_date'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Tanggal Persetujuan', 'user_approval_date', 'user_approval_date', array('align' => 'center'), NULL, "cgx_format_date()"));
    if ($cgx_def_columns['user_registration_ip'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('IP Registrasi', 'user_registration_ip', 'user_registration_ip', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['user_verified'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Terverifikasi', 'user_verified', 'user_verified', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['user_phone'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Telepon', 'user_phone', 'user_phone', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['user_phone_mobile'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('HP', 'user_mobile_phone', 'user_phone_mobile', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['role_name'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Nama Role', 'role_name', 'role_name', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['instansi'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Instansi', 'instansi', 'instansi', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['kantor'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Kantor', 'kantor', 'kantor', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['approved_by'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Disetujui Oleh', 'approved_by', 'approved_by', array('align' => 'left'), NULL, NULL));
    if (has_privilege('app.user.edit')) $cgx_datagrid->addColumn(new Structures_DataGrid_Column(NULL, NULL, NULL, array('align' => 'center', 'width' => '1'), NULL, 'cgx_password()'));
    if (has_privilege('app.user.edit')) $cgx_datagrid->addColumn(new Structures_DataGrid_Column(NULL, NULL, NULL, array('align' => 'center', 'width' => '1'), NULL, 'cgx_edit()'));
    if (has_privilege('app.user.edit')) $cgx_datagrid->addColumn(new Structures_DataGrid_Column(NULL, NULL, NULL, array('align' => 'center', 'width' => '1'), NULL, 'cgx_delete()'));

    $cgx_table = new HTML_Table($cgx_TableAttribs);
    $cgx_tableHeader = & $cgx_table->getHeader();
    $cgx_datagrid->fill($cgx_table, $cgx_RendererOptions);
    $cgx_tableHeader->setRowAttributes(0, $cgx_HeaderAttribs);
    echo $cgx_table->toHtml();

    echo "<div class='panel-footer'>\n";
    echo "<div class='row'>\n";
    echo "<div class='col-sm-6 text-left text-info'>Found " . number_format($cgx_datagrid->getRecordCount()) . " record(s)</div>\n";
    echo "<div class='col-sm-6 text-right'>\n";
    $cgx_datagrid->render(DATAGRID_RENDER_PAGER);
    echo "</div>\n";
    echo "</div>\n";
    echo "</div>\n";
    echo "</div>\n";
}

?>
<script type="text/javascript">
    $("#date").datepicker({dateFormat: 'yy-mm-dd'});
</script>
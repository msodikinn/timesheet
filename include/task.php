<?php

/*
 * task
 * Azwari Nugraha <nugraha@pt-gai.com>
 * Oct 9, 2014 9:19:23 PM
 */
error_reporting(E_ERROR | E_WARNING | E_PARSE);
if (!authenticated()) return;

if ($_REQUEST['id'] == 'new') {
    include_once 'task.new.php';
    return;
} elseif ($_REQUEST['id'] && $_REQUEST['mode'] == 'edit') {
    include_once 'task.edit.php';
    return;
} elseif ($_REQUEST['id']) {
    include_once 'task.view.php';
    return;
}

echo "<div class='row'>";
echo "<div class='col-sm-8'><h3>Tasks</h3></div>";
if (has_privilege('project_manager')) echo "<div class='col-sm-4 text-right' style='padding-top: 10px;'><button onclick=\"window.location = 'index.php?m=task&id=new';\" class='btn btn-primary'><span class='glyphicon glyphicon-plus-sign'></span> Create New Task</button></div>";
echo "</div>";

function print_personnel($data) {
    global $APP_CONNECTION;
    $rsx = mysql_query("SELECT emp_id, emp_name FROM emp JOIN task_assign USING (emp_id) WHERE task_id = '{$data['record']['task_id']}' ORDER BY emp_name", $APP_CONNECTION);
    while ($dtx = mysql_fetch_array($rsx, MYSQL_ASSOC)) {
        if (isset($ret)) {
            $ret .= "<br>";
        }
        $ret .= $dtx['emp_name'];
    }
    mysql_free_result($rsx);
    return $ret;
}

function print_ctl($data) {
    return "<a href='index.php?m=task&id={$data['record']['task_id']}' title='View'><span class='glyphicon glyphicon-list-alt'></span></a>";
}

function print_comment($data) {
    return "<a href='index.php?m=task&id={$data['record']['task_id']}#divComments' title='View Comment'><span class='badge'>{$data['record']['comment_count']}</span></a>";
}

function print_progress($data) {
    if ($data['record'][$data['fieldName']] == 100) {
        return "<div class='text-success'>{$data['record'][$data['fieldName']]}%</div>";
    } else {
        return $data['record'][$data['fieldName']] . '%';
    }
}

function cgx_format_priority($data) {
    $arr = $GLOBALS['TASK_PRIORITY'];
    if ($data['record'][$data['fieldName']] == 1) {
        $class = 'text-danger';
    } elseif ($data['record'][$data['fieldName']] == 2) {
        $class = 'text-warning';
    } elseif ($data['record'][$data['fieldName']] == 3) {
        $class = 'text-info';
    } else {
        $class = 'text-success';
    }
    return "<div class='{$class}'>{$arr[$data['record'][$data['fieldName']]]}</div>";
}

function cgx_format_task_status($data) {
    $arr = $GLOBALS['TASK_STATUS'];
    $class = array(
        'C' => 'primary',
        'O' => 'warning',
        'F' => 'success',
        'X' => 'default',
        'L' => 'default'
    );
    return "<span style='padding: 2px 5px;' class='label label-{$class[$data['record'][$data['fieldName']]]}'>{$arr[$data['record'][$data['fieldName']]]}</span>";
}

require_once 'Structures/DataGrid.php';
require_once 'HTML/Table.php';

echo "<div class='panel panel-default'>";
if (is_array($_SESSION[$GLOBALS['APP_ID']]['task']['columns'])) {
    $cgx_def_columns = $_SESSION[$GLOBALS['APP_ID']]['task']['columns'];
} else {
    $cgx_def_columns = array(
        'task_id' => 1,
        'task_title' => 1,
        'start_date' => 1,
        'end_date' => 1,
        'weight' => 1,
        'priority' => 1,
        'progress' => 1,
        'task_status' => 1,
        'personnel' => 1,
        'emp_name' => 1,
        'project_name' => 1,
    );
    $_SESSION[$GLOBALS['APP_ID']]['task']['columns'] = $cgx_def_columns;
}

$cgx_sql = "SELECT task.*, project_name, emp_name, "
        . "(SELECT COUNT(task_comment_id) comment_count FROM task_comment WHERE task_id = task.task_id) comment_count "
        . "FROM task "
        . "JOIN project USING (project_id) "
        . "JOIN emp USING (emp_id) WHERE 1 = 1";
$cgx_datagrid = new Structures_DataGrid(100);
$cgx_datagrid->setDefaultSort(array('start_date' => 'DESC'));
$cgx_options = array('dsn' => $cgx_dsn);

$cgx_filter1 = urldecode($_REQUEST['f1']);
$cgx_filter2 = urldecode($_REQUEST['f2']);
$cgx_filter3 = urldecode($_REQUEST['f3']);
$cgx_filter4 = urldecode($_REQUEST['f4']);
$cgx_search = $_REQUEST['q'];

echo "<div class='panel-heading'>";
echo "<form role='form' class='form-inline' name='frmFILTER' action='{$_SERVER['SCRIPT_NAME']}'>\n";
echo "<input type='hidden' name='m' value='{$_REQUEST['m']}'>\n";
echo "<table id='bar' class='datagrid_bar' width='100%'><tr>\n";
echo "<td>\n";
echo "<table align='left' cellspacing='0' cellpadding='0' border='0'><tr>\n";
echo "<td>Project " . cgx_filter('f1', "SELECT project_id, project_name FROM project WHERE active = 'Y' ORDER BY project_name", $cgx_filter1, TRUE, "class='form-control input-sm'") . "</td>\n";
echo "<td width='20'></td>\n";
echo "</tr></table>\n";

echo "<table align='left' cellspacing='0' cellpadding='0' border='0'><tr>\n";
echo "<td>Priority " . cgx_filter('f2', $GLOBALS['TASK_PRIORITY'], $cgx_filter2, TRUE, "class='form-control input-sm'") . "</td>\n";
echo "<td width='20'></td>\n";
echo "</tr></table>\n";

echo "<table align='left' cellspacing='0' cellpadding='0' border='0'><tr>\n";
echo "<td>Status " . cgx_filter('f3', $GLOBALS['TASK_STATUS'], $cgx_filter3, TRUE, "class='form-control input-sm'") . "</td>\n";
echo "<td width='20'></td>\n";
echo "</tr></table>\n";

echo "<table align='left' cellspacing='0' cellpadding='0' border='0'><tr>\n";
echo "<td>Show " . cgx_filter('f4', array('M' => 'My Tasks'), $cgx_filter4, TRUE, "class='form-control input-sm'") . "</td>\n";
echo "<td width='20'></td>\n";
echo "</tr></table>\n";

echo "</td>\n";
echo "<td align=right width='200'><div class='input-group col-md-12'><input class='form-control input-sm' type='text' name='q' value=\"{$cgx_search}\"><span class='input-group-addon'><span class='glyphicon glyphicon-search' onClick='frmFILTER.submit();' style='cursor:pointer;'></span></span></div></td>\n";
echo "<td></td>\n";
echo "<td width='20'></td>\n";
echo "<td width='1' class='datagrid_bar_icon'><a title='Export all (CSV)' href='action/task.php?mode=export-all'><span style='margin: 0px 4px 0px 4px;' class='glyphicon glyphicon-export'></span></a></td>\n";
echo "<td width='1' class='datagrid_bar_icon'><a title='Customize columns' href='javascript:customizeColumn(true);'><span style='margin: 0px 4px 0px 4px;' class='glyphicon glyphicon-list'></span></a></td>\n";
echo "</tr></table>\n";
echo "</form>\n";
echo "</div>";

echo "<div id='columns' class='panel-body' style='display: none;'>\n";
echo "<form name='frmCUSTOMIZE' action='action/cgx_customize.php' method='post'>\n";
echo "<input type='hidden' name='back' value='" . urlencode($_SERVER['REQUEST_URI']) . "'>\n";
echo "<input type='hidden' name='dg_name' value='task'>\n";
echo "<input type='hidden' name='col[task_id]' value='on'>\n";
echo "<div class='form-group'>";
echo "<label class='checkbox-inline'><input disabled checked type='checkbox' name='col[task_id]'> No</label>";
echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['task_title'] == 1 ? ' checked' : '') . " type='checkbox' name='col[task_title]'> Task</label>";
echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['project_id'] == 1 ? ' checked' : '') . " type='checkbox' name='col[project_id]'> Project Name</label>";
echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['project_name'] == 1 ? ' checked' : '') . " type='checkbox' name='col[project_name]'> Project Name</label>";
echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['task_description'] == 1 ? ' checked' : '') . " type='checkbox' name='col[task_description]'> Description</label>";
echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['start_date'] == 1 ? ' checked' : '') . " type='checkbox' name='col[start_date]'> Start Date</label>";
echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['end_date'] == 1 ? ' checked' : '') . " type='checkbox' name='col[end_date]'> End Date</label>";
echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['closing_date'] == 1 ? ' checked' : '') . " type='checkbox' name='col[closing_date]'> Closing Date</label>";
echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['weight'] == 1 ? ' checked' : '') . " type='checkbox' name='col[weight]'> Weight</label>";
echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['priority'] == 1 ? ' checked' : '') . " type='checkbox' name='col[priority]'> Priority</label>";
echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['progress'] == 1 ? ' checked' : '') . " type='checkbox' name='col[progress]'> Progress</label>";
echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['task_status'] == 1 ? ' checked' : '') . " type='checkbox' name='col[task_status]'> Status</label>";
echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['created_date'] == 1 ? ' checked' : '') . " type='checkbox' name='col[created_date]'> Created Date</label>";
echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['update_date'] == 1 ? ' checked' : '') . " type='checkbox' name='col[update_date]'> Update Date</label>";
echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['personnel'] == 1 ? ' checked' : '') . " type='checkbox' name='col[personnel]'> Personnel</label>";
echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['emp_name'] == 1 ? ' checked' : '') . " type='checkbox' name='col[emp_name]'> Created By</label>";
echo "</div>";
echo "<input class='btn btn-warning' type='submit' value='Update'>\n";
echo "<input class='btn btn-default' type='button' value='Cancel' onclick='customizeColumn(false);'>\n";
echo "</form>\n";
echo "</div>\n";
?>
<script type="text/javascript">
<!--
function customizeColumn(s) {
    var divCols = document.getElementById('columns');
    var divBar = document.getElementById('bar');
    if (s) {
        divCols.style.display = 'block';
        divBar.style.display = 'none';
    } else {
        window.location = window.location;
    }
}
//-->
</script>
<?php

if (strlen($cgx_filter1) > 0) $cgx_sql .= " AND task.project_id = '" . mysql_escape_string($cgx_filter1) . "'";
if (strlen($cgx_filter2) > 0) $cgx_sql .= " AND task.priority = '" . mysql_escape_string($cgx_filter2) . "'";
if (strlen($cgx_filter3) > 0) $cgx_sql .= " AND task.task_status = '" . mysql_escape_string($cgx_filter3) . "'";

if (strlen($cgx_filter4) > 0) {
    $cgx_sql .= " AND (task_id IN (SELECT task_id FROM task_assign WHERE emp_id = '" . user() . "') OR emp_id = '" . user() . "')";
}

$cgx_sql .= " and ( task.task_id LIKE '%{$cgx_search}%' OR task.task_title LIKE '%{$cgx_search}%' OR task.task_description LIKE '%{$cgx_search}%')";
if ($_SESSION[$GLOBALS['APP_ID']]['task']['error']) {
    echo "<div class='alert alert-danger'>{$_SESSION[$GLOBALS['APP_ID']]['task']['error']}</div>";
    unset($_SESSION[$GLOBALS['APP_ID']]['task']['error']);
}

if ($_SESSION[$GLOBALS['APP_ID']]['task']['info']) {
    echo "<div class='alert alert-success'>{$_SESSION[$GLOBALS['APP_ID']]['task']['info']}</div>";
    unset($_SESSION[$GLOBALS['APP_ID']]['task']['info']);
}


$cgx_test = $cgx_datagrid->bind($cgx_sql, $cgx_options);
if (PEAR::isError($cgx_test)) {
    echo $cgx_test->getMessage();
}

if ($cgx_def_columns['task_id'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('<div class="text-right">ID</div>', 'task_id', 'task_id', array('align' => 'right'), NULL, NULL));
if ($cgx_def_columns['task_title'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Task', 'task_title', 'task_title', array('align' => 'left'), NULL, NULL));
if ($cgx_def_columns['project_id'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Project Name', 'project_id', 'project_id', array('align' => 'left'), NULL, NULL));
if ($cgx_def_columns['project_name'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Project Name', 'project_name', 'project_name', array('align' => 'left'), NULL, NULL));
if ($cgx_def_columns['task_description'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Description', 'task_description', 'task_description', array('align' => 'left'), NULL, NULL));
if ($cgx_def_columns['start_date'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('<div class="text-center">Start Date</div>', 'start_date', 'start_date', array('align' => 'center'), NULL, "cgx_format_date()"));
if ($cgx_def_columns['end_date'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('<div class="text-center">End Date</div>', 'end_date', 'end_date', array('align' => 'center'), NULL, "cgx_format_date()"));
if ($cgx_def_columns['closing_date'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Closing Date', 'closing_date', 'closing_date', array('align' => 'center'), NULL, "cgx_format_date()"));
if ($cgx_def_columns['weight'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('<div class="text-right">Weight</div>', 'weight', 'weight', array('align' => 'right'), NULL, NULL));
if ($cgx_def_columns['priority'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Priority', 'priority', 'priority', array('align' => 'left'), NULL, "cgx_format_priority()"));
if ($cgx_def_columns['progress'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('<div class="text-right">Progress</div>', 'progress', 'progress', array('align' => 'right'), NULL, "print_progress()"));
if ($cgx_def_columns['task_status'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Status', 'task_status', 'task_status', array('align' => 'left'), NULL, "cgx_format_task_status()"));
if ($cgx_def_columns['created_date'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Created Date', 'created_date', 'created_date', array('align' => 'center'), NULL, "cgx_format_timestamp()"));
if ($cgx_def_columns['update_date'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Update Date', 'update_date', 'update_date', array('align' => 'center'), NULL, "cgx_format_timestamp()"));
if ($cgx_def_columns['personnel'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Personnel', NULL, NULL, array('align' => 'left'), NULL, "print_personnel()"));
if ($cgx_def_columns['emp_name'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Created By', 'emp_name', 'emp_name', array('align' => 'left'), NULL, NULL));
$cgx_datagrid->addColumn(new Structures_DataGrid_Column("<span class='glyphicon glyphicon-comment'></span>", 'comment_count', 'comment_count', array('align' => 'center'), NULL, "print_comment()"));
$cgx_datagrid->addColumn(new Structures_DataGrid_Column(NULL, NULL, NULL, array('align' => 'center'), NULL, "print_ctl()"));

$cgx_table = new HTML_Table($cgx_TableAttribs);
$cgx_tableHeader = & $cgx_table->getHeader();
$cgx_datagrid->fill($cgx_table, $cgx_RendererOptions);
$cgx_tableHeader->setRowAttributes(0, $cgx_HeaderAttribs);
echo $cgx_table->toHtml();

echo "<div class='panel-footer'>\n";
echo "<div class='row'>\n";
echo "<div class='col-sm-6 text-left text-info'>Found " . number_format($cgx_datagrid->getRecordCount()) . " record(s)</div>\n";
echo "<div class='col-sm-6 text-right'>\n";
$cgx_datagrid->render(DATAGRID_RENDER_PAGER);
echo "</div>\n";
echo "</div>\n";
echo "</div>\n";
echo "</div>\n";

?>
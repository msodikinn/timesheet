<?php

/**
 * (c) Azwari Nugraha <nugraha@duabelas.org>
 * 03/03/2014 12:16:45
 */


echo "<div class='page-header'><h3>Activity / Output</h3></div>";

function cgx_edit($data) {
    $href = "index.php?&m={$_REQUEST['m']}&pkey[activity_id]={$data['record']['activity_id']}";
    $out = "<a href='{$href}'><img title='Edit this row' src='images/icon_edit.png' border='0'></a>";
    return $out;
}

function cgx_delete($data) {
    $href  = "javascript:if(confirm('Delete this row?')){window.location='action/activity.php";
    $href .= "?backvar=index.php%253F%2526m%253Dactivity&mode=delete&pkey[activity_id]={$data['record']['activity_id']}';}";
    $out = "<a href=\"{$href}\"><img title='Delete this row' src='images/icon_delete.png' border='0'></a>";
    return $out;
}

if (strlen($_REQUEST['pkey']['activity_id']) > 0) {
    $cgx_id = $_REQUEST['id'];
    $cgx_data = cgx_fetch_table("SELECT * FROM activity WHERE activity.activity_id = '" . mysql_escape_string($_REQUEST['pkey']['activity_id']) . "'");


    echo "<div class='panel panel-default'>\n";
    echo "<div class='panel-body'>\n";
    echo "<form class='form-horizontal' role='form' action='action/activity.php' method='post'>\n";
    echo "<input type='hidden' name='backvar' value='" . urlencode("index.php?&m={$_REQUEST['m']}") . "'>\n";
    echo "<input type='hidden' name='mode' value='" . ($_REQUEST['pkey']['activity_id'] == '0' ? 'new' : 'update') . "'>\n";
    echo "<input type='hidden' name='pkey[activity_id]' value=\"{$_REQUEST['pkey']['activity_id']}\">\n";
    echo "<input type='hidden' name='table' value='activity'>\n";

    if ($_SESSION[$GLOBALS['APP_ID']]['activity']['error']) {
        echo "<div class='alert alert-danger'>{$_SESSION[$GLOBALS['APP_ID']]['activity']['error']}</div>";
        unset($_SESSION[$GLOBALS['APP_ID']]['activity']['error']);
    }

    if ($_SESSION[$GLOBALS['APP_ID']]['activity']['info']) {
        echo "<div class='alert alert-success'>{$_SESSION[$GLOBALS['APP_ID']]['activity']['info']}</div>";
        unset($_SESSION[$GLOBALS['APP_ID']]['activity']['info']);
    }

    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_activity_id'>Activity ID</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo "        <input class='form-control' id='data_activity_id' name='data[activity_id]' type='text' value=\"{$cgx_data['activity_id']}\" maxlength='1' style='text-align: left;width: 2em;' " . ($_REQUEST['pkey']['activity_id'] == '0' ? '' : ' disabled') . " />\n";
    echo "        </div>\n";
    echo "        </div>\n";
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_activity_name'>Activity Name</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo "        <input class='form-control' id='data_activity_name' name='data[activity_name]' type='text' value=\"{$cgx_data['activity_name']}\" maxlength='30' style='text-align: left;width: 31em;'  />\n";
    echo "        </div>\n";
    echo "        </div>\n";
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_active'>Active</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo cgx_form_select('data[active]', array('Y' => 'Yes', 'N' => 'No'), $cgx_data['active'], FALSE, "id='data_active'");
    echo "        </div>\n";
    echo "        </div>\n";
    echo "        <div class='form-group'>\n";
    echo "        <div class='col-sm-offset-2 col-sm-10'>\n";
    echo "        <input class='btn btn-primary' type='submit' value='Submit'>\n";
    echo "        <input class='btn btn-warning' type='button' value='Back' onclick=\"window.location = 'index.php?&m=activity';\">\n";
    echo "        </div>\n";
    echo "        </div>\n";
    echo "</form>\n";
    echo "</div>\n";
    echo "</div>\n";

} else {
    require_once 'Structures/DataGrid.php';
    require_once 'HTML/Table.php';

    echo "<div class='panel panel-default'>";
    if (is_array($_SESSION[$GLOBALS['APP_ID']]['activity']['columns'])) {
        $cgx_def_columns = $_SESSION[$GLOBALS['APP_ID']]['activity']['columns'];
    } else {
        $cgx_def_columns = array(
            'activity_id' => 1,
            'activity_name' => 1,
            'active' => 1,
        );
        $_SESSION[$GLOBALS['APP_ID']]['activity']['columns'] = $cgx_def_columns;
    }

    $cgx_sql = "SELECT * FROM activity WHERE 1 = 1";
    $cgx_datagrid = new Structures_DataGrid($cgx_max_rows);
    $cgx_options = array('dsn' => $cgx_dsn);

    $cgx_search = $_REQUEST['q'];

    echo "<div class='panel-heading'>";
    echo "<form name='frmFILTER' action='{$_SERVER['SCRIPT_NAME']}'>\n";
    echo "<input type='hidden' name='m' value='{$_REQUEST['m']}'>\n";
    echo "<table id='bar' class='datagrid_bar' width='100%'><tr>\n";
    echo "<td align='right'><input type='text' size='20' name='q' value=\"{$cgx_search}\"></td>\n";
    echo "<td width='1'><input title='Search' type='image' src='images/icon_search.png' border='0' style='padding-right: 20px;'></td>\n";
    echo "<td></td>\n";
    echo "<td width='20'></td>\n";
    if (has_privilege('admin')) {
        echo "<td width='1' class='datagrid_bar_icon'><a title='New record' href='index.php?&m={$_REQUEST['m']}&pkey[activity_id]=0'><img border='0' src='images/icon_add.png'></a></td>\n";
    } else {
        echo "<td width='1' class='datagrid_bar_icon'><img border='0' src='images/icon_add_dis.png'></td>\n";
    }
    echo "<td width='1' class='datagrid_bar_icon'><a title='Export all (CSV)' href='action/activity.php?mode=export-all'><img border='0' src='images/icon_csv.png'></a></td>\n";
    echo "<td width='1' class='datagrid_bar_icon'><a title='Customize columns' href='javascript:customizeColumn(true);'><img border='0' src='images/icon_columns.png'></a></td>\n";
    echo "</tr></table>\n";
    echo "</form>\n";
    echo "</div>";

    echo "<div id='columns' class='panel-body' style='display: none;'>\n";
    echo "<form name='frmCUSTOMIZE' action='action/cgx_customize.php' method='post'>\n";
    echo "<input type='hidden' name='back' value='" . urlencode($_SERVER['REQUEST_URI']) . "'>\n";
    echo "<input type='hidden' name='dg_name' value='activity'>\n";
    echo "<div class='form-group'>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['activity_id'] == 1 ? ' checked' : '') . " type='checkbox' name='col[activity_id]'> Activity ID</label>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['activity_name'] == 1 ? ' checked' : '') . " type='checkbox' name='col[activity_name]'> Activity Name</label>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['active'] == 1 ? ' checked' : '') . " type='checkbox' name='col[active]'> Active</label>";
    echo "</div>";
    echo "<input class='btn btn-primary' type='submit' value='Update'>\n";
    echo "<input class='btn btn-warning' type='button' value='Cancel' onclick='customizeColumn(false);'>\n";
    echo "</form>\n";
    echo "</div>\n";
?>
<script type="text/javascript">
<!--
function customizeColumn(s) {
    var divCols = document.getElementById('columns');
    var divBar = document.getElementById('bar');
    if (s) {
        divCols.style.display = 'block';
        divBar.style.display = 'none';
    } else {
        window.location = window.location;
    }
}
//-->
</script>
<?php
    $cgx_sql .= " and ( activity.activity_id LIKE '%{$cgx_search}%' OR activity.activity_name LIKE '%{$cgx_search}%')";
    if ($_SESSION[$GLOBALS['APP_ID']]['activity']['error']) {
        echo "<div class='alert alert-danger'>{$_SESSION[$GLOBALS['APP_ID']]['activity']['error']}</div>";
        unset($_SESSION[$GLOBALS['APP_ID']]['activity']['error']);
    }

    if ($_SESSION[$GLOBALS['APP_ID']]['activity']['info']) {
        echo "<div class='alert alert-success'>{$_SESSION[$GLOBALS['APP_ID']]['activity']['info']}</div>";
        unset($_SESSION[$GLOBALS['APP_ID']]['activity']['info']);
    }


    $cgx_test = $cgx_datagrid->bind($cgx_sql, $cgx_options);
    if (PEAR::isError($cgx_test)) {
        echo $cgx_test->getMessage();
    }

    if ($cgx_def_columns['activity_id'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Activity ID', 'activity_id', 'activity_id', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['activity_name'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Activity Name', 'activity_name', 'activity_name', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['active'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Active', 'active', 'active', array('align' => 'left'), NULL, "cgx_format_yesno()"));
    if (has_privilege('admin')) $cgx_datagrid->addColumn(new Structures_DataGrid_Column(NULL, NULL, NULL, array('align' => 'center', 'width' => '1'), NULL, 'cgx_edit()'));
    if (has_privilege('admin')) $cgx_datagrid->addColumn(new Structures_DataGrid_Column(NULL, NULL, NULL, array('align' => 'center', 'width' => '1'), NULL, 'cgx_delete()'));

    $cgx_table = new HTML_Table($cgx_TableAttribs);
    $cgx_tableHeader = & $cgx_table->getHeader();
    $cgx_tableBody = & $cgx_table->getBody();

    $cgx_test = $cgx_datagrid->fill($cgx_table, $cgx_RendererOptions);
    if (PEAR::isError($cgx_test)) {
        echo $cgx_test->getMessage();
    }

    $cgx_tableHeader->setRowAttributes(0, $cgx_HeaderAttribs);
    $cgx_tableBody->altRowAttributes(0, $cgx_EvenRowAttribs, $cgx_OddRowAttribs, TRUE);

    echo $cgx_table->toHtml();

    echo "<table width='100%'><tr>\n";
    echo "<td class='datagrid_pager'>Found " . number_format($cgx_datagrid->getRecordCount()) . " record(s)</td>\n";
    echo "<td align='right' class='datagrid_pager'>\n";
    $cgx_test = $cgx_datagrid->render(DATAGRID_RENDER_PAGER);
    if (PEAR::isError($cgx_test)) {
        echo $cgx_test->getMessage();
    }
    echo "</td></tr></table>\n";
    echo "</div>\n";
}

?>
<?php

/**
 * (c) Azwari Nugraha <nugraha@duabelas.org>
 * 03/03/2014 12:19:10
 */

if (!has_privilege('project_manager')) return;

echo "<div class='page-header'><h3>Project Summary</h3></div>";

require_once 'Structures/DataGrid.php';
require_once 'HTML/Table.php';

echo "<div class='panel panel-default'>";
if (is_array($_SESSION[$GLOBALS['APP_ID']]['project-summary']['columns'])) {
    $cgx_def_columns = $_SESSION[$GLOBALS['APP_ID']]['project-summary']['columns'];
} else {
    $cgx_def_columns = array(
        'project_id' => 1,
        'project_name' => 1,
        'first_act' => 1,
        'last_act' => 1,
        'manpower' => 1,
        'days' => 1,
        'mandays' => 1
    );
    $_SESSION[$GLOBALS['APP_ID']]['project-summary']['columns'] = $cgx_def_columns;
}

$cgx_sql = "SELECT project_id, project_name, MIN(ts_date) first_act, MAX(ts_date) last_act, COUNT(DISTINCT ts_date) days, COUNT(DISTINCT emp_id) manpower, COUNT(*) mandays FROM (SELECT DISTINCT project_id, emp_id, ts_date FROM ts) T1 JOIN project USING (project_id) GROUP BY project_id, project_name";
$cgx_datagrid = new Structures_DataGrid($cgx_max_rows);
$cgx_options = array('dsn' => $cgx_dsn);
$cgx_datagrid->setDefaultSort(array('mandays' => 'DESC'));
        
$cgx_filter1 = urldecode($_REQUEST['f1']);
$cgx_search = $_REQUEST['q'];

echo "<div class='panel-heading'>";
echo "<form name='frmFILTER' action='{$_SERVER['SCRIPT_NAME']}'>\n";
echo "<input type='hidden' name='m' value='{$_REQUEST['m']}'>\n";
echo "<table id='bar' class='datagrid_bar' width='100%'><tr>\n";
echo "<td>\n";
echo "<table align='left' cellspacing='0' cellpadding='0' border='0'><tr>\n";
echo "<td><nobr><label for='f1'>Active</label></nobr></td>\n";
echo "<td>&nbsp;</td>\n";
echo "<td>" . cgx_filter('f1', array('Y' => 'Yes', 'N' => 'No'), $cgx_filter1, TRUE) . "</td>\n";
echo "<td width='20'></td>\n";
echo "</tr></table>\n";
echo "</td>\n";
echo "</tr></table>\n";
echo "</form>\n";
echo "</div>";

echo "<div id='columns' class='panel-body' style='display: none;'>\n";
echo "<form name='frmCUSTOMIZE' action='action/cgx_customize.php' method='post'>\n";
echo "<input type='hidden' name='back' value='" . urlencode($_SERVER['REQUEST_URI']) . "'>\n";
echo "<input type='hidden' name='dg_name' value='project'>\n";
echo "<input type='hidden' name='col[project_id]' value='on'>\n";
echo "<input type='hidden' name='col[project_name]' value='on'>\n";
echo "</div>\n";
?>
<script type="text/javascript">
<!--
function customizeColumn(s) {
    var divCols = document.getElementById('columns');
    var divBar = document.getElementById('bar');
    if (s) {
        divCols.style.display = 'block';
        divBar.style.display = 'none';
    } else {
        window.location = window.location;
    }
}
//-->
</script>
<?php
if (strlen($cgx_filter1) > 0) $cgx_sql .= " AND project.active = '" . mysql_escape_string($cgx_filter1) . "'";
$cgx_sql .= " and ( project.project_id LIKE '%{$cgx_search}%' OR project.project_name LIKE '%{$cgx_search}%' OR project.description LIKE '%{$cgx_search}%' OR project.company_name LIKE '%{$cgx_search}%')";
if ($_SESSION[$GLOBALS['APP_ID']]['project-summary']['error']) {
    echo "<div class='alert alert-danger'>{$_SESSION[$GLOBALS['APP_ID']]['project-summary']['error']}</div>";
    unset($_SESSION[$GLOBALS['APP_ID']]['project-summary']['error']);
}

if ($_SESSION[$GLOBALS['APP_ID']]['project-summary']['info']) {
    echo "<div class='alert alert-success'>{$_SESSION[$GLOBALS['APP_ID']]['project-summary']['info']}</div>";
    unset($_SESSION[$GLOBALS['APP_ID']]['project-summary']['info']);
}


$cgx_test = $cgx_datagrid->bind($cgx_sql, $cgx_options);
if (PEAR::isError($cgx_test)) {
    echo $cgx_test->getMessage();
}

if ($cgx_def_columns['project_id'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('ID', 'project_id', 'project_id', array('align' => 'right'), NULL, NULL));
if ($cgx_def_columns['project_name'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Project Name', 'project_name', 'project_name', array('align' => 'left'), NULL, NULL));
if ($cgx_def_columns['first_act'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('First Activity', 'first_act', 'first_act', array('align' => 'left'), NULL, "cgx_format_date()"));
if ($cgx_def_columns['last_act'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Last Activity', 'last_act', 'last_act', array('align' => 'left'), NULL, "cgx_format_date()"));
if ($cgx_def_columns['manpower'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Manpower', 'manpower', 'manpower', array('align' => 'left', 'width' => '10%'), NULL, NULL));
if ($cgx_def_columns['days'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Days', 'days', 'days', array('align' => 'left', 'width' => '10%'), NULL, NULL));
if ($cgx_def_columns['mandays'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Mandays', 'mandays', 'mandays', array('align' => 'left', 'width' => '10%'), NULL, NULL));

$cgx_table = new HTML_Table($cgx_TableAttribs);
$cgx_tableHeader = & $cgx_table->getHeader();
$cgx_tableBody = & $cgx_table->getBody();

$cgx_test = $cgx_datagrid->fill($cgx_table, $cgx_RendererOptions);
if (PEAR::isError($cgx_test)) {
    echo $cgx_test->getMessage();
}

$cgx_tableHeader->setRowAttributes(0, $cgx_HeaderAttribs);
$cgx_tableBody->altRowAttributes(0, $cgx_EvenRowAttribs, $cgx_OddRowAttribs, TRUE);

echo $cgx_table->toHtml();

echo "<table width='100%'><tr>\n";
echo "<td class='datagrid_pager'>Found " . number_format($cgx_datagrid->getRecordCount()) . " record(s)</td>\n";
echo "<td align='right' class='datagrid_pager'>\n";
$cgx_test = $cgx_datagrid->render(DATAGRID_RENDER_PAGER);
if (PEAR::isError($cgx_test)) {
    echo $cgx_test->getMessage();
}
echo "</td></tr></table>\n";
echo "</div>\n";

?>
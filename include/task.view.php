<?php

/*
 * task
 * Azwari Nugraha <nugraha@pt-gai.com>
 * Oct 9, 2014 11:03:18 PM
 */

if (!authenticated()) return;

$task = npl_fetch_table(
        "SELECT task.*, project_name, emp_name, email "
        . "FROM task "
        . "JOIN emp USING (emp_id) "
        . "JOIN project USING (project_id) "
        . "WHERE task_id = '{$_REQUEST['id']}'", $APP_CONNECTION);
$is_owner = is_task_owner($task['task_id']);
$is_member = is_task_member($task['task_id']);

$rsx = mysql_query("SELECT emp.*, task_assign_id, task_id "
        . "FROM task_assign "
        . "JOIN emp USING (emp_id) "
        . "WHERE task_id = '{$task['task_id']}' "
        . "ORDER BY emp_name", $APP_CONNECTION);
while ($dtx = mysql_fetch_array($rsx, MYSQL_ASSOC)) {
    if (isset($personnel)) $personnel .= '<br>';
    $personnel .= format_emp($dtx);
    if ($is_owner && in_array($task['task_status'], array('C', 'O', 'F'))) {
        $personnel .= " <button onclick=\"if(confirm('Delete personnel?')){window.location = 'action/task.view.php?mode=delete-personnel&id={$dtx['task_assign_id']}&task_id={$dtx['task_id']}';}\" class='btn-link'><span class='text-danger glyphicon glyphicon-remove-circle'></span></button>";
    }
}
mysql_free_result($rsx);
        
echo "<div class='page-header'><h3>Task #{$task['task_id']}: {$task['task_title']}</h3></div>";

echo "<div class='text-right' style='margin-top: -15px;'>";
echo "<div class='btn-group'>";
if (in_array($task['task_status'], array('C', 'O', 'F'))) echo "<a href=\"#divComments\" class='btn btn-link'><span class='glyphicon glyphicon-bullhorn'></span> Add Comment</a>";
if (($is_owner || $is_member) && in_array($task['task_status'], array('C', 'O', 'F'))) echo "<button onclick=\"$('#divAssign').hide();$('#divAttachment').hide();$('#divProgress').toggle('slow');\" class='btn btn-link'><span class='glyphicon glyphicon-tasks'></span> Update Progress</button>";
if ($is_owner && in_array($task['task_status'], array('C', 'O', 'F')))  echo "<button onclick=\"$('#divProgress').hide();$('#divAttachment').hide();$('#divAssign').toggle('slow');\" class='btn btn-link'><span class='glyphicon glyphicon-user'></span> Add Assignment</button>";
if ($is_owner && in_array($task['task_status'], array('C', 'O', 'F')))  echo "<button onclick=\"$('#divProgress').hide();$('#divAssign').hide();$('#divAttachment').toggle('slow');\" class='btn btn-link'><span class='glyphicon glyphicon-paperclip'></span> Upload Attachment</button>";
if ($is_owner && in_array($task['task_status'], array('C', 'O', 'F')))  echo "<a href=\"index.php?m=task&id={$task['task_id']}&mode=edit\" class='btn btn-link'><span class='glyphicon glyphicon-edit'></span> Edit</a>";
if ($is_owner && $task['progress'] == 100 && !in_array($task['task_status'], array('L', 'X'))) echo "<button onclick=\"if(confirm('You are about to close task #{$task['task_id']}.\\nAre you sure?')){window.location = 'action/task.view.php?mode=close&id={$task['task_id']}';}\" class='btn btn-link'><span class='glyphicon glyphicon-ok-sign'></span> Close</button>";
if ($is_owner && in_array($task['task_status'], array('C', 'O', 'F'))) echo "<button onclick=\"if(confirm('You are about to cancel task #{$task['task_id']}.\\nAre you sure?')){window.location = 'action/task.view.php?mode=cancel&id={$task['task_id']}';}\" class='btn btn-link'><span class='glyphicon glyphicon-stop'></span> Cancel</button>";
if ($is_owner && in_array($task['task_status'], array('C', 'O', 'F', 'X'))) echo "<button onclick=\"if(confirm('You are about to delete task #{$task['task_id']}.\\nAre you sure?')){window.location = 'action/task.view.php?mode=delete&id={$task['task_id']}';}\" class='btn btn-link'><span class='glyphicon glyphicon-trash'></span> Delete</button>";

echo "<a href=\"index.php?m=task\" class='btn btn-link'><span class='glyphicon glyphicon-backward'></span> Back</a>";
echo "</div>";
echo "</div>";

// start update progress
echo "<div id='divProgress' style='display: none;' class='panel panel-default'>";
echo "<div class='panel-heading'><h4 class='panel-title'>Update Progress</h4></div>";
echo "<div class='panel-body'>";
echo "<form class='form-horizontal' role='form' action='action/task.view.php' method='post'>";
echo "<input type='hidden' name='mode' value='progress'>";
echo "<input type='hidden' name='task_id' value='{$task['task_id']}'>";

echo "<div class='form-group'>";
echo "<label for='progress' class='col-sm-2 control-label'>Current Progress</label>";
echo "<div class='col-sm-10'>";
echo "<select class='form-control' name='progress' id='progress' style='width: 10em;'>";
for ($n = 0; $n <= 100; $n+=5) {
    if ($n == $task['progress']) {
        echo "<option selected value='{$n}'>{$n} %</option>";
    } else {
        echo "<option value='{$n}'>{$n} %</option>";
    }
}
echo "</select>";
echo "</div>";
echo "</div>";

echo "<div class='form-group'>";
echo "<div class='col-sm-2'></div>";
echo "<div class='col-sm-10'>";
echo "<input class='btn btn-primary' type='submit' value='Update'>";
echo "</div>";
echo "</div>";

echo "</form>";
echo "</div>";
echo "</div>";
// end update progress


// start upload attachment
echo "<div id='divAttachment' style='display: none;' class='panel panel-default'>";
echo "<div class='panel-heading'><h4 class='panel-title'>Upload Attachment</h4></div>";
echo "<div class='panel-body'>";
echo "<form class='form-horizontal' role='form' action='action/task.view.php' method='post' enctype='multipart/form-data'>";
echo "<input type='hidden' name='mode' value='attachment'>";
echo "<input type='hidden' name='id' value='{$task['task_id']}'>";

echo "<div class='form-group'>";
echo "<label for='attachment' class='col-sm-2 control-label'>Attachment</label>";
echo "<div class='col-sm-10'>";
echo "<input name='attachment' id='attachment' type='file' title='Select file...'>";
echo "</div>";
echo "</div>";

echo "<div class='form-group'>";
echo "<div class='col-sm-2'></div>";
echo "<div class='col-sm-10'>";
echo "<input class='btn btn-primary' type='submit' value='Upload'>";
echo "</div>";
echo "</div>";

echo "</form>";
echo "</div>";
echo "</div>";

echo "<script type='text/javascript'>\n";
echo "$('input[type=file]').bootstrapFileInput();\n";
echo "</script>";
// end upload attachment


// start add assignment
echo "<div id='divAssign' style='display: none;' class='panel panel-default'>";
echo "<div class='panel-heading'><h4 class='panel-title'>Add Assignment</h4></div>";
echo "<div class='panel-body'>";
echo "<form class='form-horizontal' role='form' action='action/task.view.php' method='post'>";
echo "<input type='hidden' name='mode' value='assignment'>";
echo "<input type='hidden' name='task_id' value='{$task['task_id']}'>";

echo "<div class='form-group'>";
echo "<label for='personnel' class='col-sm-2 control-label'>Personnel</label>";
echo "<div class='col-sm-10'>";
echo cgx_form_select('personnel', "SELECT emp_id, CONCAT(emp_name, ' - ', emp_id, '') emp_name FROM emp WHERE active = 'Y' AND is_employee = 'Y' AND emp_id NOT IN (SELECT emp_id FROM task_assign WHERE task_id = '{$task['task_id']}') ORDER BY emp_name", NULL, FALSE, "id='personnel'");
echo "</div>";
echo "</div>";
 
echo "<div class='form-group'>";
echo "<div class='col-sm-2'></div>";
echo "<div class='col-sm-10'>";
echo "<input class='btn btn-primary' type='submit' value='Add'>";
echo "</div>";
echo "</div>";

echo "</form>";
echo "</div>";
echo "</div>";
// end add assignment



echo "<div class='panel panel-default'>";
echo "<table class='table table-bordered panel panel-body'>";
echo "<tr>";
echo "<td width='20%' class='active'>Task Number</td>";
echo "<td width='30%'>{$task['task_id']}</td>";
echo "<td width='20%' class='active'>Created By</td>";
echo "<td width='30%'>" . format_emp($task) . "</td>";
echo "</tr>";
echo "<tr>";
echo "<td class='active'>Project</td>";
echo "<td>{$task['project_name']}</td>";
echo "<td class='active'>Created Date</td>";
echo "<td>" . date($APP_DATETIME_FORMAT, strtotime($task['created_date'])) . "</td>";
echo "</tr>";

echo "<tr>";
echo "<td class='active'>Priority</td>";
echo "<td>{$TASK_PRIORITY[$task['priority']]}</td>";
echo "<td class='active'>Status</td>";
echo "<td>{$TASK_STATUS[$task['task_status']]}</td>";
echo "</tr>";

echo "<tr>";
echo "<td class='active'>Weight</td>";
echo "<td>" . ((double) $task['weight']) . " mandays</td>";
echo "<td class='active'>Progress</td>";
echo "<td>";

echo "<div class='progress' style='margin: 0px;'>";
echo "<div class='progress-bar progress-bar-info progress-bar-striped' role='progressbar' aria-valuenow='{$task['progress']}' aria-valuemin='0' aria-valuemax='100' style='width: {$task['progress']}%;'>";
echo "{$task['progress']}%";
echo "</div>";
echo "</div>";
        
echo "</td>";
echo "</tr>";

echo "<tr>";
echo "<td class='active'>Start Date</td>";
echo "<td>" . npl_format_date($task['start_date']) . "</td>";
echo "<td class='active'>Last Updated</td>";
echo "<td>" . date($APP_DATETIME_FORMAT, strtotime($task['update_date'])) . "</td>";
echo "</tr>";

echo "<tr>";
echo "<td class='active'>End Date</td>";
echo "<td>" . npl_format_date($task['end_date']) . "</td>";
echo "<td class='active'>Closed Date</td>";
echo "<td>" . npl_format_date($task['closing_date']) . "</td>";
echo "</tr>";

echo "<tr>";
echo "<td class='active'>Title</td>";
echo "<td colspan='3'>" . htmlentities($task['task_title']) . "</td>";
echo "</tr>";

echo "<tr>";
echo "<td class='active'>Attachment</td>";
echo "<td colspan='3'>";
$fu = new FileUpload($APP_CONNECTION);
echo $fu->html_file_list('TSK', $task['task_id'], $is_owner && !in_array($task['task_status'], array('X', 'L')), TRUE);
echo "</td>";
echo "</tr>";

echo "<tr>";
echo "<td class='active'>Assigned To</td>";
echo "<td colspan='3'>{$personnel}</td>";
echo "</tr>";

echo "<tr>";
echo "<td class='active'>Description</td>";
echo "<td colspan='3'>" . nl2br(htmlentities($task['task_description'])) . "</td>";
echo "</tr>";

echo "</table>";
echo "</div>";

// start add comment
$rsx = mysql_query(
        "SELECT task_comment.*, emp_name, email "
        . "FROM task_comment "
        . "JOIN emp USING (emp_id) "
        . "WHERE task_id = '{$task['task_id']}' "
        . "ORDER BY task_comment.update_date", $APP_CONNECTION);
$comment_count = mysql_numrows($rsx);
if ($comment_count > 0 || in_array($task['task_status'], array('C', 'O', 'F'))) {
    echo "<div id='divComments' class='panel panel-default'>";
    echo "<div class='panel-heading'><h4 class='panel-title'>Comments <span class='badge'>{$comment_count}</span></h4></div>";
    echo "<div class='panel-body'>";
    
    while ($dtx = mysql_fetch_array($rsx, MYSQL_ASSOC)) {
        echo "<div class='row'>";
        echo "<div class='col-sm-2 text-right'>";
        echo "<strong>{$dtx['emp_name']}</strong><br>";
        echo "{$dtx['email']}<br>";
        echo date($APP_DATETIME_FORMAT, strtotime($dtx['update_date']));
        if ($dtx['emp_id'] == user() && in_array($task['task_status'], array('C', 'O', 'F'))) {
            echo "<br><a onclick=\"if(confirm('Delete comment?')){window.location = 'action/task.view.php?mode=delete-comment&id={$dtx['task_comment_id']}';}\" style='cursor:pointer;'><span class='glyphicon glyphicon-remove-circle'></span> Delete</a>";
        }
        echo "</div>";
        echo "<div class='col-sm-10 text-justify'>";
        echo nl2br(htmlentities($dtx['comment']));
        echo "</div>";
        echo "</div>";
        echo "<hr>";
    }
    
    if (in_array($task['task_status'], array('C', 'O', 'F'))) {
        echo "<form class='form-horizontal' role='form' action='action/task.view.php' method='post'>";
        echo "<input type='hidden' name='mode' value='comment'>";
        echo "<input type='hidden' name='task_id' value='{$task['task_id']}'>";
        echo "<div class='form-group'>";
        echo "<label for='comment' class='col-sm-2 control-label'>Add Comment</label>";
        echo "<div class='col-sm-10'><textarea id='comment' name='comment' rows='4' class='form-control'></textarea></div>";
        echo "</div>";
        echo "<div class='form-group'>";
        echo "<div class='col-sm-2'></div>";
        echo "<div class='col-sm-10'>";
        echo "<input class='btn btn-primary' type='submit' value='Submit'>";
        echo "</div>";
        echo "</div>";
    }

    echo "</form>";
    echo "</div>";
    echo "</div>";
}
mysql_free_result($rsx);
// end add comment

?>
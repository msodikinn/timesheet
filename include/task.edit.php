<?php

/*
 * task
 * Azwari Nugraha <nugraha@pt-gai.com>
 * Oct 9, 2014 9:20:59 PM
 */

if (!authenticated()) return;

$task = npl_fetch_table(
        "SELECT task.*, project_name, emp_name, email "
        . "FROM task "
        . "JOIN emp USING (emp_id) "
        . "JOIN project USING (project_id) "
        . "WHERE task_id = '{$_REQUEST['id']}'", $APP_CONNECTION);
$is_owner = is_task_owner($task['task_id']);

if (!$is_owner) return;
if (!in_array($task['task_status'], array('C', 'O', 'F'))) return;


echo "<div class='page-header'><h3>Edit Task</h3></div>";

echo "<div class='panel panel-default'>\n";
echo "<div class='panel-body'>\n";
echo "<form class='form-horizontal' role='form' action='action/task.edit.php' method='post'>\n";
echo "<input type='hidden' name='task_id' value='{$task['task_id']}'>";

echo "        <div class='form-group'>\n";
echo "        <label class='col-sm-2 control-label' for='task_id'>Task Number</label>\n";
echo "        <div class='col-sm-10'>\n";
echo "        <input class='form-control' id='task_id' name='task_id' type='text' value=\"{$task['task_id']}\" maxlength='10' style='text-align: center; width: 10em;'  disabled />\n";
echo "        </div>\n";
echo "        </div>\n";

echo "        <div class='form-group'>\n";
echo "        <label class='col-sm-2 control-label' for='project_id'><span style='color: red;'>*</span> Project</label>\n";
echo "        <div class='col-sm-10'>\n";
echo cgx_form_select('project_id', "SELECT project_id, project_name FROM project WHERE active = 'Y' ORDER BY project_name", $task['project_id'], FALSE, "id='project_id' required");
echo "        </div>\n";
echo "        </div>\n";

echo "        <div class='form-group'>\n";
echo "        <label class='col-sm-2 control-label' for='task_title'><span style='color: red;'>*</span> Title</label>\n";
echo "        <div class='col-sm-10'>\n";
echo "        <input value=\"{$task['task_title']}\" class='form-control' id='task_title' name='task_title' type='text' maxlength='50' />\n";
echo "        </div>\n";
echo "        </div>\n";

echo "        <div class='form-group'>\n";
echo "        <label class='col-sm-2 control-label' for='task_desc'>Description</label>\n";
echo "        <div class='col-sm-10'>\n";
echo "        <textarea class='form-control' id='task_desc' name='task_desc' rows='4' style='text-align: left;' >{$task['task_description']}</textarea>\n";
echo "        </div>\n";
echo "        </div>\n";

echo "        <div class='form-group'>\n";
echo "        <label class='col-sm-2 control-label' for='priority'><span style='color: red;'>*</span> Priority</label>\n";
echo "        <div class='col-sm-4'>\n";
echo cgx_form_select('priority', $TASK_PRIORITY, $task['priority'], FALSE, "id='priority' required");
echo "        </div>\n";
echo "        <label class='col-sm-2 control-label' for='start_date'>Start Date</label>\n";
echo "        <div class='col-sm-4'>\n";
echo "        <input value=\"" . (npl_emptydate($task['start_date']) ? '' : date('d-m-Y', strtotime($task['start_date']))) . "\" class='form-control text-center tanggal' id='start_date' name='start_date' type='text' maxlength='10' style='text-align: left;width: 10em;'  />\n";
echo "        </div>\n";
echo "        </div>\n";

echo "        <div class='form-group'>\n";
echo "        <label class='col-sm-2 control-label' for='weight'><span style='color: red;'>*</span> Weight</label>";
echo "        <div class='col-sm-4'>\n";
echo "        <div class='input-group' style='width: 12em;'>\n";
echo "        <input value=\"{$task['weight']}\" class='form-control text-center' id='weight' name='weight' type='text' maxlength='5' />";
echo "        <div class='input-group-addon'>mandays</div>";
echo "        </div>";
echo "        </div>\n";
echo "        <label class='col-sm-2 control-label' for='end_date'>End Date</label>\n";
echo "        <div class='col-sm-4'>\n";
echo "        <input value=\"" . (npl_emptydate($task['end_date']) ? '' : date('d-m-Y', strtotime($task['end_date']))) . "\" class='form-control text-center tanggal' id='end_date' name='end_date' type='text' maxlength='10' style='text-align: left;width: 10em;'  />\n";
echo "        </div>\n";
echo "        </div>\n";

echo "        <div class='form-group'>\n";
echo "        <div class='col-sm-offset-2 col-sm-10'>\n";
echo "        <input class='btn btn-primary' type='submit' value='Submit'>\n";
echo "        <input class='btn btn-warning' type='button' value='Back' onclick=\"window.location = 'index.php?m=task&id={$task['task_id']}';\">\n";
echo "        </div>\n";
echo "        </div>\n";
echo "</form>\n";
echo "</div>\n";
echo "</div>\n";


?>
<script type='text/javascript'>
<!--
$(function() {
    $(".tanggal").datepicker({dateFormat: 'dd-mm-yy'});
});
//-->
</script>

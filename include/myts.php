<?php

/**
 * (c) Azwari Nugraha <nugraha@duabelas.org>
 * 03/03/2014 00:19:24
 */
error_reporting(E_ERROR | E_WARNING | E_PARSE);
if (!authenticated()) return;

echo "<div class='page-header'><h3>My Timesheet</h3></div>";

function show_task($data) {
    return nl2br($data['record']['tasks']);
}

function cgx_format_ts_type($data) {
    $arr = $TS_TYPE;
    return $arr[$data['record'][$data['fieldName']]];
}

function cgx_edit($data) {
    $href = "index.php?&m={$_REQUEST['m']}&pkey[ts_id]={$data['record']['ts_id']}";
    $out = "<a href='{$href}'><img title='Edit this row' src='images/icon_edit.png' border='0'></a>";
    return $out;
}

function cgx_delete($data) {
    $href  = "javascript:if(confirm('Delete this row?')){window.location='action/myts.php";
    $href .= "?backvar=index.php%253F%2526m%253Dmyts&mode=delete&pkey[ts_id]={$data['record']['ts_id']}';}";
    $out = "<a href=\"{$href}\"><img title='Delete this row' src='images/icon_delete.png' border='0'></a>";
    return $out;
}

if (strlen($_REQUEST['pkey']['ts_id']) > 0) {
    $cgx_id = $_REQUEST['id'];
    $cgx_data = cgx_fetch_table("SELECT ts.*, project_name FROM ts JOIN project USING (project_id) WHERE ts.ts_id = '" . mysql_escape_string($_REQUEST['pkey']['ts_id']) . "'");

    $cgx_data['ts_date'] = cgx_emptydate($cgx_data['ts_date']) ? '' : cgx_ddmmyyyy($cgx_data['ts_date']);

    echo "<div class='panel panel-default'>\n";
    echo "<div class='panel-body'>\n";
    echo "<form class='form-horizontal' role='form' action='action/myts.php' method='post'>\n";
    echo "<input type='hidden' name='backvar' value='" . urlencode("index.php?&m={$_REQUEST['m']}") . "'>\n";
    echo "<input type='hidden' name='mode' value='" . ($_REQUEST['pkey']['ts_id'] == '0' ? 'new' : 'update') . "'>\n";
    echo "<input type='hidden' name='pkey[ts_id]' value=\"{$_REQUEST['pkey']['ts_id']}\">\n";
    echo "<input type='hidden' name='table' value='ts'>\n";

    if ($_SESSION[$GLOBALS['APP_ID']]['myts']['error']) {
        echo "<div class='alert alert-danger'>{$_SESSION[$GLOBALS['APP_ID']]['myts']['error']}</div>";
        unset($_SESSION[$GLOBALS['APP_ID']]['myts']['error']);
    }

    if ($_SESSION[$GLOBALS['APP_ID']]['myts']['info']) {
        echo "<div class='alert alert-success'>{$_SESSION[$GLOBALS['APP_ID']]['myts']['info']}</div>";
        unset($_SESSION[$GLOBALS['APP_ID']]['myts']['info']);
    }
    
    $cgx_data['ts_start'] = substr($cgx_data['ts_start'], 0, 5);
    $cgx_data['ts_end'] = substr($cgx_data['ts_end'], 0, 5);
    
    if (empty($cgx_data['project_id'])) $cgx_data['project_id'] = user('default_project_id');

    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_ts_id'>ID</label>\n";
    echo "        <div class='col-sm-4'>\n";
    echo "        <input class='form-control' id='data_ts_id' name='data[ts_id]' type='text' value=\"{$cgx_data['ts_id']}\" maxlength='8' style='text-align: right;width: 9em;'  disabled />\n";
    echo "        </div>\n";
    echo "        <label class='col-sm-2 control-label' for='data_ts_type'>Timesheet</label>\n";
    echo "        <div class='col-sm-4'>\n";
    echo cgx_form_select('data[ts_type]', $TS_TYPE, $cgx_data['ts_type'], FALSE, "id='data_ts_type'");
    echo "        </div>\n";
    echo "        </div>\n";
    
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_ts_date'>Date</label>\n";
    echo "        <div class='col-sm-4'>\n";
    echo "        <input required class='form-control' id='data_ts_date' name='data[ts_date]' type='text' value=\"{$cgx_data['ts_date']}\" maxlength='10' style='text-align: left;width: 10em;'  />\n";
    echo "        </div>\n";
    //echo "        <label class='col-sm-2 control-label' for='data_project_id'>Project</label>\n";
    //echo "        <div class='col-sm-4'>\n";
    //echo cgx_form_select('data[project_id]', "SELECT project_id, project_name FROM project WHERE active = 'Y' ORDER BY project_name", $cgx_data['project_id'], FALSE, "id='data_project_id' required");
    //echo "        </div>\n";
    //echo "        </div>\n";
    echo "        <label class='col-sm-2 control-label' for='data_project_name'>Project Name</label>\n";
    echo "        <div class='col-sm-4'><div class='input-group'>\n";
    echo "        <input  type='hidden' id='project_id' name='data[project_id]' value=\"{$cgx_data['project_id']}\">\n";    
    echo "        <input  class='form-control' type='text' id='data_project_name'  value=\"{$cgx_data['project_name']}\" size='50' style='z-index: 0;' style='text-align: left;width: 30em;' readonly='true'/>\n";
    echo "        <div class='input-group-addon'><span onclick=\"selectProject();\" style='cursor: pointer;' class='glyphicon glyphicon-list'></span></div>\n"; 
    echo "        </div>\n";
    echo "        </div>\n\n";
    echo "        <br>";
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_ts_start'>Start Time</label>\n";
    echo "        <div class='col-sm-4'>\n";
    echo "        <input required class='form-control' id='data_ts_start' name='data[ts_start]' type='text' value=\"{$cgx_data['ts_start']}\" maxlength='5' style='text-align: left;width: 6em;'  />\n";
    echo "        </div>\n";
    echo "        <label class='col-sm-2 control-label' for='data_location'>Working Location</label>\n";
    echo "        <div class='col-sm-4'>\n";
    echo "        <input required class='form-control' id='data_location' name='data[location]' type='text' value=\"{$cgx_data['location']}\" maxlength='30' />\n";
    echo "        </div>\n";
    echo "        </div>\n";
    
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_ts_end'>End Time</label>\n";
    echo "        <div class='col-sm-4'>\n";
    echo "        <input class='form-control' id='data_ts_end' name='data[ts_end]' type='text' value=\"{$cgx_data['ts_end']}\" maxlength='5' style='text-align: left;width: 6em;'  />\n";
    echo "        </div>\n";
    echo "        <label class='col-sm-2 control-label' for='data_contact_person'>Contact Person</label>\n";
    echo "        <div class='col-sm-4'>\n";
    echo "        <input class='form-control' id='data_contact_person' name='data[contact_person]' type='text' value=\"{$cgx_data['contact_person']}\" maxlength='20'  />\n";
    echo "        </div>\n";
    echo "        </div>\n";
    
    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label' for='data_tasks'>Tasks</label>\n";
    echo "        <div class='col-sm-10'>\n";
    echo "        <textarea required class='form-control' id='data_tasks' name='data[tasks]' rows='8' style='text-align: left;' >{$cgx_data['tasks']}</textarea>\n";
    echo "        </div>\n";
    echo "        </div>\n";

    echo "        <div class='form-group'>\n";
    echo "        <label class='col-sm-2 control-label'>Activity / Output</label>\n";
    echo "        <div class='col-sm-10'>\n";
    $rsx = mysql_query("SELECT activity.*, ts_id FROM activity LEFT JOIN (SELECT * FROM ts_activity WHERE ts_id = '{$cgx_data['ts_id']}') x USING (activity_id) WHERE active = 'Y' ORDER BY activity_name", $APP_CONNECTION);
    while ($dtx = mysql_fetch_array($rsx, MYSQL_ASSOC)) {
        if ($dtx['ts_id'] > 0) {
            echo "        <label class='checkbox-inline'><input checked name='data[activity][{$dtx['activity_id']}]' type='checkbox'>{$dtx['activity_name']}</label>";
        } else {
            echo "        <label class='checkbox-inline'><input name='data[activity][{$dtx['activity_id']}]' type='checkbox'>{$dtx['activity_name']}</label>";
        }
    }
    mysql_free_result($rsx);
    echo "        </div>\n";
    echo "        </div>\n";
    
    echo "        <div class='form-group'>\n";
    echo "        <div class='col-sm-offset-2 col-sm-10'>\n";
    echo "        <input class='btn btn-primary' type='submit' value='Submit'>\n";
    echo "        <input class='btn btn-warning' type='button' value='Back' onclick=\"window.location = 'index.php?&m=myts';\">\n";
    echo "        </div>\n";
    echo "        </div>\n";
    echo "</form>\n";
    echo "</div>\n";
    echo "</div>\n";

?>
<script type='text/javascript'>
<!--
$(function() {
    $("#data_ts_date").datepicker({dateFormat: 'dd-mm-yy'});
    $("#data_ts_start").timepicker();
    $("#data_ts_end").timepicker();
});
//-->
</script>
<?php
} else {
    require_once 'Structures/DataGrid.php';
    require_once 'HTML/Table.php';

    echo "<div class='panel panel-default'>";
    if (is_array($_SESSION[$GLOBALS['APP_ID']]['myts']['columns'])) {
        $cgx_def_columns = $_SESSION[$GLOBALS['APP_ID']]['myts']['columns'];
    } else {
        $cgx_def_columns = array(
            'ts_id' => 1,
            'ts_date' => 1,
            'project_name' => 1,
            'tasks' => 1,
            'location' => 1,
            'contact_person' => 1,
        );
        $_SESSION[$GLOBALS['APP_ID']]['myts']['columns'] = $cgx_def_columns;
    }

    $cgx_sql = "SELECT ts.*, project_name "
            . "FROM ts "
            . "JOIN project USING (project_id) "
            . "WHERE emp_id = '" . user('emp_id') . "'";
    $cgx_datagrid = new Structures_DataGrid($cgx_max_rows);
    $cgx_datagrid->setDefaultSort(array('ts_date' => 'ASC'));
    $cgx_options = array('dsn' => $cgx_dsn);

    $cgx_filter1 = urldecode($_REQUEST['f1']);
    $cgx_search = $_REQUEST['q'];

    $cgx_filter1 = empty($cgx_filter1) ? date('Y-m') : $cgx_filter1;
    
    if (empty($cgx_filter1)) {
        $cgx_filter1 = npl_fetch_table("SELECT MAX(DATE_FORMAT(ts_date, '%Y-%d')) P FROM ts WHERE emp_id = '" . user('emp_id') . "'");
        $cgx_filter1 = $cgx_filter1['P'];
    }

    echo "<div class='panel-heading'>";
    echo "<form name='frmFILTER' action='{$_SERVER['SCRIPT_NAME']}'>\n";
    echo "<input type='hidden' name='m' value='{$_REQUEST['m']}'>\n";
    echo "<table id='bar' class='datagrid_bar' width='100%'><tr>\n";
    echo "<td>\n";
    echo "<table align='left' cellspacing='0' cellpadding='0' border='0' width='100%'><tr>\n";
    echo "<td><input type='button' class='btn btn-primary' value='Input Timesheet' onclick=\"window.location = 'index.php?&m={$_REQUEST['m']}&pkey[ts_id]=0';\"></td>\n";
    echo "<td>&nbsp;</td>\n";
    echo "<td>Period " . cgx_filter('f1', "SELECT DISTINCT DATE_FORMAT(ts_date, '%Y-%m') period_id, DATE_FORMAT(ts_date, '%M %Y') period_name FROM ts", $cgx_filter1, FALSE) . "</td>\n";
    echo "<td width='20'></td>\n";
    echo "</tr></table>\n";
    echo "</td>\n";
    echo "<td align='right'><input type='text' size='20' name='q' value=\"{$cgx_search}\"></td>\n";
    echo "<td width='1'><input title='Search' type='image' src='images/icon_search.png' border='0' style='padding-right: 20px;'></td>\n";
    echo "<td></td>\n";
    echo "<td width='20'></td>\n";
    if (authenticated()) {
        echo "<td width='1' class='datagrid_bar_icon'><a title='New record' href='index.php?&m={$_REQUEST['m']}&pkey[ts_id]=0'><img border='0' src='images/icon_add.png'></a></td>\n";
    } else {
        echo "<td width='1' class='datagrid_bar_icon'><img border='0' src='images/icon_add_dis.png'></td>\n";
    }
    echo "<td width='1' class='datagrid_bar_icon'><a title='Export all (CSV)' href='action/myts.php?mode=export-all'><img border='0' src='images/icon_csv.png'></a></td>\n";
    echo "<td width='1' class='datagrid_bar_icon'><a title='Customize columns' href='javascript:customizeColumn(true);'><img border='0' src='images/icon_columns.png'></a></td>\n";
    echo "</tr></table>\n";
    echo "</form>\n";
    echo "</div>";

    echo "<div id='columns' class='panel-body' style='display: none;'>\n";
    echo "<form name='frmCUSTOMIZE' action='action/cgx_customize.php' method='post'>\n";
    echo "<input type='hidden' name='back' value='" . urlencode($_SERVER['REQUEST_URI']) . "'>\n";
    echo "<input type='hidden' name='dg_name' value='myts'>\n";
    echo "<input type='hidden' name='col[ts_id]' value='on'>\n";
    echo "<div class='form-group'>";
    echo "<label class='checkbox-inline'><input disabled checked type='checkbox' name='col[ts_id]'> ID</label>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['ts_date'] == 1 ? ' checked' : '') . " type='checkbox' name='col[ts_date]'> Date</label>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['project_name'] == 1 ? ' checked' : '') . " type='checkbox' name='col[project_name]'> Project</label>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['ts_type'] == 1 ? ' checked' : '') . " type='checkbox' name='col[ts_type]'> Timesheet</label>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['ts_start'] == 1 ? ' checked' : '') . " type='checkbox' name='col[ts_start]'> Start Time</label>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['ts_end'] == 1 ? ' checked' : '') . " type='checkbox' name='col[ts_end]'> End Time</label>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['tasks'] == 1 ? ' checked' : '') . " type='checkbox' name='col[tasks]'> Tasks</label>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['location'] == 1 ? ' checked' : '') . " type='checkbox' name='col[location]'>Working Location</label>";
    echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['contact_person'] == 1 ? ' checked' : '') . " type='checkbox' name='col[contact_person]'> Contact Person</label>";
    echo "</div>";
    echo "<input class='btn btn-primary' type='submit' value='Update'>\n";
    echo "<input class='btn btn-warning' type='button' value='Cancel' onclick='customizeColumn(false);'>\n";
    echo "</form>\n";
    echo "</div>\n";
?>
<script type="text/javascript">
<!--
function customizeColumn(s) {
    var divCols = document.getElementById('columns');
    var divBar = document.getElementById('bar');
    if (s) {
        divCols.style.display = 'block';
        divBar.style.display = 'none';
    } else {
        window.location = window.location;
    }
}
//-->
</script>
<?php

    if (strlen($cgx_filter1) > 0) $cgx_sql .= " AND DATE_FORMAT(ts_date, '%Y-%m') = '" . mysql_escape_string($cgx_filter1) . "'";
    $cgx_sql .= " and ( ts.tasks LIKE '%{$cgx_search}%' OR ts.location LIKE '%{$cgx_search}%' OR ts.contact_person LIKE '%{$cgx_search}%')";
    if ($_SESSION[$GLOBALS['APP_ID']]['myts']['error']) {
        echo "<div class='alert alert-danger'>{$_SESSION[$GLOBALS['APP_ID']]['myts']['error']}</div>";
        unset($_SESSION[$GLOBALS['APP_ID']]['myts']['error']);
    }

    if ($_SESSION[$GLOBALS['APP_ID']]['myts']['info']) {
        echo "<div class='alert alert-success'>{$_SESSION[$GLOBALS['APP_ID']]['myts']['info']}</div>";
        unset($_SESSION[$GLOBALS['APP_ID']]['myts']['info']);
    }


    $cgx_test = $cgx_datagrid->bind($cgx_sql, $cgx_options);
    if (PEAR::isError($cgx_test)) {
        echo $cgx_test->getMessage();
    }

    if ($cgx_def_columns['ts_id'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('ID', 'ts_id', 'ts_id', array('align' => 'right'), NULL, NULL));
    if ($cgx_def_columns['ts_date'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Date', 'ts_date', 'ts_date', array('align' => 'left'), NULL, "cgx_format_date()"));
    if ($cgx_def_columns['project_name'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Project', 'project_name', 'project_name', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['ts_type'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Timesheet', 'ts_type', 'ts_type', array('align' => 'left'), NULL, "cgx_format_ts_type()"));
    if ($cgx_def_columns['ts_start'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Start Time', 'ts_start', 'ts_start', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['ts_end'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('End Time', 'ts_end', 'ts_end', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['tasks'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Tasks', 'tasks', 'tasks', array('align' => 'left'), NULL, "show_task()"));
    if ($cgx_def_columns['location'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Working Location', 'location', 'location', array('align' => 'left'), NULL, NULL));
    if ($cgx_def_columns['contact_person'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Contact Person', 'contact_person', 'contact_person', array('align' => 'left'), NULL, NULL));
    if (authenticated()) $cgx_datagrid->addColumn(new Structures_DataGrid_Column(NULL, NULL, NULL, array('align' => 'center', 'width' => '1'), NULL, 'cgx_edit()'));
    if (authenticated()) $cgx_datagrid->addColumn(new Structures_DataGrid_Column(NULL, NULL, NULL, array('align' => 'center', 'width' => '1'), NULL, 'cgx_delete()'));

    $cgx_table = new HTML_Table($cgx_TableAttribs);
    $cgx_tableHeader = & $cgx_table->getHeader();
    $cgx_tableBody = & $cgx_table->getBody();

    $cgx_test = $cgx_datagrid->fill($cgx_table, $cgx_RendererOptions);
    if (PEAR::isError($cgx_test)) {
        echo $cgx_test->getMessage();
    }

    $cgx_tableHeader->setRowAttributes(0, $cgx_HeaderAttribs);
    $cgx_tableBody->altRowAttributes(0, $cgx_EvenRowAttribs, $cgx_OddRowAttribs, TRUE);

    echo $cgx_table->toHtml();

    echo "<table width='100%'><tr>\n";
    echo "<td class='datagrid_pager'>Found " . number_format($cgx_datagrid->getRecordCount()) . " record(s)</td>\n";
    echo "<td align='right' class='datagrid_pager'>\n";
    $cgx_test = $cgx_datagrid->render(DATAGRID_RENDER_PAGER);
    if (PEAR::isError($cgx_test)) {
        echo $cgx_test->getMessage();
    }
    echo "</td></tr></table>\n";
    echo "</div>\n";
}

?>
<script type="text/javascript">
$(document).ready(function(){
    $("ts_date").datepicker({dateFormat: 'dd-mm-yy'});
});    
        function selectProject() {
            var w = 800;
            var h = 500;
            var l = (screen.width - w) / 2;
            var t = (screen.height - h) / 2;
            oWindow = window.open('reference.php?s=t.project', 'winRef',
                'directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,width=' + w + ',height=' + h + ',left=' + l + ',top=' + t);
            oWindow.focus();
        }
        
        function setProject(id, nama) {
            var txt_bp = document.getElementById('data_project_name');
            var hid_bp = document.getElementById('project_id');
            txt_bp.value = nama;
            hid_bp.value = id;
        }
</script>
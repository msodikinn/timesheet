<?php

/**
 * (c) Azwari Nugraha <nugraha@duabelas.org>
 * 03/03/2014 14:03:24
 */
error_reporting(E_ERROR | E_WARNING | E_PARSE);
if (!authenticated()) return;
$project = npl_fetch_table("SELECT * FROM project WHERE project_id = '{$_REQUEST['id']}'");

if (empty($_REQUEST['f1'])) {
    $_REQUEST['f1'] = npl_fetch_table(
            "SELECT DATE_FORMAT(MAX(ts_date), '%Y-%m') f1 "
            . "FROM ts "
            . "WHERE project_id = '{$_REQUEST['id']}'");
    $_REQUEST['f1'] = $_REQUEST['f1']['f1'];
}

function show_task($data) {
    return nl2br($data['record']['tasks']);
}

echo "<div class='page-header'><h3>{$project['project_name']}</h3></div>";

require_once 'Structures/DataGrid.php';
require_once 'HTML/Table.php';

echo "<div class='panel panel-default'>";
if (is_array($_SESSION[$GLOBALS['APP_ID']]['project-ts']['columns'])) {
    $cgx_def_columns = $_SESSION[$GLOBALS['APP_ID']]['project-ts']['columns'];
} else {
    $cgx_def_columns = array(
        'ts_id' => 1,
        'ts_date' => 1,
        'emp_name' => 1,
        'tasks' => 1,
        'location' => 1,
    );
    $_SESSION[$GLOBALS['APP_ID']]['project-ts']['columns'] = $cgx_def_columns;
}

$cgx_sql = "SELECT ts.*, project_name, emp_name "
        . "FROM ts "
        . "JOIN project USING (project_id) "
        . "JOIN emp USING (emp_id) "
        . "WHERE project_id = '{$project['project_id']}' AND ts_type = 'W'";
$cgx_datagrid = new Structures_DataGrid($cgx_max_rows);
$cgx_options = array('dsn' => $cgx_dsn);

$cgx_filter1 = urldecode($_REQUEST['f1']);
$cgx_search = $_REQUEST['q'];

echo "<div class='panel-heading'>";
echo "<form name='frmFILTER' action='{$_SERVER['SCRIPT_NAME']}'>\n";
echo "<input type='hidden' name='m' value='{$_REQUEST['m']}'>\n";
echo "<input type='hidden' name='id' value='{$_REQUEST['id']}'>\n";
echo "<table id='bar' class='datagrid_bar' width='100%'><tr>\n";
echo "<td>\n";
echo "<table align='left' cellspacing='0' cellpadding='0' border='0'><tr>\n";
echo "<td>Period " . cgx_filter('f1', "SELECT DISTINCT DATE_FORMAT(ts_date, '%Y-%m') period_id, DATE_FORMAT(ts_date, '%M %Y') period_name FROM ts WHERE project_id = '{$_REQUEST['id']}'", $cgx_filter1, FALSE) . "</td>\n";
echo "<td width='20'></td>\n";
echo "</tr></table>\n";
echo "</td>\n";
echo "<td align='right'><input type='text' size='20' name='q' value=\"{$cgx_search}\"></td>\n";
echo "<td width='1'><input title='Search' type='image' src='images/icon_search.png' border='0' style='padding-right: 20px;'></td>\n";
echo "<td></td>\n";
echo "<td width='20'></td>\n";
echo "<td width='1' class='datagrid_bar_icon'><a title='Export all (CSV)' href='action/project-ts.php?mode=export-all&id={$_REQUEST['id']}'><img border='0' src='images/icon_csv.png'></a></td>\n";
echo "<td width='1' class='datagrid_bar_icon'><a title='Customize columns' href='javascript:customizeColumn(true);'><img border='0' src='images/icon_columns.png'></a></td>\n";
echo "</tr></table>\n";
echo "</form>\n";
echo "</div>";

echo "<div id='columns' class='panel-body' style='display: none;'>\n";
echo "<form name='frmCUSTOMIZE' action='action/cgx_customize.php' method='post'>\n";
echo "<input type='hidden' name='back' value='" . urlencode($_SERVER['REQUEST_URI']) . "'>\n";
echo "<input type='hidden' name='dg_name' value='project-ts'>\n";
echo "<input type='hidden' name='col[ts_date]' value='on'>\n";
echo "<input type='hidden' name='col[ts_id]' value='on'>\n";
echo "<div class='form-group'>";
echo "<label class='checkbox-inline'><input disabled checked type='checkbox' name='col[ts_id]'> Task ID</label>";
echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['emp_name'] == 1 ? ' checked' : '') . " type='checkbox' name='col[emp_name]'> Employee Name</label>";
echo "<label class='checkbox-inline'><input disabled checked type='checkbox' name='col[ts_date]'> Date</label>";
echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['ts_start'] == 1 ? ' checked' : '') . " type='checkbox' name='col[ts_start]'> Start Time</label>";
echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['ts_end'] == 1 ? ' checked' : '') . " type='checkbox' name='col[ts_end]'> End Time</label>";
echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['tasks'] == 1 ? ' checked' : '') . " type='checkbox' name='col[tasks]'> Tasks</label>";
echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['location'] == 1 ? ' checked' : '') . " type='checkbox' name='col[location]'> Working Location</label>";
echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['contact_person'] == 1 ? ' checked' : '') . " type='checkbox' name='col[contact_person]'> Contact Person</label>";
echo "<label class='checkbox-inline'><input" . ($cgx_def_columns['last_updated'] == 1 ? ' checked' : '') . " type='checkbox' name='col[last_updated]'> Last Updated</label>";
echo "</div>";
echo "<input class='btn btn-primary' type='submit' value='Update'>\n";
echo "<input class='btn btn-warning' type='button' value='Cancel' onclick='customizeColumn(false);'>\n";
echo "</form>\n";
echo "</div>\n";
?>
<script type="text/javascript">
<!--
function customizeColumn(s) {
    var divCols = document.getElementById('columns');
    var divBar = document.getElementById('bar');
    if (s) {
        divCols.style.display = 'block';
        divBar.style.display = 'none';
    } else {
        window.location = window.location;
    }
}
//-->
</script>
<?php

if (strlen($cgx_filter1) > 0) $cgx_sql .= " AND DATE_FORMAT(ts_date, '%Y-%m') = '" . mysql_escape_string($cgx_filter1) . "'";
$cgx_sql .= " and ( ts.tasks LIKE '%{$cgx_search}%' OR ts.location LIKE '%{$cgx_search}%' OR ts.contact_person LIKE '%{$cgx_search}%')";
if ($_SESSION[$GLOBALS['APP_ID']]['project-ts']['error']) {
    echo "<div class='alert alert-danger'>{$_SESSION[$GLOBALS['APP_ID']]['project-ts']['error']}</div>";
    unset($_SESSION[$GLOBALS['APP_ID']]['project-ts']['error']);
}

if ($_SESSION[$GLOBALS['APP_ID']]['project-ts']['info']) {
    echo "<div class='alert alert-success'>{$_SESSION[$GLOBALS['APP_ID']]['project-ts']['info']}</div>";
    unset($_SESSION[$GLOBALS['APP_ID']]['project-ts']['info']);
}


$cgx_test = $cgx_datagrid->bind($cgx_sql, $cgx_options);
if (PEAR::isError($cgx_test)) {
    echo $cgx_test->getMessage();
}

if ($cgx_def_columns['ts_id'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('ID', 'ts_id', 'ts_id', array('align' => 'right'), NULL, NULL));
if ($cgx_def_columns['emp_name'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Employee', 'emp_name', 'emp_name', array('align' => 'left'), NULL, NULL));
if ($cgx_def_columns['ts_date'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Date', 'ts_date', 'ts_date', array('align' => 'center'), NULL, "cgx_format_date()"));
if ($cgx_def_columns['ts_start'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Start Time', 'ts_start', 'ts_start', array('align' => 'left'), NULL, "cgx_format_date()"));
if ($cgx_def_columns['ts_end'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('End Time', 'ts_end', 'ts_end', array('align' => 'left'), NULL, "cgx_format_date()"));
if ($cgx_def_columns['tasks'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Tasks', 'tasks', 'tasks', array('align' => 'left'), NULL, "show_task()"));
if ($cgx_def_columns['location'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Working Location', 'location', 'location', array('align' => 'left'), NULL, NULL));
if ($cgx_def_columns['contact_person'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Contact Person', 'contact_person', 'contact_person', array('align' => 'left'), NULL, NULL));
if ($cgx_def_columns['last_updated'] == 1) $cgx_datagrid->addColumn(new Structures_DataGrid_Column('Last Updated', 'last_updated', 'last_updated', array('align' => 'center'), NULL, "cgx_format_timestamp()"));

$cgx_table = new HTML_Table($cgx_TableAttribs);
$cgx_tableHeader = & $cgx_table->getHeader();
$cgx_tableBody = & $cgx_table->getBody();

$cgx_test = $cgx_datagrid->fill($cgx_table, $cgx_RendererOptions);
if (PEAR::isError($cgx_test)) {
    echo $cgx_test->getMessage();
}

$cgx_tableHeader->setRowAttributes(0, $cgx_HeaderAttribs);
$cgx_tableBody->altRowAttributes(0, $cgx_EvenRowAttribs, $cgx_OddRowAttribs, TRUE);

echo $cgx_table->toHtml();

echo "<table width='100%'><tr>\n";
echo "<td class='datagrid_pager'>Found " . number_format($cgx_datagrid->getRecordCount()) . " record(s)</td>\n";
echo "<td align='right' class='datagrid_pager'>\n";
$cgx_test = $cgx_datagrid->render(DATAGRID_RENDER_PAGER);
if (PEAR::isError($cgx_test)) {
    echo $cgx_test->getMessage();
}
echo "</td></tr></table>\n";
echo "</div>\n";

?>
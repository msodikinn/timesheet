<?php

require_once 'init.php';

if (!authenticated()) {
    header("Location: login.php");
    exit;
}

?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>GAI Timesheet</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/default.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="css/cgx.css">
    <link rel="stylesheet" href="css/redmond/jquery-ui-1.10.3.custom.css" />
    <link rel="stylesheet" href="css/jquery-ui-timepicker-addon.css" />
    
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-ui-1.10.3.custom.min.js"></script> 
    <script src="js/jquery-ui-timepicker-addon.js"></script> 
    <script src="js/bootstrap.file-input.js"></script>

  </head>

  <body>

    <!-- Static navbar -->
    <div class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0px;">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
            <a class="navbar-brand" href="./">GAI Timesheet</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php?m=task">Tasks</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Active Projects <b class="caret"></b></a>
              <ul class="dropdown-menu">
                  <?php
                  
                  $rsx = mysql_query("SELECT * FROM project WHERE active = 'Y' ORDER BY project_name", $APP_CONNECTION);
                  while ($dtx = mysql_fetch_array($rsx, MYSQL_ASSOC)) {
                      echo "<li><a href='index.php?m=project-ts&id={$dtx['project_id']}'>{$dtx['project_name']}</a></li>\n";
                  }
                  mysql_free_result($rsx);
                  
                  ?>
              </ul>
            </li>
            <li><a href="index.php?m=myts">My Timesheet</a></li>
            <?php if (is_admin()) { ?>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">References <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="index.php?m=emp">Employee</a></li>
                <li><a href="index.php?m=project">Project</a></li>
                <li><a href="index.php?m=activity">Activity</a></li>
              </ul>
            </li>
            <?php } ?>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Report <b class="caret"></b></a>
              <ul class="dropdown-menu">
            <?php if (has_privilege('project_manager')) { ?>
                <li><a href="index.php?m=att">Attendance Report</a></li>
                <li><a href="index.php?m=project-summary">Project Summary</a></li>
            <?php } ?>
                <li><a href="index.php?m=mts">Monthly Timesheet</a></li>
              </ul>
            </li>
            <?php if (is_admin()) { ?>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Administration <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="index.php?m=user">User Management</a></li>
              </ul>
            </li>
            </ul>
            <?php } ?>
          <ul class="nav navbar-nav navbar-right">
              <li><a href="./index.php?m=profile"><img style="margin: -10px 10px;" width="42" class="img-circle" src="<?php echo user_image(); ?>">[ <?php echo user('emp_id') . ' - ' . user('emp_name'); ?> ]</a></li>
            <li><a href="action/logout.php">Logout</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>


    <div class="container">

      <!-- Main component for a primary marketing message or call to action -->
<!--      <div class="jumbotron">
        <h1>Navbar example</h1>
        <p>This example is a quick exercise to illustrate how the default, static and fixed to top navbar work. It includes the responsive CSS and HTML, so it also adapts to your viewport and device.</p>
        <p>To see the difference between static and fixed top navbars, just scroll.</p>
        <p>
          <a class="btn btn-lg btn-primary" href="../../components/#navbar" role="button">View navbar docs &raquo;</a>
        </p>
      </div>-->


        <?php
        $_REQUEST['m'] = empty($_REQUEST['m']) ? 'dashboard' : $_REQUEST['m'];
        
        if (file_exists("include/{$_REQUEST['m']}.php")) {
            include("include/{$_REQUEST['m']}.php");
        } else {
            echo "File not found!";
        }
        ?>



    </div> <!-- /container -->

    <script src="js/bootstrap.min.js"></script>
  </body>
</html>

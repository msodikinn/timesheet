<?php

/*
 * default
 * Azwari Nugraha <nugraha@pt-gai.com>
 * Sep 7, 2014 10:57:05 PM
 */

function fuDelete($id_file, $key) {
    global $APP_ATTACHMENT, $APP_CONNECTION;

    $res = new xajaxResponse();
    $frec = npl_fetch_table("SELECT * FROM t_file WHERE id_t_file = '{$id_file}' AND file_key = '{$key}'");
    if (is_array($frec)) {
        $path = $APP_ATTACHMENT . '/' . $frec['id_m_file'] . '/' . $frec['id_ref'] . '/' . $id_file;
        if (file_exists($path)) unlink($path);
        mysql_query("DELETE FROM t_file WHERE id_t_file = '{$id_file}' AND file_key = '{$key}'", $APP_CONNECTION);
        $res->script("window.location = window.location;");
    } else {
        $res->alert("File tidak ditemukan!");
    }
    return $res;
}

$xajax->register(XAJAX_FUNCTION, "fuDelete");

?>
<?php

/*
 * task
 * Azwari Nugraha <nugraha@pt-gai.com>
 * Oct 9, 2014 10:04:58 PM
 */

require_once '../init.php';
if (!authenticated()) die;

$data = npl_fetch_table("SELECT * FROM task WHERE task_id = '{$_REQUEST['task_id']}'");
if ($data['emp_id'] == user()) {
    
    $sql = "UPDATE task SET "
            . "project_id = '{$_REQUEST['project_id']}', "
            . "task_title = '" . mysql_real_escape_string($_REQUEST['task_title'], $APP_CONNECTION) . "', "
            . "task_description = '" . mysql_real_escape_string($_REQUEST['task_desc'], $APP_CONNECTION) . "', "
            . "start_date = " . (npl_emptydate(npl_dmy2ymd($_REQUEST['start_date'])) ? 'NULL' : "'" . npl_dmy2ymd($_REQUEST['start_date']) .  "'") . ", "
            . "end_date = " . (npl_emptydate(npl_dmy2ymd($_REQUEST['end_date'])) ? 'NULL' : "'" . npl_dmy2ymd($_REQUEST['end_date']) .  "'") . ", "
            . "weight = '{$_REQUEST['weight']}', "
            . "priority = '{$_REQUEST['priority']}' "
            . "WHERE task_id = '{$_REQUEST['task_id']}'";
    if (mysql_query($sql, $APP_CONNECTION)) {
        header("Location: ../index.php?m=task&id={$_REQUEST['task_id']}");
        exit;
    } else {
        die(mysql_error($APP_CONNECTION));
    }
    
} else {
    die ('insufficient privilege');
}

?>
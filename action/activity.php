<?php

/**
 * (c) Azwari Nugraha <nugraha@duabelas.org>
 * 02/03/2014 23:41:08
 */

require_once '../init.php';

if (!authenticated()) {
    header("Location: ../index.php");
    exit;
}

if ($_REQUEST['mode'] == 'update' || $_REQUEST['mode'] == 'new') if (!has_privilege('admin')) die ('akses ditolak');
if ($_REQUEST['mode'] == 'delete') if (!has_privilege('admin')) die ('akses ditolak');

if ($_REQUEST['mode'] == 'export-all') {
    header("Content-Type: text/csv");
    header("Content-Disposition: attachment; filename=\"activity-" . date("Y-m-d") . ".csv\"");
    echo "\"Activity ID\",\"Activity Name\",\"Active\"\n";
    $cgx_rs_export = oci_parse($cgx_connection, "SELECT * FROM activity");
    oci_execute($cgx_rs_export);
    while (($cgx_dt_export = oci_fetch_array($cgx_rs_export, OCI_ASSOC)) !== FALSE) {
        echo "\"" . str_replace("\"", "\"\"", $cgx_dt_export['activity_id']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['activity_name']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['active']) . "\"";
        echo "\n";
    }
    oci_free_statement($cgx_rs_export);
    exit;
} elseif ($_REQUEST['mode'] == 'update') {
    $cgx_sql = "UPDATE activity SET";
    $cgx_sql .= " activity_name = '" . mysql_escape_string($_REQUEST['data']['activity_name']) . "'";
    $cgx_sql .= ", active = '" . mysql_escape_string($_REQUEST['data']['active']) . "'";
    $cgx_sql .= " WHERE";
    $cgx_sql .= " activity_id = '{$_REQUEST['pkey']['activity_id']}'";
} elseif ($_REQUEST['mode'] == 'new') {
    $cgx_sql = "INSERT INTO activity (";
    $cgx_sql .= "activity_id,activity_name,active";
    $cgx_sql .= ") values (";
    $cgx_sql .= "'" . mysql_escape_string($_REQUEST['data']['activity_id']) . "'";
    $cgx_sql .= ",'" . mysql_escape_string($_REQUEST['data']['activity_name']) . "'";
    $cgx_sql .= ",'" . mysql_escape_string($_REQUEST['data']['active']) . "'";
    $cgx_sql .= ")";
} elseif ($_REQUEST['mode'] == 'delete') {
    $cgx_sql = "DELETE FROM activity ";
    $cgx_sql .= " WHERE";
    $cgx_sql .= " activity_id = '{$_REQUEST['pkey']['activity_id']}'";
}

if (@mysql_query($cgx_sql, $cgx_connection)) {
    $_SESSION[$GLOBALS['APP_ID']]['activity']['error'] = FALSE;
    $_SESSION[$GLOBALS['APP_ID']]['activity']['info'] = 'Your data has been successfully updated';
    if ($_REQUEST['mode'] == 'new') $cgx_new_id = mysql_insert_id($cgx_connection);
} else {
    $_SESSION[$GLOBALS['APP_ID']]['activity']['error'] = mysql_error($cgx_connection);
}

if ($_REQUEST['mode'] == 'update') {
    header("Location: ../index.php?" . urldecode($_REQUEST['backvar']) . "&pkey[activity_id]={$_REQUEST['pkey']['activity_id']}");
} elseif ($_REQUEST['mode'] == 'new') {
    header("Location: ../index.php?" . urldecode($_REQUEST['backvar']) . "&pkey[activity_id]={$_REQUEST['data']['activity_id']}");
} elseif ($_REQUEST['mode'] == 'delete') {
    header("Location: ../index.php?" . urldecode($_REQUEST['backvar']));
}
exit;

?>
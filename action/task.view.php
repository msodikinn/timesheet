<?php

/*
 * task
 * Azwari Nugraha <nugraha@pt-gai.com>
 * Oct 10, 2014 1:30:18 AM
 */

require_once '../init.php';

if ($_REQUEST['mode'] == 'progress') {
    
    if (!(is_task_member($_REQUEST['task_id']) || is_task_owner($_REQUEST['task_id']))) die ('insufficient privilege');
    
    if ($_REQUEST['progress'] == 0) {
        $status = 'C';
    } elseif ($_REQUEST['progress'] == 100) {
        $status = 'F';
    } else {
        $status = 'O';
    }
    
    $sql = "UPDATE task SET"
            . " progress = '{$_REQUEST['progress']}', "
            . " task_status = '{$status}' "
            . "WHERE task_id = '{$_REQUEST['task_id']}'";
    mysql_query($sql, $APP_CONNECTION);
    header("Location: ../index.php?m=task&id={$_REQUEST['task_id']}");
    exit;
    
} elseif ($_REQUEST['mode'] == 'assignment') {
    
    if (!is_task_owner($_REQUEST['task_id'])) die ('insufficient privilege');
    
    $sql = "INSERT INTO task_assign (task_id, emp_id) "
            . "VALUES ('{$_REQUEST['task_id']}', '{$_REQUEST['personnel']}')";
            
    mysql_query($sql, $APP_CONNECTION);
    header("Location: ../index.php?m=task&id={$_REQUEST['task_id']}");
    exit;
    
} elseif ($_REQUEST['mode'] == 'comment') {
    
    if (!authenticated()) die ('insufficient privilege');
    
    $sql = "INSERT INTO task_comment (task_id, emp_id, comment) "
            . "VALUES ('{$_REQUEST['task_id']}', '" . user() . "', '" . mysql_real_escape_string($_REQUEST['comment'], $APP_CONNECTION) . "')";

    mysql_query($sql, $APP_CONNECTION);
    header("Location: ../index.php?m=task&id={$_REQUEST['task_id']}");
    exit;
    
} elseif ($_REQUEST['mode'] == 'delete-comment') {
    
    if (!authenticated()) die ('insufficient privilege');
    $data = npl_fetch_table("SELECT * FROM task_comment WHERE task_comment_id = '{$_REQUEST['id']}'");
    if ($data['emp_id'] == user()) {
        mysql_query("DELETE FROM task_comment WHERE task_comment_id = '{$_REQUEST['id']}'", $APP_CONNECTION);
        header("Location: ../index.php?m=task&id={$data['task_id']}");
        exit;
    } else {
        die ('insufficient privilege');
    }
    
} elseif ($_REQUEST['mode'] == 'delete-personnel') {
    
    if (!authenticated()) die ('insufficient privilege');
    $data = npl_fetch_table("SELECT * FROM task WHERE task_id = '{$_REQUEST['task_id']}'");
    if ($data['emp_id'] == user()) {
        mysql_query("DELETE FROM task_assign WHERE task_assign_id = '{$_REQUEST['id']}'", $APP_CONNECTION);
        header("Location: ../index.php?m=task&id={$data['task_id']}");
        exit;
    } else {
        die ('insufficient privilege');
    }
    
} elseif ($_REQUEST['mode'] == 'close') {
    
    if (!authenticated()) die ('insufficient privilege');
    $data = npl_fetch_table("SELECT * FROM task WHERE task_id = '{$_REQUEST['id']}'");
    if ($data['emp_id'] == user()) {
        mysql_query("UPDATE task SET task_status = 'L', closing_date = NOW() WHERE task_id = '{$_REQUEST['id']}'", $APP_CONNECTION);
        header("Location: ../index.php?m=task&id={$_REQUEST['id']}");
        exit;
    } else {
        die ('insufficient privilege');
    }
    
} elseif ($_REQUEST['mode'] == 'cancel') {
    
    if (!authenticated()) die ('insufficient privilege');
    $data = npl_fetch_table("SELECT * FROM task WHERE task_id = '{$_REQUEST['id']}'");
    if ($data['emp_id'] == user()) {
        mysql_query("UPDATE task SET task_status = 'X', closing_date = NOW() WHERE task_id = '{$_REQUEST['id']}'", $APP_CONNECTION);
        header("Location: ../index.php?m=task&id={$_REQUEST['id']}");
        exit;
    } else {
        die ('insufficient privilege');
    }
    
} elseif ($_REQUEST['mode'] == 'delete') {
    
    if (!authenticated()) die ('insufficient privilege');
    $data = npl_fetch_table("SELECT * FROM task WHERE task_id = '{$_REQUEST['id']}'");
    if ($data['emp_id'] == user()) {
        mysql_query("DELETE FROM task_assign WHERE task_id = '{$_REQUEST['id']}'", $APP_CONNECTION);
        mysql_query("DELETE FROM task_comment WHERE task_id = '{$_REQUEST['id']}'", $APP_CONNECTION);
        mysql_query("DELETE FROM task WHERE task_id = '{$_REQUEST['id']}'", $APP_CONNECTION);
        header("Location: ../index.php?m=task");
        exit;
    } else {
        die ('insufficient privilege');
    }
    
} elseif ($_REQUEST['mode'] == 'attachment') {
    
    if (!authenticated()) die ('insufficient privilege');
    $data = npl_fetch_table("SELECT * FROM task WHERE task_id = '{$_REQUEST['id']}'");
    if ($data['emp_id'] == user()) {
        $fu = new FileUpload();
        $fu->upload('TSK', $_REQUEST['id'], 'attachment');
        header("Location: ../index.php?m=task&id={$_REQUEST['id']}");
        exit;
    }
}

?>
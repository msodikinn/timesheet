<?php

/**
 * (c) Azwari Nugraha <nugraha@duabelas.org>
 * 02/03/2014 22:50:26
 */

require_once '../init.php';

if (!authenticated()) {
    header("Location: ../index.php");
    exit;
}

if ($_REQUEST['mode'] == 'update' || $_REQUEST['mode'] == 'new') if (!has_privilege('admin')) die ('akses ditolak');
if ($_REQUEST['mode'] == 'delete') if (!has_privilege('admin')) die ('akses ditolak');

if ($_REQUEST['mode'] == 'export-all') {
    header("Content-Type: text/csv");
    header("Content-Disposition: attachment; filename=\"user-" . date("Y-m-d") . ".csv\"");
    echo "\"Emp No\",\"Employee Name\",\"Email\",\"Active\",\"Administrator\",\"Last Login\",\"Last Ip\"\n";
    $cgx_rs_export = oci_parse($cgx_connection, "SELECT * FROM emp");
    oci_execute($cgx_rs_export);
    while (($cgx_dt_export = oci_fetch_array($cgx_rs_export, OCI_ASSOC)) !== FALSE) {
        echo "\"" . str_replace("\"", "\"\"", $cgx_dt_export['emp_id']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['emp_name']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['email']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['active']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['is_admin']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['last_login']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['last_ip']) . "\"";
        echo "\n";
    }
    oci_free_statement($cgx_rs_export);
    exit;
} elseif ($_REQUEST['mode'] == 'update') {
    $cgx_sql = "UPDATE emp SET";
    $cgx_sql .= " active = '" . mysql_escape_string($_REQUEST['data']['active']) . "'";
    $cgx_sql .= ", is_admin = '" . mysql_escape_string($_REQUEST['data']['is_admin']) . "'";
    $cgx_sql .= ", is_project_manager = '" . mysql_escape_string($_REQUEST['data']['is_project_manager']) . "'";
    $cgx_sql .= " WHERE";
    $cgx_sql .= " emp_id = '{$_REQUEST['pkey']['emp_id']}'";
}

if (@mysql_query($cgx_sql, $cgx_connection)) {
    $_SESSION[$GLOBALS['APP_ID']]['user']['error'] = FALSE;
    $_SESSION[$GLOBALS['APP_ID']]['user']['info'] = 'Your data has been successfully updated';
    if (strlen($_REQUEST['data']['password']) > 0) {
        $new_password = md5($_REQUEST['data']['password']);
        mysql_query("UPDATE emp SET password = '{$new_password}' "
        . "WHERE emp_id = '{$_REQUEST['pkey']['emp_id']}'", $APP_CONNECTION);
    }
} else {
    $_SESSION[$GLOBALS['APP_ID']]['user']['error'] = mysql_error($cgx_connection);
}

if ($_REQUEST['mode'] == 'update') {
    header("Location: ../index.php?" . urldecode($_REQUEST['backvar']) . "&pkey[emp_id]={$_REQUEST['pkey']['emp_id']}");
} elseif ($_REQUEST['mode'] == 'new') {
    header("Location: ../index.php?" . urldecode($_REQUEST['backvar']) . "&pkey[emp_id]={$_REQUEST['data']['emp_id']}");
} elseif ($_REQUEST['mode'] == 'delete') {
    header("Location: ../index.php?" . urldecode($_REQUEST['backvar']));
}
exit;

?>
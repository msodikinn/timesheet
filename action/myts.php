<?php

/**
 * (c) Azwari Nugraha <nugraha@duabelas.org>
 * 03/03/2014 12:05:22
 */

require_once '../init.php';

if (!authenticated()) {
    header("Location: ../index.php");
    exit;
}

if ($_REQUEST['mode'] == 'update' || $_REQUEST['mode'] == 'new') if (!authenticated()) die ('akses ditolak');
if ($_REQUEST['mode'] == 'delete') if (!authenticated()) die ('akses ditolak');

if ($_REQUEST['mode'] == 'export-all') {
    header("Content-Type: text/csv");
    header("Content-Disposition: attachment; filename=\"myts-" . date("Y-m-d") . ".csv\"");
    echo "\"ID\",\"Date\",\"Project\",\"Timesheet\",\"Start Time\",\"End Time\",\"Tasks\",\"Location\",\"Contact Person\"\n";
    $cgx_rs_export = mysql_query("SELECT ts.*, project_name FROM ts JOIN project USING (project_id) WHERE emp_id = '" . user('emp_id') . "'", $cgx_connection);
    while (($cgx_dt_export = mysql_fetch_array($cgx_rs_export, MYSQL_ASSOC)) !== FALSE) {
        echo "\"" . str_replace("\"", "\"\"", $cgx_dt_export['ts_id']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['ts_date']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['project_name']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['ts_type']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['ts_start']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['ts_end']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['tasks']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['location']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['contact_person']) . "\"";
        echo "\n";
    }
    mysql_free_result($cgx_rs_export);
    exit;
} elseif ($_REQUEST['mode'] == 'update') {
    $cgx_sql = "UPDATE ts SET";
    $cgx_sql .= " ts_date = '" . cgx_dmy2ymd($_REQUEST['data']['ts_date']) . "'";
    $cgx_sql .= ", project_id = '{$_REQUEST['data']['project_id']}'";
    $cgx_sql .= ", emp_id = '" . user('emp_id') . "'";
    $cgx_sql .= ", ts_type = '" . mysql_escape_string($_REQUEST['data']['ts_type']) . "'";
    $cgx_sql .= ", ts_start = '" . mysql_escape_string($_REQUEST['data']['ts_start']) . "'";
    $cgx_sql .= ", ts_end = '" . mysql_escape_string($_REQUEST['data']['ts_end']) . "'";
    $cgx_sql .= ", tasks = '" . mysql_escape_string($_REQUEST['data']['tasks']) . "'";
    $cgx_sql .= ", location = '" . mysql_escape_string($_REQUEST['data']['location']) . "'";
    $cgx_sql .= ", contact_person = '" . mysql_escape_string($_REQUEST['data']['contact_person']) . "'";
    $cgx_sql .= " WHERE";
    $cgx_sql .= " ts_id = '{$_REQUEST['pkey']['ts_id']}'";
} elseif ($_REQUEST['mode'] == 'new') {
    $cgx_sql = "INSERT INTO ts (";
    $cgx_sql .= "ts_date,project_id,emp_id,ts_type,ts_start,ts_end,tasks,location,contact_person";
    $cgx_sql .= ") values (";
    $cgx_sql .= "'" . cgx_dmy2ymd($_REQUEST['data']['ts_date']) . "'";
    $cgx_sql .= ",'{$_REQUEST['data']['project_id']}'";
    $cgx_sql .= ",'" . user('emp_id') . "'";
    $cgx_sql .= ",'" . mysql_escape_string($_REQUEST['data']['ts_type']) . "'";
    $cgx_sql .= ",'" . mysql_escape_string($_REQUEST['data']['ts_start']) . "'";
    $cgx_sql .= ",'" . mysql_escape_string($_REQUEST['data']['ts_end']) . "'";
    $cgx_sql .= ",'" . mysql_escape_string($_REQUEST['data']['tasks']) . "'";
    $cgx_sql .= ",'" . mysql_escape_string($_REQUEST['data']['location']) . "'";
    $cgx_sql .= ",'" . mysql_escape_string($_REQUEST['data']['contact_person']) . "'";
    $cgx_sql .= ")";
} elseif ($_REQUEST['mode'] == 'delete') {
    $cgx_sql = "DELETE FROM ts ";
    $cgx_sql .= " WHERE";
    $cgx_sql .= " ts_id = '{$_REQUEST['pkey']['ts_id']}'";
}

if (@mysql_query($cgx_sql, $cgx_connection)) {
    $_SESSION[$GLOBALS['APP_ID']]['myts']['error'] = FALSE;
    $_SESSION[$GLOBALS['APP_ID']]['myts']['info'] = 'Your data has been successfully updated';
    if ($_REQUEST['mode'] == 'new') {
        $cgx_new_id = mysql_insert_id($cgx_connection);
    } else {
        $cgx_new_id = $_REQUEST['pkey']['ts_id'];
    }
    mysql_query("UPDATE project SET update_date = NOW() WHERE project_id = '{$_REQUEST['data']['project_id']}'", $APP_CONNECTION);
    mysql_query("UPDATE emp SET default_project_id = '{$_REQUEST['data']['project_id']}' WHERE emp_id = '" . user('emp_id') . "'", $APP_CONNECTION);
} else {
    $_SESSION[$GLOBALS['APP_ID']]['myts']['error'] = mysql_error($cgx_connection);
}

if ($_REQUEST['mode'] == 'update' || $_REQUEST['mode'] == 'new') {
    mysql_query("DELETE FROM ts_activity WHERE ts_id = '{$cgx_new_id}'", $APP_CONNECTION);
    if (is_array($_REQUEST['data']['activity'])) {
        foreach ($_REQUEST['data']['activity'] as $activity_id => $on) {
            mysql_query("INSERT INTO ts_activity (ts_id, activity_id) " .
                    "VALUES ('{$cgx_new_id}', '{$activity_id}')",
                    $APP_CONNECTION);
        }
    }
}

if ($_REQUEST['mode'] == 'update') {
//    header("Location: ../index.php?" . urldecode($_REQUEST['backvar']) . "&pkey[ts_id]={$_REQUEST['pkey']['ts_id']}");
    header("Location: ../index.php?" . urldecode($_REQUEST['backvar']));
} elseif ($_REQUEST['mode'] == 'new') {
//    header("Location: ../index.php?" . urldecode($_REQUEST['backvar']) . "&pkey[ts_id]={$cgx_new_id}");
    header("Location: ../index.php?" . urldecode($_REQUEST['backvar']));
} elseif ($_REQUEST['mode'] == 'delete') {
    header("Location: ../index.php?" . urldecode($_REQUEST['backvar']));
}
exit;

?>
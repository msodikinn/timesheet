<?php

/*
 * profile
 * Azwari Nugraha <nugraha@duabelas.org>
 * Mar 3, 2014 12:02:58 PM
 */

require_once '../init.php';
require_once '../lib/images.php';

if (!authenticated()) {
    header("Location: ../index.php");
    exit;
}

if ($_FILES['avatar-file']['error'] == 0) {
    image_crop_max($_FILES['avatar-file']['tmp_name'], $APP_BASE_DIR . '/images/profile/' . user('emp_id') . '.jpg', 64, 64);
}

if ($_REQUEST['avatar-del']) {
    unlink($APP_BASE_DIR . '/images/profile/' . user('emp_id') . '.jpg');
}

$cgx_data = cgx_fetch_table("SELECT * FROM emp WHERE emp.emp_id = '" . user('emp_id') . "'");

if (!empty($_REQUEST['passwd1'])) {
    if (empty($_REQUEST['passwd1']) || empty($_REQUEST['passwd2']) || empty($_REQUEST['passwd3'])) {
        $_SESSION[$GLOBALS['APP_ID']]['profile']['error'] = "Password cannot be empty";
    } elseif (md5($_REQUEST['passwd1']) != $cgx_data['password']) {
        $_SESSION[$GLOBALS['APP_ID']]['profile']['error'] = "Invalid current password";
    } elseif ($_REQUEST['passwd2'] != $_REQUEST['passwd3']) {
        $_SESSION[$GLOBALS['APP_ID']]['profile']['error'] = "Confirmed password not match";
    } else {
        mysql_query(
                "UPDATE emp SET password = '" . md5($_REQUEST['passwd2']) . "' "
                . "WHERE emp_id = '" . user('emp_id') . "'",
                $APP_CONNECTION);
        $_SESSION[$GLOBALS['APP_ID']]['profile']['info'] = "Your password has been successfully changed";
    }
}

header("Location: ../index.php?m=profile");
exit;

?>
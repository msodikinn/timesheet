<?php

/**
 * (c) Azwari Nugraha <nugraha@duabelas.org>
 * 02/03/2014 20:38:03
 */

require_once '../init.php';

if (!authenticated()) {
    header("Location: ../index.php");
    exit;
}

if ($_REQUEST['mode'] == 'update' || $_REQUEST['mode'] == 'new') if (!has_privilege('admin')) die ('akses ditolak');
if ($_REQUEST['mode'] == 'delete') if (!has_privilege('admin')) die ('akses ditolak');

if ($_REQUEST['mode'] == 'export-all') {
    header("Content-Type: text/csv");
    header("Content-Disposition: attachment; filename=\"project-" . date("Y-m-d") . ".csv\"");
    echo "\"ID\",\"Project Name\",\"Description\",\"Company Name\",\"Date Start\",\"Date End\",\"Active\",\"Last Updated\"\n";
    $cgx_rs_export = oci_parse($cgx_connection, "SELECT * FROM project");
    oci_execute($cgx_rs_export);
    while (($cgx_dt_export = oci_fetch_array($cgx_rs_export, OCI_ASSOC)) !== FALSE) {
        echo "\"" . str_replace("\"", "\"\"", $cgx_dt_export['project_id']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['project_name']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['description']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['company_name']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['date_start']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['date_end']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['active']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['update_date']) . "\"";
        echo "\n";
    }
    oci_free_statement($cgx_rs_export);
    exit;
} elseif ($_REQUEST['mode'] == 'update') {
    $cgx_sql = "UPDATE project SET";
    $cgx_sql .= " project_name = '" . mysql_escape_string($_REQUEST['data']['project_name']) . "'";
    $cgx_sql .= ", project_code = '" . mysql_escape_string($_REQUEST['data']['project_code']) . "'";
    $cgx_sql .= ", description = '" . mysql_escape_string($_REQUEST['data']['description']) . "'";
    $cgx_sql .= ", company_name = '" . mysql_escape_string($_REQUEST['data']['company_name']) . "'";
    $cgx_sql .= ", date_start = '" . cgx_dmy2ymd($_REQUEST['data']['date_start']) . "'";
    $cgx_sql .= ", date_end = '" . cgx_dmy2ymd($_REQUEST['data']['date_end']) . "'";
    $cgx_sql .= ", active = '" . mysql_escape_string($_REQUEST['data']['active']) . "'";
    $cgx_sql .= " WHERE";
    $cgx_sql .= " project_id = '{$_REQUEST['pkey']['project_id']}'";
} elseif ($_REQUEST['mode'] == 'new') {
    $cgx_sql = "INSERT INTO project (";
    $cgx_sql .= "project_name,project_code,description,company_name,date_start,date_end,active";
    $cgx_sql .= ") values (";
    $cgx_sql .= "'" . mysql_escape_string($_REQUEST['data']['project_name']) . "'";
    $cgx_sql .= ",'" . mysql_escape_string($_REQUEST['data']['project_code']) . "'";
    $cgx_sql .= ",'" . mysql_escape_string($_REQUEST['data']['description']) . "'";
    $cgx_sql .= ",'" . mysql_escape_string($_REQUEST['data']['company_name']) . "'";
    $cgx_sql .= ",'" . cgx_dmy2ymd($_REQUEST['data']['date_start']) . "'";
    $cgx_sql .= ",'" . cgx_dmy2ymd($_REQUEST['data']['date_end']) . "'";
    $cgx_sql .= ",'" . mysql_escape_string($_REQUEST['data']['active']) . "'";
    $cgx_sql .= ")";
} elseif ($_REQUEST['mode'] == 'delete') {
    $cgx_sql = "DELETE FROM project ";
    $cgx_sql .= " WHERE";
    $cgx_sql .= " project_id = '{$_REQUEST['pkey']['project_id']}'";
}

if (@mysql_query($cgx_sql, $cgx_connection)) {
    $_SESSION[$GLOBALS['APP_ID']]['project']['error'] = FALSE;
    $_SESSION[$GLOBALS['APP_ID']]['project']['info'] = 'Your data has been successfully updated';
    if ($_REQUEST['mode'] == 'new') $cgx_new_id = mysql_insert_id($cgx_connection);
} else {
    $_SESSION[$GLOBALS['APP_ID']]['project']['error'] = mysql_error($cgx_connection);
}

if ($_REQUEST['mode'] == 'update') {
    header("Location: ../index.php?" . urldecode($_REQUEST['backvar']) . "&pkey[project_id]={$_REQUEST['pkey']['project_id']}");
} elseif ($_REQUEST['mode'] == 'new') {
    header("Location: ../index.php?" . urldecode($_REQUEST['backvar']) . "&pkey[project_id]={$cgx_new_id}");
} elseif ($_REQUEST['mode'] == 'delete') {
    header("Location: ../index.php?" . urldecode($_REQUEST['backvar']));
}
exit;

?>
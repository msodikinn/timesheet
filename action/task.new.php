<?php

/*
 * task
 * Azwari Nugraha <nugraha@pt-gai.com>
 * Oct 9, 2014 10:04:58 PM
 */

require_once '../init.php';
if (!authenticated()) die;

$sql = "INSERT INTO task ("
        . "project_id, task_title, task_description, "
        . "created_date, start_date, end_date, "
        . "weight, priority, emp_id"
        . ") VALUES ("
        . "'{$_REQUEST['project_id']}', '" . mysql_real_escape_string($_REQUEST['task_title'], $APP_CONNECTION) . "', '" . mysql_real_escape_string($_REQUEST['task_desc'], $APP_CONNECTION) . "', "
        . "NOW(), " . (npl_emptydate(npl_dmy2ymd($_REQUEST['start_date'])) ? 'NULL' : "'" . npl_dmy2ymd($_REQUEST['start_date']) .  "'") . ", " . (npl_emptydate(npl_dmy2ymd($_REQUEST['end_date'])) ? 'NULL' : "'" . npl_dmy2ymd($_REQUEST['end_date']) .  "'") . ", "
        . "'{$_REQUEST['weight']}', '{$_REQUEST['priority']}', '" . user() . "')";
if (mysql_query($sql, $APP_CONNECTION)) {
    
    $new_id = mysql_insert_id($APP_CONNECTION);
    if ($_REQUEST['assigned_to']) mysql_query("INSERT INTO task_assign (task_id, emp_id) VALUES ('{$new_id}', '{$_REQUEST['assigned_to']}')", $APP_CONNECTION);
    
    header("Location: ../index.php?m=task&id={$new_id}");
    exit;
} else {
    die(mysql_error($APP_CONNECTION));
}

?>
<?php

/**
 * (c) Azwari Nugraha <nugraha@pt-gai.com>
 * 29/04/2014 16:30:16
 */

require_once '../init.php';

if (!authenticated()) {
    header("Location: ../index.php");
    exit;
}

//if ($_REQUEST['mode'] == 'update' || $_REQUEST['mode'] == 'new') if (!has_privilege('timesheet.edit')) die ('akses ditolak');
//if ($_REQUEST['mode'] == 'delete') if (!has_privilege('timesheet.edit')) die ('akses ditolak');

if ($_REQUEST['mode'] == 'export-all') {
    header("Content-Type: text/csv");
    $cgx_search = $_REQUEST['cgx_search'];
    $start_date = $_REQUEST['start_date'];
    $end_date = $_REQUEST['end_date'];
    header("Content-Disposition: attachment; filename=\"ts-" . date("Y-m-d") . ".csv\"");
    echo "\"Timesheet ID\",\"Name\",\"Date\",\"Start Time\",\"End Time\",\"Location\",\"Project Name\",\"Tasks\"\n";
    $cgx_rs_export = mysql_query("select * from ts join emp using (emp_id) "
            . "join project using (project_id) where ((location LIKE '%{$cgx_search}%') || (emp_id LIKE '%{$cgx_search}%') || "
            . "(emp_name LIKE '%{$cgx_search}%') || "
            . "(project_name LIKE '%{$cgx_search}%')) AND ((ts_date >= '{$start_date}') AND (ts_date <= '{$end_date}'))", $cgx_connection);
    while (($cgx_dt_export = mysql_fetch_array($cgx_rs_export, MYSQL_ASSOC)) !== FALSE) {
        echo "\"" . str_replace("\"", "\"\"", $cgx_dt_export['ts_id']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['emp_name']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['ts_date']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['ts_start']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['ts_end']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['location']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['project_name']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['tasks']) . "\"";
        echo "\n";
    }
    mysql_free_result($cgx_rs_export);
    exit;
}// elseif ($_REQUEST['mode'] == 'update') {
   // $cgx_sql = "UPDATE ts SET";
   // $cgx_sql .= " emp_name = '" . mysql_escape_string($_REQUEST['data']['emp_name']) . "'";
   // $cgx_sql .= ", ts_date = '" . mysql_escape_string($_REQUEST['data']['ts_date']) . "'";
    //$cgx_sql .= ", ts_start = '" . mysql_escape_string($_REQUEST['data']['ts_start']) . "'";
    //$cgx_sql .= ", ts_end = '" . mysql_escape_string($_REQUEST['data']['ts_end']) . "'";
    //$cgx_sql .= ", location = '" . mysql_escape_string($_REQUEST['data']['location']) . "'";
    //$cgx_sql .= ", project_name = '" . mysql_escape_string($_REQUEST['data']['project_name']) . "'";
    //$cgx_sql .= ", tasks = '" . mysql_escape_string($_REQUEST['data']['tasks']) . "'";
    //$cgx_sql .= " WHERE";
    //$cgx_sql .= " ts_id = '" . mysql_escape_string($_REQUEST['data']['ts_id']) . "'";
//} elseif ($_REQUEST['mode'] == 'new') {
    //$cgx_sql = "INSERT INTO ts (";
    //$cgx_sql .= "hr_m_emp_skill_id,hr_m_emp_id,skill_name";
    //$cgx_sql .= ") values (";
    //$cgx_sql .= "'" . mysql_escape_string($_REQUEST['data']['hr_m_emp_skill_id']) . "'";
    //$cgx_sql .= ",'" . mysql_escape_string($_REQUEST['data']['hr_m_emp_id']) . "'";
    //$cgx_sql .= ",'" . mysql_escape_string($_REQUEST['data']['skill_name']) . "'";
    //$cgx_sql .= ")";
//} elseif ($_REQUEST['mode'] == 'delete') {
    //$cgx_sql = "DELETE FROM ts ";
    //$cgx_sql .= " WHERE";
    //$cgx_sql .= " ts_id = '{$_REQUEST['pkey']['ts_id']}'";
//}

if (@mysql_query($cgx_sql, $cgx_connection)) {
    $_SESSION[$GLOBALS['APP_ID']]['ts']['error'] = FALSE;
    $_SESSION[$GLOBALS['APP_ID']]['ts']['info'] = 'Your data has been successfully updated';
    //if ($_REQUEST['mode'] == 'new') $cgx_new_id = mysql_insert_id($cgx_connection);
} else {
    $_SESSION[$GLOBALS['APP_ID']]['ts']['error'] = mysql_error($cgx_connection);
}

//if ($_REQUEST['mode'] == 'update') {
    //header("Location: ../index.php?" . urldecode($_REQUEST['backvar']) . "&pkey[ts_id]={$_REQUEST['pkey']['ts_id']}");
//} elseif ($_REQUEST['mode'] == 'new') {
    //header("Location: ../index.php?" . urldecode($_REQUEST['backvar']) . "&pkey[ts_id]={$_REQUEST['data']['ts_id']}");
//} elseif ($_REQUEST['mode'] == 'delete') {
    //header("Location: ../index.php?" . urldecode($_REQUEST['backvar']));
//}
exit;

?>
<?php

/**
 * (c) Azwari Nugraha <nugraha@duabelas.org>
 * 02/03/2014 20:53:10
 */

require_once '../init.php';

if (!authenticated()) {
    header("Location: ../index.php");
    exit;
}

if ($_REQUEST['mode'] == 'update' || $_REQUEST['mode'] == 'new') if (!has_privilege('admin')) die ('akses ditolak');
if ($_REQUEST['mode'] == 'delete') if (!has_privilege('admin')) die ('akses ditolak');

if ($_REQUEST['mode'] == 'export-all') {
    header("Content-Type: text/csv");
    header("Content-Disposition: attachment; filename=\"emp-" . date("Y-m-d") . ".csv\"");
    echo "\"No ID\",\"Name\",\"Birth Place\",\"Birth Date\",\"Sex\",\"Address\",\"Phone\",\"Email\"\n";
    $cgx_rs_export = mysql_query("SELECT * FROM pelamar",$cgx_connection);
    while (($cgx_dt_export = mysql_fetch_array($cgx_rs_export, MYSQL_ASSOC)) !== FALSE) {
        echo "\"" . str_replace("\"", "\"\"", $cgx_dt_export['no_id']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['name']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['birth_place']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['birth_date']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['sex']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['address']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['phone']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['email']) . "\"";
        echo "\n";
    }
    mysql_free_result($cgx_rs_export);
    exit;
} elseif ($_REQUEST['mode'] == 'update') {
    $cgx_sql = "UPDATE pelamar SET";
    $cgx_sql .= " name = '" . mysql_escape_string($_REQUEST['data']['name']) . "'";
    $cgx_sql .= ", birth_place = '" . mysql_escape_string($_REQUEST['data']['birth_place']) . "'";
    $cgx_sql .= ", birth_date = '" . cgx_dmy2ymd($_REQUEST['data']['birth_date']) . "'";
    $cgx_sql .= ", sex = '" . mysql_escape_string($_REQUEST['data']['sex']) . "'";
    $cgx_sql .= ", address = '" . mysql_escape_string($_REQUEST['data']['address']) . "'";
    $cgx_sql .= ", phone = '" . mysql_escape_string($_REQUEST['data']['phone']) . "'";
    $cgx_sql .= ", email = '" . mysql_escape_string($_REQUEST['data']['email']) . "'";
    $cgx_sql .= " WHERE";
    $cgx_sql .= " no_id = '{$_REQUEST['pkey']['no_id']}'";
} elseif ($_REQUEST['mode'] == 'new') {
    $cgx_sql = "INSERT INTO pelamar (";
    $cgx_sql .= "no_id,name,birth_place,birth_date,sex,address,phone,email";
    $cgx_sql .= ") values (";
    $cgx_sql .= "'" . mysql_escape_string($_REQUEST['data']['no_id']) . "'";
    $cgx_sql .= ",'" . mysql_escape_string($_REQUEST['data']['name']) . "'";
    $cgx_sql .= ",'" . mysql_escape_string($_REQUEST['data']['birth_place']) . "'";
    $cgx_sql .= ",'" . cgx_dmy2ymd($_REQUEST['data']['birth_date']) . "'";
    $cgx_sql .= ",'" . mysql_escape_string($_REQUEST['data']['sex']) . "'";
    $cgx_sql .= ",'" . mysql_escape_string($_REQUEST['data']['address']) . "'";
    $cgx_sql .= ",'" . mysql_escape_string($_REQUEST['data']['phone']) . "'";
    $cgx_sql .= ",'" . mysql_escape_string($_REQUEST['data']['email']) . "'";
    $cgx_sql .= ")";
} elseif ($_REQUEST['mode'] == 'delete') {
    $cgx_sql = "DELETE FROM pelamar ";
    $cgx_sql .= " WHERE";
    $cgx_sql .= " no_id = '{$_REQUEST['pkey']['no_id']}'";
}

if (@mysql_query($cgx_sql, $cgx_connection)) {
    $_SESSION[$GLOBALS['APP_ID']]['pelamar']['error'] = FALSE;
    $_SESSION[$GLOBALS['APP_ID']]['pelamar']['info'] = 'Your data has been successfully updated';
    if ($_REQUEST['mode'] == 'new') $cgx_new_id = mysql_insert_id($cgx_connection);
} else {
    $_SESSION[$GLOBALS['APP_ID']]['pelamar']['error'] = mysql_error($cgx_connection);
}

if ($_REQUEST['mode'] == 'update') {
    header("Location: ../index.php?" . urldecode($_REQUEST['backvar']) . "&pkey[no_id]={$_REQUEST['pkey']['no_id']}");
} elseif ($_REQUEST['mode'] == 'new') {
    header("Location: ../index.php?" . urldecode($_REQUEST['backvar']) . "&pkey[no_id]={$_REQUEST['data']['no_id']}");
} elseif ($_REQUEST['mode'] == 'delete') {
    header("Location: ../index.php?" . urldecode($_REQUEST['backvar']));
}
exit;

?>
<?php

/**
 * (c) Azwari Nugraha <nugraha@pt-gai.com>
 * 10/10/2014 20:16:56
 */

require_once '../init.php';

if (!authenticated()) {
    header("Location: ../index.php");
    exit;
}

if ($_REQUEST['mode'] == 'update' || $_REQUEST['mode'] == 'new') if (!authenticated()) die ('akses ditolak');
if ($_REQUEST['mode'] == 'delete') if (!authenticated()) die ('akses ditolak');

if ($_REQUEST['mode'] == 'export-all') {
    header("Content-Type: text/csv");
    header("Content-Disposition: attachment; filename=\"task-" . date("Y-m-d") . ".csv\"");
    echo "\"No\",\"Task\",\"Project Name\",\"Project Name\",\"Description\",\"Start Date\",\"End Date\",\"Closing Date\",\"Weight\",\"Priority\",\"Progress\",\"Status\",\"Created Date\",\"Update Date\",\"Created By\"\n";
    $cgx_rs_export = mysql_query("SELECT task.*, project_name, emp_name FROM task JOIN project USING (project_id) JOIN emp USING (emp_id)", $cgx_connection);
    while (($cgx_dt_export = mysql_fetch_array($cgx_rs_export, MYSQL_ASSOC)) !== FALSE) {
        echo "\"" . str_replace("\"", "\"\"", $cgx_dt_export['task_id']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['task_title']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['project_id']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['project_name']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['task_description']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['start_date']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['end_date']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['closing_date']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['weight']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['priority']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['progress']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['task_status']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['created_date']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['update_date']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['emp_name']) . "\"";
        echo "\n";
    }
    mysql_free_result($cgx_rs_export);
    exit;
} elseif ($_REQUEST['mode'] == 'update') {
    $cgx_sql = "UPDATE  SET";
    $cgx_sql .= " WHERE";
} elseif ($_REQUEST['mode'] == 'new') {
    $cgx_sql = "INSERT INTO  (";
    $cgx_sql .= "INSERT INTO  (";
    $cgx_sql .= ") values (";
    $cgx_sql .= ")";
} elseif ($_REQUEST['mode'] == 'delete') {
    $cgx_sql = "DELETE FROM  ";
    $cgx_sql .= " WHERE";
}

if (@mysql_query($cgx_sql, $cgx_connection)) {
    $_SESSION[$GLOBALS['APP_ID']]['task']['error'] = FALSE;
    $_SESSION[$GLOBALS['APP_ID']]['task']['info'] = 'Your data has been successfully updated';
    if ($_REQUEST['mode'] == 'new') $cgx_new_id = $_REQUEST['data']['Array'];
} else {
    $_SESSION[$GLOBALS['APP_ID']]['task']['error'] = mysql_error($cgx_connection);
}

if ($_REQUEST['mode'] == 'update') {
    header("Location: ../index.php?" . urldecode($_REQUEST['backvar']) . "&");
} elseif ($_REQUEST['mode'] == 'new') {
    header("Location: ../index.php?" . urldecode($_REQUEST['backvar']) . "&");
} elseif ($_REQUEST['mode'] == 'delete') {
    header("Location: ../index.php?" . urldecode($_REQUEST['backvar']));
}
exit;

?>
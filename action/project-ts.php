<?php

/**
 * (c) Azwari Nugraha <nugraha@duabelas.org>
 * 03/03/2014 14:03:24
 */

require_once '../init.php';

if (!authenticated()) {
    header("Location: ../index.php");
    exit;
}

if ($_REQUEST['mode'] == 'update' || $_REQUEST['mode'] == 'new') if (!authenticated()) die ('akses ditolak');
if ($_REQUEST['mode'] == 'delete') if (!authenticated()) die ('akses ditolak');

if ($_REQUEST['mode'] == 'export-all') {
    header("Content-Type: text/csv");
    header("Content-Disposition: attachment; filename=\"project-ts-" . date("Y-m-d") . ".csv\"");
    echo "\"Task ID\",\"Employee Name\",\"Date\",\"Start Time\",\"End Time\",\"Tasks\",\"Working Location\",\"Contact Person\",\"Last Updated\"\n";
    $cgx_rs_export = mysql_query("SELECT ts.*, project_name, emp_name FROM ts JOIN project USING (project_id) JOIN emp USING (emp_id) WHERE project_id = '{$_REQUEST['id']}'", $cgx_connection);
    while (($cgx_dt_export = mysql_fetch_array($cgx_rs_export, MYSQL_ASSOC)) !== FALSE) {
        echo "\"" . str_replace("\"", "\"\"", $cgx_dt_export['ts_id']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['emp_name']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['ts_date']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['ts_start']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['ts_end']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['tasks']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['location']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['contact_person']) . "\"";
        echo ",\"" . str_replace("\"", "\"\"", $cgx_dt_export['last_updated']) . "\"";
        echo "\n";
    }
    mysql_free_result($cgx_rs_export);
    exit;
} elseif ($_REQUEST['mode'] == 'update') {
    $cgx_sql = "UPDATE  SET";
    $cgx_sql .= " WHERE";
} elseif ($_REQUEST['mode'] == 'new') {
    $cgx_sql = "INSERT INTO  (";
    $cgx_sql .= "INSERT INTO  (";
    $cgx_sql .= ") values (";
    $cgx_sql .= ")";
} elseif ($_REQUEST['mode'] == 'delete') {
    $cgx_sql = "DELETE FROM  ";
    $cgx_sql .= " WHERE";
}

if (@mysql_query($cgx_sql, $cgx_connection)) {
    $_SESSION[$GLOBALS['APP_ID']]['project-ts']['error'] = FALSE;
    $_SESSION[$GLOBALS['APP_ID']]['project-ts']['info'] = 'Your data has been successfully updated';
    if ($_REQUEST['mode'] == 'new') $cgx_new_id = $_REQUEST['data']['Array'];
} else {
    $_SESSION[$GLOBALS['APP_ID']]['project-ts']['error'] = mysql_error($cgx_connection);
}

if ($_REQUEST['mode'] == 'update') {
    header("Location: ../index.php?" . urldecode($_REQUEST['backvar']) . "&");
} elseif ($_REQUEST['mode'] == 'new') {
    header("Location: ../index.php?" . urldecode($_REQUEST['backvar']) . "&");
} elseif ($_REQUEST['mode'] == 'delete') {
    header("Location: ../index.php?" . urldecode($_REQUEST['backvar']));
}
exit;

?>